package com.inventos.plataformab.view.presenter;

import android.test.AndroidTestCase;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.LoginUseCase;
import com.inventos.plataformab.domain.interactor.RetrievePasswordUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.login.LoginPresenter;
import com.inventos.plataformab.presentation.util.FacebookHelper;
import com.inventos.plataformab.presentation.view.LoginView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest  extends AndroidTestCase {
    @Mock private SaveUserUseCase saveUserUseCase;
    @Mock private LoginView loginView;
    @Mock private LoginUseCase loginUseCase;
    @Mock private RetrievePasswordUseCase retrievePasswordUseCase;
    @Mock private FacebookHelper facebookHelper;
    @InjectMocks private LoginPresenter presenter;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        presenter.setView(loginView);
    }

    @Test
    public void testAuthenticate() {
        User dummyUser = User.newUserFromLogin("teste@123.com", "123");
        presenter.authenticate(dummyUser.getEmail(), dummyUser.getPassword());
        verify(loginUseCase, times(1)).setUser(any(User.class));
        verify(loginView, times(1)).hideMain();
        verify(loginView, times(1)).showLoading();
        verify(loginUseCase, times(1)).execute(any(DefaultSubscriber.class));
    }

    @Test
    public void testAuthenticateUsingFacebook() {
        presenter.authenticateUsingFacebook();
        verify(loginView, times(1)).hideMain();
        verify(loginView, times(1)).showLoading();
        verify(facebookHelper, times(1)).startAuthentication();
    }

    @Test
    public void testRecoverPassword() {
        String dummyEmail = "dummyEmail@email.com";
        presenter.retrievePassword(dummyEmail);
        verify(retrievePasswordUseCase, times(1)).setEmail(dummyEmail);
        verify(loginView, times(1)).hideForgotPassword();
        verify(loginView, times(1)).showLoading();
        verify(retrievePasswordUseCase, times(1)).execute(any(DefaultSubscriber.class));
    }
}
