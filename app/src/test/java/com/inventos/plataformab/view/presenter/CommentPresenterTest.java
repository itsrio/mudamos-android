package com.inventos.plataformab.view.presenter;

import android.test.AndroidTestCase;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetCommentsUseCase;
import com.inventos.plataformab.domain.interactor.LikeDislikeUseCase;
import com.inventos.plataformab.domain.interactor.RemoveLikeDislikeUseCase;
import com.inventos.plataformab.domain.interactor.SendCommentUseCase;
import com.inventos.plataformab.presentation.presenter.subject.CommentPresenter;
import com.inventos.plataformab.presentation.view.SubjectView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class CommentPresenterTest extends AndroidTestCase {
    @Mock SendCommentUseCase sendCommentUseCase;
    @Mock GetCommentsUseCase getCommentsUseCase;
    @Mock LikeDislikeUseCase likeDislikeUseCase;
    @Mock RemoveLikeDislikeUseCase removeLikeDislikeUseCase;
    @Mock SubjectView subjectView;
    @InjectMocks CommentPresenter presenter;

    boolean dummyHasAnswered = true;
    boolean dummyIsAnonymous = true;

    int dummySubjectId = 10;
    int dummyCommentId = 2;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        presenter.setView(subjectView);
    }

    @Test
    public void testLoadComments(){
        presenter.initialize();
        verify(subjectView, times(1)).showLoading();
        verify(subjectView, times(1)).hideRetry();
        verify(getCommentsUseCase).setPage(0);
        verify(getCommentsUseCase).execute(any(DefaultSubscriber.class));
        verifyNoMoreInteractions(subjectView);
        verifyNoMoreInteractions(getCommentsUseCase);
    }

    @Test
    public void testGetCommentPagination() {
        int pages = 5;
        for (int i = 1; i <= pages; i++) {
            presenter.loadMoreComments();
            assertEquals(presenter.getPage(), i);
        }
        verify(getCommentsUseCase, times(pages)).setPage(any(Integer.class));
        verify(getCommentsUseCase, times(pages)).execute(any(DefaultSubscriber.class));
        verifyNoMoreInteractions(subjectView);
        verifyNoMoreInteractions(getCommentsUseCase);
    }

    @Test
    public void testSendCommentsAsAnonymous() {
        String dummyComment = "Test";
        presenter.setCommentConfiguration(dummyHasAnswered, dummyIsAnonymous);
        presenter.onSendClick(dummyComment);
        verify(subjectView).showAlreadyCommentedDialog();
        presenter.sendComment();
        verify(subjectView).showLoadingDialog();
        verify(sendCommentUseCase).setComment(dummyComment, dummyIsAnonymous);
    }

    @Test
    public void testSendCommentsWithUserName() {
        String dummyComment = "Test";
        dummyIsAnonymous = false;
        presenter.setCommentConfiguration(dummyHasAnswered, dummyIsAnonymous);
        presenter.onSendClick(dummyComment);
        verify(subjectView).showAlreadyCommentedDialog();
        presenter.sendComment();
        verify(subjectView).showLoadingDialog();
        verify(sendCommentUseCase).setComment(dummyComment, dummyIsAnonymous);
    }

    @Test
    public void testSendCommentsWithoutAnswer() {
        String dummyComment = "Test";
        dummyHasAnswered = false;
        presenter.setCommentConfiguration(dummyHasAnswered, dummyIsAnonymous);
        presenter.onSendClick(dummyComment);
        verify(subjectView).showAnonymousDialog();
    }

    @Test
    public void testCommentLike() {
        presenter.addLike(dummySubjectId, dummyCommentId);
        verify(likeDislikeUseCase).setSubjectId(dummySubjectId);
        verify(likeDislikeUseCase).setCommentId(dummyCommentId);
        verify(likeDislikeUseCase).setLike(true);
        verify(likeDislikeUseCase).execute(any(DefaultSubscriber.class));
    }

    @Test
    public void testCommentDislike() {
        presenter.addDislike(dummySubjectId, dummyCommentId);
        verify(likeDislikeUseCase).setSubjectId(dummySubjectId);
        verify(likeDislikeUseCase).setCommentId(dummyCommentId);
        verify(likeDislikeUseCase).setLike(false);
        verify(likeDislikeUseCase).execute(any(DefaultSubscriber.class));
    }

    @Test
    public void testUndoCommentLike() {
        presenter.removeLike(dummySubjectId, dummyCommentId);
        verify(removeLikeDislikeUseCase).setSubjectId(dummySubjectId);
        verify(removeLikeDislikeUseCase).setCommentId(dummyCommentId);
        verify(removeLikeDislikeUseCase).setLike(true);
        verify(removeLikeDislikeUseCase).execute(any(DefaultSubscriber.class));
    }

    @Test
    public void testUndoCommentDislike() {
        presenter.removeDislike(dummySubjectId, dummyCommentId);
        verify(removeLikeDislikeUseCase).setSubjectId(dummySubjectId);
        verify(removeLikeDislikeUseCase).setCommentId(dummyCommentId);
        verify(removeLikeDislikeUseCase).setLike(false);
        verify(removeLikeDislikeUseCase).execute(any(DefaultSubscriber.class));
    }

    @Test
    public void testPresenterDestroy(){
        presenter.destroy();
        verify(getCommentsUseCase).unsubscribe();
        verify(sendCommentUseCase).unsubscribe();
        verifyNoMoreInteractions(getCommentsUseCase);
        verifyNoMoreInteractions(sendCommentUseCase);
    }
}
