package com.inventos.plataformab.view.presenter;

import android.test.AndroidTestCase;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.EditUserUseCase;
import com.inventos.plataformab.domain.interactor.GetSignUpComponentsUseCase;
import com.inventos.plataformab.domain.interactor.GetStatesUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.user.EditUserPresenter;
import com.inventos.plataformab.presentation.view.EditUserView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EditUserPresenterTest extends AndroidTestCase {

    @Mock private EditUserUseCase editUserUseCase;
    @Mock private SaveUserUseCase saveUserUseCase;
    @Mock private GetStatesUseCase getStatesUseCase;
    @Mock private GetSignUpComponentsUseCase getSignUpComponentsUseCase;
    @Mock private EditUserView editUserView;
    @InjectMocks private EditUserPresenter presenter;

    @Override protected void setUp() throws Exception {
        super.setUp();
        presenter.setView(editUserView);
    }

    @Test
    public void testPresenterStart() {
        presenter.start();
        verify(editUserView, times(1)).hideMain();
        verify(editUserView, times(1)).showLoading();
        verify(editUserView, times(1)).hideRetry();
        verify(getStatesUseCase, times(1)).execute(any(DefaultSubscriber.class));
        verify(getSignUpComponentsUseCase, times(1)).execute(any(DefaultSubscriber.class));
    }

    @Test public void testPresenterEditUser() {
        User dummyUser = new User();
        dummyUser.setPassword("123");
        when(editUserView.getNewUserFields()).thenReturn(dummyUser);
        presenter.editUser(dummyUser);
        verify(editUserView, times(1)).getNewUserFields();
        verify(editUserUseCase, times(1)).setUser(dummyUser);
        verify(editUserUseCase, times(1)).execute(any(DefaultSubscriber.class));
    }

    @Test public void testPresenterStop() {
        presenter.destroy();
        verify(editUserUseCase, times(1)).unsubscribe();
        verify(saveUserUseCase, times(1)).unsubscribe();
        verify(getStatesUseCase, times(1)).unsubscribe();
        verify(getSignUpComponentsUseCase, times(1)).unsubscribe();
    }
}
