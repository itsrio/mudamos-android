package com.inventos.plataformab.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListView;

import com.inventos.plataformab.BaseTestRunner;
import com.inventos.plataformab.BuildConfig;
import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.activity.ChangePasswordActivity;
import com.inventos.plataformab.presentation.view.activity.ConfigurationActivity;
import com.inventos.plataformab.presentation.view.activity.EditUserActivity;
import com.inventos.plataformab.presentation.view.activity.MainActivity;
import com.inventos.plataformab.presentation.view.activity.SignUpActivity;
import com.inventos.plataformab.presentation.view.activity.SplashActivity;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

@RunWith(BaseTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, shadows = {ShadowCrashlytics.class})
public class NavigationTest extends TestCase {

    @Test
    public void clickingSignUp_shouldStartSignUpActivity() {
        Activity activity = Robolectric.buildActivity(SplashActivity.class).create().
                start().
                resume().
                visible().get();
        activity.findViewById(R.id.signup_button).performClick();
        Intent expectedIntent = SignUpActivity.getCallingIntent(activity, null);
        assert(Shadows.shadowOf(activity).getNextStartedActivity()).equals(expectedIntent);
    }

    @Test
    public void clickingExplore_shouldStartMainActivity() {
        Activity activity = Robolectric.buildActivity(SplashActivity.class).create().
                start().
                resume().
                visible().get();
        activity.findViewById(R.id.explore_button).performClick();

        Intent expectedIntent = new Intent(activity, MainActivity.class);
        assert(Shadows.shadowOf(activity).getNextStartedActivity()).equals(expectedIntent);
    }

    @Test public void clickingEdit_shouldStartEditUserActivity() {
        Activity activity = Robolectric.buildActivity(ConfigurationActivity.class).create().
                start().
                resume().
                visible().get();
        ListView configItems = (ListView) activity.findViewById(R.id.rv_configurations);
        configItems.performItemClick(
                configItems.getAdapter().getView(0, null, null), 0,
                configItems.getAdapter().getItemId(0)
        );
        Intent expectedIntent = new Intent(activity, EditUserActivity.class);
        assert(Shadows.shadowOf(activity).getNextStartedActivity()).equals(expectedIntent);
    }

    @Test public void clickingChangePassword_shouldStartChangePasswordActivity() {
        Activity activity = Robolectric.buildActivity(ConfigurationActivity.class).create().
                start().
                resume().
                visible().get();
        ListView configItems = (ListView) activity.findViewById(R.id.rv_configurations);
        configItems.performItemClick(
                configItems.getAdapter().getView(1, null, null), 1,
                configItems.getAdapter().getItemId(1)
        );
        Intent expectedIntent = new Intent(activity, ChangePasswordActivity.class);
        assert(Shadows.shadowOf(activity).getNextStartedActivity()).equals(expectedIntent);
    }

}
