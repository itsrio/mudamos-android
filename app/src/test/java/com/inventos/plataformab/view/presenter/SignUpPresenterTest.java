package com.inventos.plataformab.view.presenter;

import android.test.AndroidTestCase;

import com.inventos.plataformab.domain.interactor.CreateUserUseCase;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetSignUpComponentsUseCase;
import com.inventos.plataformab.domain.interactor.GetStatesUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.signup.SignUpPresenter;
import com.inventos.plataformab.presentation.view.SignUpView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SignUpPresenterTest extends AndroidTestCase {

    @Mock private CreateUserUseCase createUserUseCase;
    @Mock private SaveUserUseCase saveUserUseCase;
    @Mock private GetStatesUseCase getStatesUseCase;
    @Mock private GetSignUpComponentsUseCase getSignUpComponentsUseCase;
    @Mock private SignUpView signUpView;
    @Mock private DefaultSubscriber subscriber;

    @InjectMocks SignUpPresenter presenter;

    @Override protected void setUp() throws Exception {
        super.setUp();
        presenter.setView(signUpView);
    }

    @Test
    public void testPresenterStart() {
        presenter.start();
        verify(signUpView, times(1)).loadLoginButtons();
        verify(signUpView, times(1)).hideMain();
        verify(signUpView, times(1)).showLoading();
        verify(signUpView, times(1)).hideRetry();
        verify(getStatesUseCase, times(1)).execute(any(DefaultSubscriber.class));
        verify(getSignUpComponentsUseCase, times(1)).execute(any(DefaultSubscriber.class));
    }

    @Test public void testPresenterCreateUser() {
        User dummyUser = new User();
        dummyUser.setPassword("123");
        when(signUpView.getNewUserFields()).thenReturn(dummyUser);
        presenter.createUser();
        verify(signUpView, times(1)).getNewUserFields();
        verify(createUserUseCase, times(1)).setUser(dummyUser);
        verify(createUserUseCase, times(1)).execute(any(DefaultSubscriber.class));
    }

    @Test public void testPresenterStop() {
        presenter.destroy();
        verify(createUserUseCase, times(1)).unsubscribe();
        verify(saveUserUseCase, times(1)).unsubscribe();
        verify(getStatesUseCase, times(1)).unsubscribe();
        verify(getSignUpComponentsUseCase, atMost(1)).unsubscribe();
    }

}