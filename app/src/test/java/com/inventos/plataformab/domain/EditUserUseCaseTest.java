package com.inventos.plataformab.domain;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.domain.interactor.CreateUserUseCase;
import com.inventos.plataformab.domain.interactor.EditUserUseCase;
import com.inventos.plataformab.presentation.model.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class EditUserUseCaseTest {
    @Mock
    private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private UserDataStore mockUserRepository;

    @InjectMocks EditUserUseCase editUserUseCase;

    User dummyUser;

    @Before
    public void setUp() {
        dummyUser = new User();
        this.editUserUseCase.setUser(dummyUser);
    }

    @Test
    public void testEditUserSuccessfully() {
        editUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).editUser(dummyUser);
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test
    public void testEditUserNull() {
        editUserUseCase.setUser(null);
        editUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).editUser(null);
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }
}
