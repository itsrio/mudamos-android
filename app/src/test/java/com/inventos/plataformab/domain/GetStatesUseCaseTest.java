package com.inventos.plataformab.domain;

import com.inventos.plataformab.data.repository.StateRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.domain.interactor.GetStatesUseCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class GetStatesUseCaseTest {
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private StateRepository stateRepository;

    @InjectMocks GetStatesUseCase getStatesUseCase;

    @Before
    public void setUp() {
        getStatesUseCase = new GetStatesUseCase(stateRepository, mockThreadExecutor,
                mockPostExecutionThread);
    }

    @Test public void testGetStates() {
        getStatesUseCase.buildUseCaseObservable();
        verify(stateRepository).getStates();
        verifyNoMoreInteractions(stateRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }
}
