package com.inventos.plataformab.domain;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.domain.interactor.CreateUserUseCase;
import com.inventos.plataformab.presentation.model.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class CreateUserUseCaseTest {
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private UserDataStore mockUserRepository;

    @InjectMocks CreateUserUseCase createUserUseCase;

    User dummyUser;

    @Before
    public void setUp() {
        createUserUseCase = new CreateUserUseCase(mockUserRepository, mockThreadExecutor,
                mockPostExecutionThread);
        dummyUser = new User();
        this.createUserUseCase.setUser(dummyUser);
    }

    @Test public void testCreateUserSuccessfully() {
        createUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).createUser(dummyUser, "");
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test public void testCreateUserTwitterSuccessfully() {
        createUserUseCase.setSocialNetwork("twitter");
        createUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).createUser(dummyUser, "twitter");
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test public void testCreateUserFacebookSuccessfully() {
        createUserUseCase.setSocialNetwork("facebook");
        createUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).createUser(dummyUser, "facebook");
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }
}
