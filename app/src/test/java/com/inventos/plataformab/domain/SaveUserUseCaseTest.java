package com.inventos.plataformab.domain;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SaveUserUseCaseTest {
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private UserDataStore mockUserRepository;
    @InjectMocks SaveUserUseCase saveUserUseCase;
    User dummyUser;

    @Before public void setUp() {
        saveUserUseCase = new SaveUserUseCase(mockUserRepository, mockThreadExecutor,
                mockPostExecutionThread);
        dummyUser = new User();
        this.saveUserUseCase.setUser(dummyUser);
    }

    @Test public void testSaveUserSuccessfully() {
        saveUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).saveUser(dummyUser);
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test public void testSaveUserNullUser() {
        saveUserUseCase.setUser(null);
        saveUserUseCase.buildUseCaseObservable();
        verify(mockUserRepository).saveUser(null);
        verifyNoMoreInteractions(mockUserRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }
}
