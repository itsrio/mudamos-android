package com.inventos.plataformab.domain;

import com.inventos.plataformab.data.repository.SignUpRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.domain.interactor.GetSignUpComponentsUseCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class GetSignUpComponentsUseCaseTest {

    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private SignUpRepository mockSignUpRepository;
    @InjectMocks GetSignUpComponentsUseCase getSignUpComponentsUseCase;

    @Before
    public void setUp() {
        getSignUpComponentsUseCase = new GetSignUpComponentsUseCase(mockSignUpRepository,
                mockThreadExecutor, mockPostExecutionThread);
    }

    @Test public void testGetSignUpComponentsUseCase() {
        getSignUpComponentsUseCase.buildUseCaseObservable();
        verify(mockSignUpRepository).getComponents();
        verifyNoMoreInteractions(mockSignUpRepository);
        verifyZeroInteractions(mockThreadExecutor);
        verifyZeroInteractions(mockPostExecutionThread);
    }
}
