package com.inventos.plataformab;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.res.Fs;

public class BaseTestRunner extends RobolectricGradleTestRunner {
    public BaseTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected AndroidManifest getAppManifest(Config config) {
        String manifest = "src/main/AndroidManifest.xml";
        String res = String.format("../app/build/intermediates/res/merged/%1$s/%2$s",
                BuildConfig.FLAVOR, BuildConfig.BUILD_TYPE);
        String asset = "src/main/assets";
        return new AndroidManifest(Fs.fileFromPath(manifest), Fs.fileFromPath(res),
                Fs.fileFromPath(asset));
    }
}
