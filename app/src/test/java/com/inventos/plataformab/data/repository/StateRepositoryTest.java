package com.inventos.plataformab.data.repository;

import android.test.AndroidTestCase;

import com.inventos.plataformab.data.repository.datasource.state.StateDataStore;
import com.inventos.plataformab.data.repository.datasource.state.StateDataStoreFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StateRepositoryTest extends AndroidTestCase {
    @Mock StateDataStore stateDataStore;
    @Mock StateDataStoreFactory stateDataStoreFactory;
    @InjectMocks StateRepository stateRepository;

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testGetStates() {
        when(stateDataStoreFactory.createDbDataStore()).thenReturn(stateDataStore);
        stateRepository.getStates();
        verify(stateDataStoreFactory, times(1)).createDbDataStore();
        verify(stateDataStore, times(1)).getStates();
        verifyNoMoreInteractions(stateDataStore);
        verifyNoMoreInteractions(stateDataStoreFactory);
    }

}
