package com.inventos.plataformab.data.repository;

import android.test.AndroidTestCase;

import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStore;
import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStoreFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommentRepositoryTest extends AndroidTestCase {
    @Mock CommentDataStore commentDataStore;
    @Mock CommentDataStoreFactory commentDataStoreFactory;
    @InjectMocks CommentRepository commentRepository;

    private int dummySubjectId = 10;
    private int dummyCommentId = 10;

    @Before public void initDataStore() {
        when(commentDataStoreFactory.createCloudDataStore()).thenReturn(commentDataStore);
    }

    @Test public void testGetComments() {
        int dummyPageNumber = 10;
        commentRepository.getComments(dummySubjectId, dummyCommentId, dummyPageNumber);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).getComments(dummySubjectId,
                dummyCommentId, dummyPageNumber);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }

    @Test public void testSendComment() {
        String dummyComment = "";
        commentRepository.sendComment(dummySubjectId, dummyCommentId, dummyComment,
                true);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).sendComment(dummySubjectId, dummyCommentId, dummyComment,
                true);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }

    @Test public void testSendCommentAsAnonymous() {
        String dummyComment = "";
        commentRepository.sendComment(dummySubjectId, dummyCommentId, dummyComment,
                false);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).sendComment(dummySubjectId, dummyCommentId, dummyComment,
                false);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }

    @Test public void testSendLike() {
        commentRepository.sendLike(dummySubjectId, dummyCommentId, true);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).sendLike(dummySubjectId, dummyCommentId, true);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }

    @Test public void testSendDislike() {
        commentRepository.sendLike(dummySubjectId, dummyCommentId, false);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).sendLike(dummySubjectId, dummyCommentId, false);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }

    @Test public void testRemoveLike() {
        commentRepository.removeLike(dummySubjectId, dummyCommentId, true);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).removeLike(dummySubjectId, dummyCommentId, true);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }

    @Test public void testRemoveDislike() {
        commentRepository.removeLike(dummySubjectId, dummyCommentId, false);
        verify(commentDataStoreFactory, times(1)).createCloudDataStore();
        verify(commentDataStore, times(1)).removeLike(dummySubjectId, dummyCommentId, false);
        verifyNoMoreInteractions(commentDataStore);
        verifyNoMoreInteractions(commentDataStoreFactory);
    }
}
