package com.inventos.plataformab.data.repository;

import android.test.AndroidTestCase;

import com.inventos.plataformab.data.repository.datasource.main.MainRequestDataStore;
import com.inventos.plataformab.data.repository.datasource.main.MainRequestDataStoreFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainRequestRepositoryTest extends AndroidTestCase {
    MainRequestDataStoreFactory mainRequestDataStoreFactory;
    MainRequestDataStore mainRequestDataStore;
    MainRequestRepository mainRequestRepository;

    @Test
    public void testGetMainRequestFromCloud() {
        mainRequestDataStoreFactory = mock(MainRequestDataStoreFactory.class);
        mainRequestDataStore = mock(MainRequestDataStore.class);
        when(mainRequestDataStoreFactory.createDbDataStore()).thenReturn(mainRequestDataStore);
        when(mainRequestDataStoreFactory.createCloudDataStore()).thenReturn(mainRequestDataStore);
        mainRequestRepository = new MainRequestRepository(mainRequestDataStoreFactory, true);
        verify(mainRequestDataStoreFactory, times(1)).createCloudDataStore();
        mainRequestRepository.getMainRequest();
        verify(mainRequestDataStore, times(1)).getMainRequest();
        verifyNoMoreInteractions(mainRequestDataStore);
        verifyNoMoreInteractions(mainRequestDataStoreFactory);
    }

    @Test
    public void testGetMainRequestFromDb() {
        mainRequestDataStoreFactory = mock(MainRequestDataStoreFactory.class);
        mainRequestDataStore = mock(MainRequestDataStore.class);
        when(mainRequestDataStoreFactory.createDbDataStore()).thenReturn(mainRequestDataStore);
        mainRequestRepository = new MainRequestRepository(mainRequestDataStoreFactory, false);
        verify(mainRequestDataStoreFactory, times(1)).createDbDataStore();
        mainRequestRepository.getMainRequest();
        verify(mainRequestDataStore, times(1)).getMainRequest();
        verifyNoMoreInteractions(mainRequestDataStore);
        verifyNoMoreInteractions(mainRequestDataStoreFactory);
    }
}
