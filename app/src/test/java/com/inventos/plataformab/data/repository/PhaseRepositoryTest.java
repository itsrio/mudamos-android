package com.inventos.plataformab.data.repository;

import android.test.AndroidTestCase;

import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStore;
import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStoreFactory;
import com.inventos.plataformab.presentation.model.Cycle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhaseRepositoryTest extends AndroidTestCase {
    @Mock PhaseDataStoreFactory phaseDataStoreFactory;
    @Mock PhaseDataStore phaseDataStore;
    @InjectMocks PhaseRepository phaseRepository;

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testPhasesByCycle() {
        verify(phaseDataStoreFactory, times(1)).createDbDataStore();
        Cycle dummyCycle = new Cycle();
        phaseRepository.phasesByCycle(dummyCycle);
        verify(phaseDataStore, times(1)).phasesByCycle(dummyCycle);
        verifyNoMoreInteractions(phaseDataStore);
        verifyNoMoreInteractions(phaseDataStoreFactory);
    }

    @Test
    public void testPhasesById() {
        int dummyId = 5;
        when(phaseDataStoreFactory.createDbDataStore()).thenReturn(phaseDataStore);
        phaseRepository.phaseById(dummyId);
        verify(phaseDataStoreFactory, times(1)).createDbDataStore();
        verify(phaseDataStore, times(1)).phaseById(dummyId);
        verifyNoMoreInteractions(phaseDataStore);
        verifyNoMoreInteractions(phaseDataStoreFactory);
    }
}
