package com.inventos.plataformab.data.repository;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.test.AndroidTestCase;

import com.inventos.plataformab.data.repository.datasource.cycle.CycleDataStore;
import com.inventos.plataformab.data.repository.datasource.cycle.CycleDataStoreFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CycleRepositoryTest extends AndroidTestCase {
    @Mock ConnectivityManager connectivityManager;
    @Mock NetworkInfo networkInfo;
    @Mock Context context;
    @Mock CycleDataStoreFactory cycleDataStoreFactory;
    @Mock CycleDataStore cycleDataStore;
    CycleRepository cycleRepository;

    @Before public void setFactoryCalls() {
        when(context.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connectivityManager);
        when(cycleDataStoreFactory.createDbDataStore()).thenReturn(cycleDataStore);
        when(cycleDataStoreFactory.createCloudDataStore()).thenReturn(cycleDataStore);
        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);
    }

    @Test
    public void testGetCyclesFromCloud() {
        when(networkInfo.isConnectedOrConnecting()).thenReturn(true);
        cycleRepository = new CycleRepository(cycleDataStoreFactory, context);
        cycleRepository.cycles();
        verify(cycleDataStoreFactory, times(1)).createCloudDataStore();
        verify(cycleDataStore, times(1)).cycles();
        verifyNoMoreInteractions(cycleDataStoreFactory);
        verifyNoMoreInteractions(cycleDataStore);
    }

    @Test
    public void testGetCyclesFromDb() {
        when(networkInfo.isConnectedOrConnecting()).thenReturn(false);
        cycleRepository = new CycleRepository(cycleDataStoreFactory, context);
        cycleRepository.cycles();
        verify(cycleDataStoreFactory, times(1)).createDbDataStore();
        verify(cycleDataStore, times(1)).cycles();
        verifyNoMoreInteractions(cycleDataStoreFactory);
        verifyNoMoreInteractions(cycleDataStore);
    }

    @Test
    public void saveCyclesInCloud() {
        when(networkInfo.isConnectedOrConnecting()).thenReturn(true);
        cycleRepository = new CycleRepository(cycleDataStoreFactory, context);
        cycleRepository.cycles();
        verify(cycleDataStoreFactory, times(1)).createCloudDataStore();
        verify(cycleDataStore, times(1)).cycles();
        verifyNoMoreInteractions(cycleDataStoreFactory);
        verifyNoMoreInteractions(cycleDataStore);
    }

    @Test
    public void saveCyclesInDb() {
        when(networkInfo.isConnectedOrConnecting()).thenReturn(false);
        cycleRepository = new CycleRepository(cycleDataStoreFactory, context);
        cycleRepository.cycles();
        verify(cycleDataStoreFactory, times(1)).createDbDataStore();
        verify(cycleDataStore, times(1)).cycles();
        verifyNoMoreInteractions(cycleDataStoreFactory);
        verifyNoMoreInteractions(cycleDataStore);
    }
}
