package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.fragment.CommentFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.FragmentFactory;

public class CommentActivity extends BaseActivity implements CommentFragment.CommentListener {
    public static String COMMENT_FRAGMENT_TAG = "comment.fragment";
    public static String INTENT_EXTRA_PARAM_SUBJECT_ID = "param.subject.id";
    private static final String INTENT_EXTRA_PARAM_CYCLE_COLOR= "param.cycle.color";
    private static final String INTENT_EXTRA_PARAM_COMMENT_PARENT_LIST = "param.comment.parent";
    private static final String INTENT_EXTRA_PARAM_HAS_ANSWERED = "param.has.answered";
    private static final String INTENT_EXTRA_PARAM_IS_ANONYMOUS = "param.is.anonymous";
    private static final String INTENT_EXTRA_PARAM_PHASE_HAS_ENDED = "param.phase.has.ended";

    public static int COMMENT_ACTIVITY_REQUEST_CODE = 1;
    @Bind(R.id.toolbar) Toolbar mToolbar;

    private int mToolbarColor;
    private int subjectId;
    private ArrayList<Integer> mParents;
    private boolean hasAnswered;
    private boolean isAnonymous;
    private boolean phaseHasEnded;


    public static Intent getCallingIntent(Context context, int subjectId, int cycleColor, boolean phaseHasEnded) {
        Intent callingIntent = new Intent(context, CommentActivity.class);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_SUBJECT_ID , subjectId);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_CYCLE_COLOR, cycleColor);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_PHASE_HAS_ENDED, phaseHasEnded);
        return callingIntent;
    }

    public static Intent getCallingIntent(Context context, int subjectId, int cycleColor,
                                          ArrayList<Integer> commentParent, boolean hasAnswered,
                                          boolean isAnonymous, boolean phaseHasEnded) {
        Intent callingIntent = new Intent(context, CommentActivity.class);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_SUBJECT_ID, subjectId);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_CYCLE_COLOR, cycleColor);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_COMMENT_PARENT_LIST, commentParent);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_HAS_ANSWERED, hasAnswered);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_IS_ANONYMOUS, isAnonymous);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_PHASE_HAS_ENDED, phaseHasEnded);
        return callingIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "CommentActivity";
        super.onCreate(savedInstanceState);
        //instantiate callbacks manager
        setContentView(R.layout.activity_subject);
        initializeActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COMMENT_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            this.hasAnswered = data.getBooleanExtra(INTENT_EXTRA_PARAM_HAS_ANSWERED,
                    this.hasAnswered);
            this.isAnonymous = data.getBooleanExtra(INTENT_EXTRA_PARAM_IS_ANONYMOUS,
                    this.isAnonymous);
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(COMMENT_FRAGMENT_TAG);
            if (fragment instanceof CommentFragment) {
                ((CommentFragment) fragment).setCommentConfigFromChildActivity
                        (this.hasAnswered, this.isAnonymous);
            }
        }
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        initParents();
        initToolbar();
        addFragment(R.id.fragment_container, FragmentFactory.newCommentFragment(subjectId, mParents,
                mToolbarColor, hasAnswered, isAnonymous, phaseHasEnded), COMMENT_FRAGMENT_TAG);
    }

    private void initParents() {
        mParents = getIntent().getIntegerArrayListExtra(INTENT_EXTRA_PARAM_COMMENT_PARENT_LIST);
        if (mParents == null) {
            mParents = new ArrayList<>();
        }
        hasAnswered = getIntent().getBooleanExtra(INTENT_EXTRA_PARAM_HAS_ANSWERED, false);
        isAnonymous = getIntent().getBooleanExtra(INTENT_EXTRA_PARAM_IS_ANONYMOUS, false);
        phaseHasEnded = getIntent().getBooleanExtra(INTENT_EXTRA_PARAM_PHASE_HAS_ENDED, false);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Intent intent = getIntent();
        subjectId = intent.getIntExtra(INTENT_EXTRA_PARAM_SUBJECT_ID, 0);
        mToolbarColor = intent.getIntExtra(INTENT_EXTRA_PARAM_CYCLE_COLOR, R.color.toolbar_color);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (mParents.size() == 0) {
                getSupportActionBar().setTitle(R.string.subject_list);
            }
            else {
                getSupportActionBar().setTitle(R.string.comments);
            }
        }
        mToolbar.setBackgroundColor(mToolbarColor);
    }

    @Override
    public void openCommentChildren(int subjectId, ArrayList<Integer> parents, boolean hasAnswered, boolean isAnonymous) {
        this.navigator.navigateToCommentActivity(this, subjectId, mToolbarColor, parents, hasAnswered, isAnonymous, phaseHasEnded);
    }

    @Override
    public void sendInfoToParent(boolean hasAnswered, boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
        this.hasAnswered = hasAnswered;
        Intent intent = new Intent();
        intent.putExtra(INTENT_EXTRA_PARAM_HAS_ANSWERED, hasAnswered);
        intent.putExtra(INTENT_EXTRA_PARAM_IS_ANONYMOUS, hasAnswered);
        setResult(RESULT_OK, intent);
    }

    @Override
    public void goToLogin() {
        this.navigator.navigateToSplashActivityClearTop(this);
    }
}
