package com.inventos.plataformab.presentation.view.adapter.configuration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inventos.plataformab.R;

import java.util.ArrayList;

public class ConfigurationAdapter extends ArrayAdapter<String> implements AdapterView.OnItemClickListener {

    private static final int EDIT_PROFILE = 0;
    private static final int CHANGE_PASSWORD = 1;

    private ArrayList<String> configOptions;
    private final Context context;
    private final LayoutInflater layoutInflater;
    private ItemClickListener itemClickListener;

    public interface ItemClickListener {
        void openProfileEdit();
        void openChangePassword();
    }


    public ConfigurationAdapter(Context context, ArrayList<String> options) {
        super(context, -1, options);
        configOptions = new ArrayList<>();
        configOptions.add("Editar"); configOptions.add("Senha");
        this.layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    public void setItemClickListener(ItemClickListener listener) {
        itemClickListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView =
                layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        TextView textView = (TextView) rowView.findViewById(android.R.id.text1);
        textView.setText(configOptions.get(position));
        textView.setTextColor(context.getResources().getColor(R.color.comment));
        return rowView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (itemClickListener != null) {
            switch (position) {
                case EDIT_PROFILE:
                    itemClickListener.openProfileEdit();
                    break;
                case CHANGE_PASSWORD:
                    itemClickListener.openChangePassword();
                    break;
            }
        }
    }


}
