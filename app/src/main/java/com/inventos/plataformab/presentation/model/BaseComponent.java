package com.inventos.plataformab.presentation.model;

import java.util.ArrayList;
import java.util.HashMap;

public class BaseComponent {
    private HashMap<String, ArrayList<String>>  errors;

    public HashMap<String, ArrayList<String>> getErrors() {
        return errors;
    }

    public void setErrors(HashMap<String, ArrayList<String>> errors) {
        this.errors = errors;
    }
}
