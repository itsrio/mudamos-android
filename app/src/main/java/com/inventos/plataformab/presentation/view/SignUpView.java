package com.inventos.plataformab.presentation.view;

import com.facebook.CallbackManager;
import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.State;
import com.inventos.plataformab.presentation.model.User;

import org.json.JSONObject;

import java.util.List;

public interface SignUpView extends LoadDataView {
    void loadLoginButtons();
    User getNewUserFields();
    void redirectToMain();
    void renderStateAndCitySpinners(List<State> stateList);
    void renderSignUpComponents(SignUpComponents components);
    void hideMain();
    void showMain();
    void fillUserFields(JSONObject object);
}