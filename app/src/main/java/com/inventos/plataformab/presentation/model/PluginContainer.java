package com.inventos.plataformab.presentation.model;

import java.util.ArrayList;
import java.util.List;

public class PluginContainer extends BaseComponent {
    protected List<Subject> subjects;
    //protected List<BlogPosts> subjects;


    public List<Subject> getSubjects() {
        if (this.subjects == null)
            this.subjects = new ArrayList<>();
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
