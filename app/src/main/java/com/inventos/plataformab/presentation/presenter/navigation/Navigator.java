package com.inventos.plataformab.presentation.presenter.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.inventos.plataformab.presentation.model.Subject;
import com.inventos.plataformab.presentation.view.activity.ChangePasswordActivity;
import com.inventos.plataformab.presentation.view.activity.CommentActivity;
import com.inventos.plataformab.presentation.view.activity.ConfigurationActivity;
import com.inventos.plataformab.presentation.view.activity.EditUserActivity;
import com.inventos.plataformab.presentation.view.activity.MainActivity;
import com.inventos.plataformab.presentation.view.activity.NotificationsActivity;
import com.inventos.plataformab.presentation.view.activity.PhaseActivity;
import com.inventos.plataformab.presentation.view.activity.SignUpActivity;
import com.inventos.plataformab.presentation.view.activity.SplashActivity;

import java.util.ArrayList;

public class Navigator {

    Gson gson;

    public Navigator(){
        this.gson = new Gson();
    }

    public void navigateToSignupActivity(Context context, String userJson) {
        if (context != null) {
            Intent intentToLaunch = SignUpActivity.getCallingIntent(context,userJson);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToMainActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = MainActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToMainActivity(Context context, String user) {
        if (context != null && user != null) {
            Intent intentToLaunch = MainActivity.getCallingIntent(context);
            intentToLaunch.putExtra(MainActivity.USER_EXTRA, user);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToMainActivityClearTop(Context context) {
        if (context != null) {
            Intent intentToLaunch = MainActivity.getCallingIntent(context);
            intentToLaunch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToSplashActivityClearTop(Context context) {
        if (context != null) {
            Intent intentToLaunch = SplashActivity.getCallingIntent(context);
            intentToLaunch.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToPhaseActivity(Context context, int phaseId, String cycleName,
                                        int cycleColor) {
        if (context != null && phaseId != 0) {
            Intent intentToLaunch = PhaseActivity.getCallingIntent(context, phaseId, cycleName,
                    cycleColor);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToCommentActivity(Context context, Subject subject, int cycleColor, boolean phaseHasEnded) {
        int subjectId = subject.getId();
        if (context != null && subjectId != 0) {
            Intent intentToLaunch = CommentActivity.getCallingIntent(context, subjectId, cycleColor, phaseHasEnded);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToCommentActivity(Context context, int subjectId, int cycleColor,
                                          ArrayList<Integer> parentComments, boolean hasAnswered,
                                          boolean isAnonymous, boolean phaseHasEnded) {
        if (context != null && subjectId != 0) {
            Intent intentToLaunch = CommentActivity.getCallingIntent(
                    context, subjectId, cycleColor, parentComments, hasAnswered, isAnonymous, phaseHasEnded);
            ((Activity) context).startActivityForResult(intentToLaunch,
                    CommentActivity.COMMENT_ACTIVITY_REQUEST_CODE);
        }
    }

    public void navigateToConfigurationActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = ConfigurationActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigatoToChangePasswordActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = ChangePasswordActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToEditUserActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = EditUserActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToNotificationsActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = NotificationsActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }
}