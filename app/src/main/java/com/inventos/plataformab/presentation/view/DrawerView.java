package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.User;

public interface DrawerView {
    void login();
    String getUser();
    User getUserAsObject();
    void logoutUser();
    void updateNavigationView();
    void initContentView();
    void showTerms();
    void openConfigurationActivity();
    void openBlogActivity();
    void openSignUpActivity();
    void openNotificationsActivity();
}
