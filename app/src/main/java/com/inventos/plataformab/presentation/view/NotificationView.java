package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.Notifications;

import java.util.List;

public interface NotificationView extends LoadDataView {
    void hideMain();
    void showMain();
    void addNotifications(List<Notifications> notifications);
}
