package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.fragment.CropperFragment;
import com.inventos.plataformab.presentation.view.fragment.EditUserFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.FragmentFactory;

public class EditUserActivity extends BaseActivity implements EditUserFragment.EditUserListener, CropperFragment.CropListener {
    public static String EDIT_USER_FRAGMENT_FLAG = "edit.user.fragment";

    @Bind(R.id.toolbar) Toolbar mToolbar;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, EditUserActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initializeActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EditUserFragment fragment = (EditUserFragment)
                getSupportFragmentManager().findFragmentByTag(EDIT_USER_FRAGMENT_FLAG);
        if (fragment != null && fragment.isVisible()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.edit_user));
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFragment(R.id.fragment_container, FragmentFactory.newEditUserFragment(this),
                EDIT_USER_FRAGMENT_FLAG);
    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    @Override
    public void showImageDialog() {
        final CharSequence[] items = { // "Tirar foto",
                "Escolher da galeria", "Cancelar" };
        AlertDialog.Builder builder = new AlertDialog.Builder(EditUserActivity.this);
        builder.setTitle("Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Intent intent;
                switch (item) {
                    case 0:
//                        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        EditUserActivity.this.startActivityForResult(intent, CropperFragment.REQUEST_CAMERA);
//                        break;
//                    case 1:
                        intent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        EditUserActivity.this.startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                CropperFragment.SELECT_FILE);
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        builder.show();
    }
}
