package com.inventos.plataformab.presentation.view.fragment;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.view.activity.BaseActivity;

public class BaseFragment extends Fragment {

    ProgressDialog mProgressDialog;

    protected void showToastMessage(String message) {
        ((BaseActivity) getActivity()).showToastMessage(message);
    }

    protected void showToastMessageWithLongDuration(String message) {
        ((BaseActivity) getActivity()).showToastMessageWithLongDuration(message);
    }

    protected User getLoggedUser() {
        return ((BaseActivity) this.getActivity()).getLoggedUserAsObject();
    }

    public void showProgressDialog(String title, String message){
        title =  (title == null) ? "" : title;
        mProgressDialog = ProgressDialog.show(this.getActivity(), title, message, true);
    }

    public void showProgressDialog(int title, int messageId){
        String titleMessage = (title == 0) ? "" : getString(title);
        String message = getString(messageId);
        mProgressDialog = ProgressDialog.show(this.getActivity(), titleMessage, message, true);
    }

    public void showProgressDialog(int messageId){
        String message = getString(messageId);
        mProgressDialog = ProgressDialog.show(this.getActivity(), "", message, true);
    }

    public void dismissProgressDialog(){
        if (mProgressDialog != null){
            mProgressDialog.dismiss();
        }
    }

    protected void hideKeyboard() {
        ((BaseActivity) getActivity()).hideKeyboard();
    }
}
