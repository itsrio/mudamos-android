package com.inventos.plataformab.presentation.view.adapter.about;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.About;
import com.inventos.plataformab.presentation.model.Settings;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AboutCardAdapter extends RecyclerView.Adapter<AboutCardAdapter.AboutViewHolder> {

    private Settings settings;
    private final LayoutInflater layoutInflater;

    public AboutCardAdapter(Context context, Settings settings) {
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.settings = settings;
    }

    @Override
    public AboutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.row_about, parent, false);
        return new AboutViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AboutViewHolder holder, int position) {
        final String title = this.settings.getTitles().get(position);
        final String content = this.settings.getContent().get(position);
        holder.cardTitle.setText(title.toUpperCase());
        holder.cardDescription.setText(getFixedLinks(content));
        holder.cardDescription.setClickable(true);
        holder.cardDescription.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private Spannable getFixedLinks(String content) {
        Spannable spannable = (Spannable) Html.fromHtml(content);
        for (URLSpan u: spannable.getSpans(0, spannable.length(), URLSpan.class)) {
            spannable.setSpan(new UnderlineSpan() {
                public void updateDrawState(TextPaint tp) {
                    tp.setUnderlineText(false);
                }
            }, spannable.getSpanStart(u), spannable.getSpanEnd(u), 0);
        }
        return spannable;
    }

    @Override
    public int getItemCount() {
        return (this.settings != null) ? this.settings.getTitles().size() : 0;
    }

    static class AboutViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.card_title) TextView cardTitle;
        @Bind(R.id.card_description) TextView cardDescription;

        public AboutViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
