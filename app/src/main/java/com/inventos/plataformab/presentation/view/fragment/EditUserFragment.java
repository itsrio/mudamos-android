package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.City;
import com.inventos.plataformab.presentation.model.Profile;
import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.State;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.user.EditUserPresenter;
import com.inventos.plataformab.presentation.view.EditUserView;
import com.inventos.plataformab.presentation.view.adapter.profile.ProfileSpinnerAdapter;
import com.inventos.plataformab.presentation.view.adapter.state.CitySpinnerAdapter;
import com.inventos.plataformab.presentation.view.adapter.state.StateSpinnerAdapter;
import com.inventos.plataformab.presentation.view.widget.LabelledSpinner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import factory.PresenterFactory;

public class EditUserFragment extends CropperFragment implements EditUserView {
    EditUserPresenter editUserPresenter;
    EditUserListener editUserListener;
    StateSpinnerAdapter stateSpinnerAdapter;
    CitySpinnerAdapter citySpinnerAdapter;
    ProfileSpinnerAdapter profileSpinnerAdapter;
    ProfileSpinnerAdapter subProfileSpinnerAdapter;
    HashMap<String, Integer> genders;
    User user;

    int mStatePosition;
    int mSubprofilePosition;
    int mProfilePosition;
    int mGenderPosition;

    public interface EditUserListener {
        void finishActivity();
    }

    @Bind(R.id.main_view) LinearLayout main_view;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;
    @Bind(R.id.username_edit_text) EditText username_edit_text;
    @Bind(R.id.password_edit_text) EditText password_edit_text;
    @Bind(R.id.new_email_edit_text) EditText email_edit_text;
    @Bind(R.id.password_confirmation_edit_text) EditText password_confirmation_edit_text;
    @Bind(R.id.birth_date_edit_text) EditText birth_date_edit_text;
    @Bind(R.id.state_spinner) LabelledSpinner state_spinner;
    @Bind(R.id.gender_spinner) LabelledSpinner gender_spinner;
    @Bind(R.id.city_text_view) AutoCompleteTextView city_text_view;
    @Bind(R.id.profile_spinner) LabelledSpinner profile_spinner;
    @Bind(R.id.sub_profile_spinner) LabelledSpinner sub_profile_spinner;
    @Bind(R.id.profile_caption_text_view) TextView profile_caption_text_view;
    @Bind(R.id.alias_input_container) RelativeLayout alias_input_container;
    @Bind(R.id.submit_button) Button submit_button;
    @Bind(R.id.terms_and_conditions_container) RelativeLayout terms_and_conditions_container;
    @Bind(R.id.iv_image_container) ImageView iv_image_container;
    @Bind(R.id.bt_image_plus) Button bt_image_plus;
    @Bind(R.id.iv_icon) ImageView iv_icon;


    public EditUserFragment() { super(); }

    public static EditUserFragment newInstance() {
        EditUserFragment editUserFragment = new EditUserFragment();
        Bundle argumentsBundle = new Bundle();
        editUserFragment.setArguments(argumentsBundle);
        return editUserFragment;
    }

    public void setPresenter(@NonNull EditUserPresenter presenter) { editUserPresenter = presenter; }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, fragmentView);
        user = getLoggedUser();
        loadProfileThumb();
        return fragmentView;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (editUserPresenter == null) editUserPresenter =
                PresenterFactory.newEditUserPresenter(this.getActivity());
        editUserPresenter.setView(this);
        editUserPresenter.start();
    }


    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof EditUserListener) {
            this.editUserListener = (EditUserListener) activity;
        }
    }

    @Override public void onStart() {
        super.onStart();
        hideNewUserFields();
    }

    private void loadProfileThumb() {
        Picasso.with(this.getActivity())
                .load(getLoggedUser().getPicture()).into(iv_image_container);
        iv_image_container.setVisibility(View.VISIBLE);
        bt_image_plus.setVisibility(View.GONE);
        iv_icon.setVisibility(View.GONE);
    }

    public void fillUserFields() {
        username_edit_text.setText(user.getName());
        email_edit_text.setText(user.getEmail());
        birth_date_edit_text.setText(user.getBirthday());
        profile_spinner.getSpinner().setSelection(mProfilePosition);
        if (user.getSubProfile() != null){
            sub_profile_spinner.getSpinner().setSelection(mSubprofilePosition);
        }
        else {
            sub_profile_spinner.setVisibility(View.GONE);
        }
        gender_spinner.getSpinner().setSelection(mGenderPosition);
    }

    private void hideNewUserFields() {
        password_edit_text.setVisibility(View.GONE);
        password_confirmation_edit_text.setVisibility(View.GONE);
        alias_input_container.setVisibility(View.GONE);
        submit_button.setText(getResources().getText(R.string.save));
        terms_and_conditions_container.setVisibility(View.GONE);
    }

    @Override public void onResume() {
        super.onResume();
        editUserPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        editUserPresenter.pause();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        editUserPresenter.destroy();
    }

    @Override
    public User getNewUserFields() {
        String newName = username_edit_text.getText().toString();
        String newEmail = email_edit_text.getText().toString();
        String newState = state_spinner.getSpinner().getSelectedItem().toString();
        String newCity = city_text_view.getText().toString();
        String newBirthday = birth_date_edit_text.getText().toString();
        Profile newProfile = (Profile) profile_spinner.getSpinner().getSelectedItem();
        Profile newSubProfile = null;
        if (sub_profile_spinner.getVisibility() == View.VISIBLE) {
            newSubProfile = (Profile) sub_profile_spinner.getSpinner().getSelectedItem();
        }
        String newGender =
                this.genders.get(gender_spinner.getSpinner().getSelectedItem().toString()).toString();

        User updatedUser = new User();
        updatedUser.setPassword(user.getPassword());

        if (!user.getName().equals(newName)) updatedUser.setName(newName);
        if (!user.getEmail().equals(newEmail)) updatedUser.setEmail(newEmail);
        if (!user.getState().equals(newState)) updatedUser.setState(newState);
        if (!user.getCity().equals(newCity)) updatedUser.setCity(newCity);
        if (!user.getBirthday().equals(newBirthday)) updatedUser.setBirthday(newBirthday);
        if (!user.getProfile().equals(newProfile)) updatedUser.setProfile(newProfile);
        if (newSubProfile != null) {
            if (user.getSubProfile() == null || ! user.getSubProfile().equals(newSubProfile)) {
                updatedUser.setSubProfile(newSubProfile);
            }
        }
        if (!user.getGender().equals(newGender)) updatedUser.setGender(newGender);

        if (newProfileImage != null) {
            updatedUser.setPicture(newProfileImage);
        }

        return updatedUser;
    }

    @Override
    public void renderStateAndCitySpinners(List<State> stateList) {
         stateSpinnerAdapter = new StateSpinnerAdapter(this.getActivity(), stateList);
        mStatePosition = stateSpinnerAdapter.getSelectedState(user.getState());
        state_spinner.setCustomAdapter(stateSpinnerAdapter);
        citySpinnerAdapter = new CitySpinnerAdapter(this.getActivity(), new ArrayList<City>());
        city_text_view.setAdapter(citySpinnerAdapter);
        citySpinnerAdapter.setCityList(
                stateSpinnerAdapter.getStateList().get(mStatePosition).getCities());
        state_spinner.getSpinner().setSelection(mStatePosition);
        city_text_view.setText(user.getCity());
        state_spinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                State state = stateSpinnerAdapter.getStateList().get(position);
                if (! user.getState().equals(state.getUf()) && !user.getState().equals(state.getName()) ){
                    city_text_view.setText("");
                }
                citySpinnerAdapter.setCityList(
                        stateSpinnerAdapter.getStateList().get(position).getCities());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void renderSignUpComponents(SignUpComponents components) {
        initProfileSpinners(components.getProfiles());
        initGenderSpinner(components.getGenders());
        fillUserFields();
    }

    private void initGenderSpinner(HashMap<String, Integer> genders) {
        this.genders = genders;
        List<String> keys = new ArrayList<>(genders.keySet());
        for (Map.Entry<String, Integer> entry : genders.entrySet()) {
            if (entry.getValue().toString().equals(user.getGender())) {
                break;
            }
            mGenderPosition++;
        }
        mGenderPosition = keys.indexOf(user.getGender());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, keys);
        gender_spinner.setCustomAdapter(adapter);
    }

    public void initProfileSpinners(List<Profile> profiles) {
        profileSpinnerAdapter = new ProfileSpinnerAdapter(this.getActivity(), profiles);
        mProfilePosition = profileSpinnerAdapter.getSelectedProfile(user.getProfile().getId());
        profile_spinner.setCustomAdapter(profileSpinnerAdapter);
        profile_spinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                EditUserFragment.this.initSubProfileSpinner(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        subProfileSpinnerAdapter = new ProfileSpinnerAdapter(this.getActivity(),
                new ArrayList<Profile>());
        sub_profile_spinner.setCustomAdapter(subProfileSpinnerAdapter);
        if (user.getSubProfile() != null) {
            this.initSubProfileSpinner(mProfilePosition);
            this.showSubProfile();
        }
        else {
            this.hideSubProfile();
        }
    }

    private void initSubProfileSpinner(int position) {
        Profile currentProfile = profileSpinnerAdapter.getProfileList().get(position);
        profile_caption_text_view.setVisibility(View.VISIBLE);
        profile_caption_text_view.setText(currentProfile.getDescription());
        subProfileSpinnerAdapter.setProfileList(currentProfile.getChildren());
        if (currentProfile.getChildrenCount() > 0) EditUserFragment.this.showSubProfile();

        else EditUserFragment.this.hideSubProfile();
    }

    @OnClick(R.id.submit_button) void onCreateUserButtonClick() {
        User updatedUser = getNewUserFields();
        if (updatedUser != null) {
            editUserPresenter.editUser(updatedUser);
        }
    }
    @OnClick(R.id.retry_button) void retryConnection() {
        editUserPresenter.start();
    }

    @OnFocusChange(R.id.birth_date_edit_text) void onDateFocus() {
        if (birth_date_edit_text.isFocused()) {
            setDate();
        }
    }

    @OnClick(R.id.birth_date_edit_text) void onDateClick() {
        setDate();
    }

    private void setDate() {
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setEditText(birth_date_edit_text);
            newFragment.show(getActivity().getSupportFragmentManager(), "date picker");
    }

    private void hideSubProfile() {
        sub_profile_spinner.setVisibility(View.GONE);
    }

    private void showSubProfile() {
        sub_profile_spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMain() {
        main_view.setVisibility(View.GONE);
    }

    @Override
    public void showMain() {
        main_view.setVisibility(View.VISIBLE);
    }

    @Override public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public void redirectToMain() {
        editUserListener.finishActivity();
    }

}
