package com.inventos.plataformab.presentation.util;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.inventos.plataformab.presentation.view.activity.BaseActivity;

import org.json.JSONObject;

import java.util.Arrays;

public class FacebookHelper {

    public interface FacebookListener {
        void onUserFetched(JSONObject user);
        void onError();
        void onCancel();
    }

    public CallbackManager facebookCallbackManager;
    private LoginResult loginResult;
    private LoginManager loginManager;
    private FacebookListener listener;
    private Activity activity;

    public FacebookHelper(CallbackManager manager, LoginManager loginManager, BaseActivity activity) {
        this.loginManager = loginManager;
        this.facebookCallbackManager = manager;
        this.activity = activity;
    }

    public CallbackManager getFacebookCallbackManager() {
        return facebookCallbackManager;
    }

    public LoginResult getLoginResult() {
        return loginResult;
    }

    public void setListener(FacebookListener listener) {
        this.listener = listener;
    }

    public void startAuthentication() {
        this.loginManager.registerCallback(facebookCallbackManager, new FacebookCallback());
        loginManager.logInWithReadPermissions(
                activity, Arrays.asList("public_profile","email", "user_birthday"));
    }

    public class FacebookCallback implements com.facebook.FacebookCallback<LoginResult> {
        @Override
        public void onSuccess(LoginResult loginResult) {
            FacebookHelper.this.loginResult = loginResult;
            getFbUserInfo();
        }

        @Override
        public void onCancel() {
            listener.onCancel();
        }

        @Override
        public void onError(FacebookException exception) {
            listener.onError();
        }
    }

    public class UserJsonObjectCallback implements GraphRequest.GraphJSONObjectCallback {
        @Override
        public void onCompleted(JSONObject object, GraphResponse response) {
            //Profile userProfile = Profile.getCurrentProfile();
            listener.onUserFetched(object);
        }
    }

    private void getFbUserInfo(){
        GraphRequest request  = GraphRequest.newMeRequest(
                this.loginResult.getAccessToken(), new UserJsonObjectCallback());
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
