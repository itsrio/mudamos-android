package com.inventos.plataformab.presentation.view;

public interface ChangePasswordView {
    String getNewPassword(String newPassword);
    void showErrorMessage(String message);
    void finishActivity();

}
