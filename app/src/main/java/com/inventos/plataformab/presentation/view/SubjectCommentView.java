package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.Comment;

import java.util.List;

public interface SubjectCommentView {
    void renderComments(List<Comment> comments);
}
