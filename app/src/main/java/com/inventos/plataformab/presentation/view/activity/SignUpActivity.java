package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.fragment.CropperFragment;
import com.inventos.plataformab.presentation.view.fragment.SignUpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.FragmentFactory;

public class SignUpActivity extends BaseActivity implements SignUpFragment.SignUpListener,
        CropperFragment.CropListener {
    public static String SIGN_UP_FRAGMENT_FLAG = "signup.fragment";
    public static String USER_EXTRA = "user";

    @Bind(R.id.toolbar) Toolbar mToolbar;

    public static Intent getCallingIntent(Context context, String userJson) {
        Intent intentToLaunch = new Intent(context, SignUpActivity.class);
        intentToLaunch.putExtra(USER_EXTRA, userJson);
        return intentToLaunch;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "SignUpActivity";
        super.onCreate(savedInstanceState);
        //instantiate callbacks manager
        setContentView(R.layout.activity_signup);
        initializeActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SignUpFragment signUpFragment = (SignUpFragment)
                getSupportFragmentManager().findFragmentByTag(SIGN_UP_FRAGMENT_FLAG);
        if (signUpFragment != null && signUpFragment.isVisible()) {
            signUpFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.sign_in));
        String userJson = getIntent().getExtras().getString(USER_EXTRA, null);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFragment(R.id.fragment_container, FragmentFactory.newSignUpFragment(this, userJson),
                SIGN_UP_FRAGMENT_FLAG);
    }

    @Override
    public void redirectToMain() {
        this.navigator.navigateToMainActivityClearTop(this);
    }

    @Override
    public void showImageDialog() {
        final CharSequence[] items = { "Tirar foto", "Escolher da galeria", "Cancelar" };
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        builder.setTitle("Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                Intent intent;
                switch (item) {
                    case 0:
                        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        SignUpActivity.this.startActivityForResult(intent, CropperFragment.REQUEST_CAMERA);
                        break;
                    case 1:
                        intent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        SignUpActivity.this.startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                CropperFragment.SELECT_FILE);
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        builder.show();
    }

}
