package com.inventos.plataformab.presentation.view.listener;

import android.support.design.widget.NavigationView;
import android.view.MenuItem;

import com.inventos.plataformab.presentation.view.activity.BaseActivity;

public class DrawerSelectedListener implements NavigationView.OnNavigationItemSelectedListener {

    BaseActivity mActivity;

    public DrawerSelectedListener(BaseActivity activity) {
        mActivity = activity;
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        return false;
    }
}
