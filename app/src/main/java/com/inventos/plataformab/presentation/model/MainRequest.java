package com.inventos.plataformab.presentation.model;

import java.util.List;

public class MainRequest extends BaseComponent {
    // A wrapper to parse all stuff inside main app request
    List<Cycle> cycles;
    Settings settings;

    public List<Cycle> getCycles() {
        return cycles;
    }

    public void setCycles(List<Cycle> cycles) {
        this.cycles = cycles;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}
