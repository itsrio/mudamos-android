package com.inventos.plataformab.presentation.presenter.login;

import android.support.annotation.NonNull;

import com.inventos.plataformab.MudamosApplication;
import com.inventos.plataformab.domain.exception.DefaultErrorBundle;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.LoginUseCase;
import com.inventos.plataformab.domain.interactor.RetrievePasswordUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.util.FacebookHelper;
import com.inventos.plataformab.presentation.view.LoginView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.RetrofitError;

public class LoginPresenter implements Presenter, FacebookHelper.FacebookListener {
    public LoginView mLoginView;

    LoginUseCase mLoginUseCase;
    SaveUserUseCase mSaveUserUseCase;
    RetrievePasswordUseCase mRetrievePasswordUseCase;
    String userPassword;
    FacebookHelper facebookHelper;
    private boolean socialNetworkLogin = false;


    public LoginPresenter(LoginUseCase loginUseCase, SaveUserUseCase saveUserUseCase,
                          RetrievePasswordUseCase retrievePasswordUseCase,
                          FacebookHelper helper) {
        mLoginUseCase = loginUseCase;
        mSaveUserUseCase = saveUserUseCase;
        mRetrievePasswordUseCase = retrievePasswordUseCase;
        this.facebookHelper = helper;
        this.facebookHelper.setListener(this);
    }

    public void setView(@NonNull LoginView loginView) {
        mLoginView = loginView;
    }

    @Override
    public void start() {
        mLoginView.setFacebookCallbackManager(
                this.facebookHelper.getFacebookCallbackManager());
    }

    @Override
    public void resume() {}

    @Override
    public void pause() {}

    @Override
    public void destroy() {}

    public void retrievePassword(String email) {
        mLoginView.showLoading();
        mLoginView.hideForgotPassword();
        mRetrievePasswordUseCase.setEmail(email);
        mRetrievePasswordUseCase.execute(new RetrievePasswordSubscriber());
    }

    public void authenticate(String email, String password) {
        User user = User.newUserFromLogin(email, password);
        userPassword = user.getPassword();
        mLoginUseCase.setUser(user);
        mLoginView.hideMain();
        mLoginView.showLoading();
        mLoginUseCase.execute(new LoginSubscriber());
    }

    private void showErrorMessage(DefaultErrorBundle error){
        mLoginView.showError(error.getErrorMessage());
    }

    private void showErrorMessage(String errorMessage){
        mLoginView.showError(errorMessage);
    }

    private void handleFacebookUserInfo(JSONObject userJson) {
        mLoginView.hideLoading();
        mLoginView.showMain();
        try {
            socialNetworkLogin = true;
            User user = new User();
            if (userJson.has("email")) user.setEmail(userJson.getString("email"));
            if (userJson.has("email")) {
                if (userJson.has("birthday")) {
                    String date = userJson.getString("birthday");
                    String[] dateArray = date.split("/");
                    if (dateArray.length == 3) {
                        user.setBirthday(dateArray[1] + "/" +
                                dateArray[0] + "/" +
                                dateArray[2]);
                    }
                }
            }

            if (userJson.has("name")) user.setName(userJson.getString("name"));
            if (userJson.has("id")) user.setUid(userJson.getString("id"));

            user.setOauth(facebookHelper.getLoginResult()
                    .getAccessToken().getToken());
            user.setProvider(LoginUseCase.FACEBOOK);

            mLoginUseCase.setUser(user);
            mLoginUseCase.setSocialNetwork(LoginUseCase.FACEBOOK);
            mLoginUseCase.execute(new LoginSubscriber(userJson.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
            showErrorMessage("Erro na obtenção dos dados nas redes sociais");
        }


    }

    @Override
    public void onUserFetched(JSONObject user) {
        if (user != null) {
            LoginPresenter.this.handleFacebookUserInfo(user);
        }
        else {
            showErrorMessage("Algum erro ocorreu");
        }
    }

    @Override
    public void onError() {
        mLoginView.showMain();
        mLoginView.hideLoading();
        showErrorMessage("Erro de autenticação");
    }

    @Override
    public void onCancel() {
        mLoginView.showMain();
        mLoginView.hideLoading();
    }

    private final class LoginSubscriber extends DefaultSubscriber<User> {
        private String user;

        public LoginSubscriber() {}

        public LoginSubscriber(String user) {
            this.user = user;
        }

        @Override public void onError(Throwable e) {
            RetrofitError error = (RetrofitError) e;
            if (!socialNetworkLogin) {
                LoginPresenter.this.showErrorMessage(new DefaultErrorBundle(error));
                LoginPresenter.this.mLoginView.hideLoading();
                LoginPresenter.this.mLoginView.showMain();
            }
            else {
                if (error.getResponse().getStatus() == 404) {
                    // User tried social network login but didn't have an user in server
                    LoginPresenter.this.mLoginView.redirectToSignUp(user);
                }
            }
        }

        @Override public void onNext(User user) {
            if (user != null){
                MudamosApplication.setAuthenticationToken(user.getAuthToken());
                user.setPassword(userPassword);
                mSaveUserUseCase.setUser(user);
                mSaveUserUseCase.execute(new DefaultSubscriber());
                mLoginView.redirectToMain();
            }
        }
    }

    private final class RetrievePasswordSubscriber extends DefaultSubscriber<User> {

        @Override public void onError(Throwable e) {
            LoginPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
            LoginPresenter.this.mLoginView.hideLoading();
            LoginPresenter.this.mLoginView.showForgotPassword();
        }

        @Override public void onNext(User user) {
            LoginPresenter.this.mLoginView.showError(
                    "Foi enviada uma mensagem para o seu email");
            LoginPresenter.this.mLoginView.close();
        }
    }

    public void authenticateUsingFacebook() {
        mLoginView.hideMain();
        mLoginView.showLoading();
        facebookHelper.startAuthentication();
    }
}
