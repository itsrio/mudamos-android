package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.presenter.subject.CommentPresenter;
import com.inventos.plataformab.presentation.view.SubjectView;
import com.inventos.plataformab.presentation.view.activity.CommentActivity;
import com.inventos.plataformab.presentation.view.adapter.comment.CommentAdapter;
import com.inventos.plataformab.presentation.view.adapter.comment.CommentLayoutManager;
import com.inventos.plataformab.presentation.view.widget.AnonymousDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class CommentFragment extends BaseFragment implements SubjectView {
    public static final String SUBJECT_ID = "subject.id";
    public static final String CYCLE_COLOR = "cycle.color";
    public static final String COMMENT_PARENTS = "comment.parents";
    public static final String HAS_ANSWERED = "comment.has.answered";
    public static final String PHASE_HAS_ENDED = "phase.has.ended";
    public static final String IS_ANONYMOUS = "comment.is.answered";

    @Bind(R.id.rv_comments) RecyclerView rv_comments;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;
    @Bind(R.id.send) ImageButton send;
    @Bind(R.id.comment_input) EditText comment_input;
    @Bind(R.id.rl_empty) RelativeLayout rl_empty;
    @Bind(R.id.comment_input_container) LinearLayout comment_input_container;

    CommentPresenter mPresenter;
    CommentAdapter commentAdapter;

    int cycleColor = R.color.about_blue;
    int subjectId = 0;
    int commentLevel = -1;
    ArrayList<Integer> parentComments;
    boolean hasAnswered;
    boolean isAnonymous;
    private boolean phaseHasEnded;
    private boolean dialogAlreadyShowed = false;

    public interface CommentListener {
        void openCommentChildren(int subjectId, ArrayList<Integer> parents, boolean hasAnswered,
                                 boolean isAnonymous);
        void sendInfoToParent(boolean hasAnswered, boolean isAnonymous);
        void goToLogin();
    }

    public CommentListener commentListener;

    public static CommentFragment newInstance(int subjectId, int color,
                                              ArrayList<Integer> commentParents,
                                              boolean hasAnswered, boolean isAnonymous,
                                              boolean phaseHasEnded) {
        CommentFragment fragment = new CommentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SUBJECT_ID, subjectId);
        bundle.putInt(CYCLE_COLOR, color);
        bundle.putBoolean(PHASE_HAS_ENDED, phaseHasEnded);
        bundle.putIntegerArrayList(COMMENT_PARENTS, commentParents);
        bundle.putBoolean(HAS_ANSWERED, hasAnswered);
        bundle.putBoolean(IS_ANONYMOUS, isAnonymous);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CommentActivity) {
            this.commentListener= (CommentListener) activity;
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_subject, container, false);
        ButterKnife.bind(this, fragmentView);
        initialize();
        return fragmentView;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override public void onStart() {
        super.onStart();
        setupRecyclerViewAdapter();
        mPresenter.start();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
    }

    private void initialize() {
        subjectId = getArguments().getInt(SUBJECT_ID);
        cycleColor = getArguments().getInt(CYCLE_COLOR);
        hasAnswered = getArguments().getBoolean(HAS_ANSWERED);
        phaseHasEnded = getArguments().getBoolean(PHASE_HAS_ENDED, false);
        isAnonymous = getArguments().getBoolean(IS_ANONYMOUS);
        initParentComments();
        if (mPresenter == null) {
            int parentId = parentComments.size() == 0 ?
                    0 : parentComments.get(parentComments.size() - 1);
            mPresenter = PresenterFactory.newCommentPresenter(subjectId, parentId);
        }
        mPresenter.setCommentConfiguration(hasAnswered, isAnonymous);
        initCommentInput();
    }

    private void initCommentInput() {
        if (phaseHasEnded) {
            comment_input_container.setVisibility(View.GONE);
        }
        if (getLoggedUser() == null) {
            send.setEnabled(false);
            comment_input.setHint(R.string.login_to_comment);
            comment_input.setFocusable(false);
            comment_input.setClickable(true);
            comment_input.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    commentListener.goToLogin();
                }
            });
        }
    }

    public void initParentComments() {
        parentComments = new ArrayList<>();
        ArrayList<Integer> argumentParents = getArguments().getIntegerArrayList(COMMENT_PARENTS);
        if (argumentParents != null) {
            parentComments.addAll(argumentParents);
        }
        if (commentLevel == -1) {
            commentLevel = parentComments.size();
        }
    }

    public void setPresenter(@NonNull CommentPresenter presenter) {
        mPresenter = presenter;
        mPresenter.setView(this);
    }

    private void setupRecyclerViewAdapter() {
        CommentLayoutManager commentLayoutManager = new CommentLayoutManager(getActivity());
        this.rv_comments.setLayoutManager(commentLayoutManager);
        commentAdapter = new CommentAdapter(getActivity(), new ArrayList<Comment>(),
                getLoggedUser(), this.parentComments.size(), this.cycleColor, phaseHasEnded);
        commentAdapter.setOnItemClickListener(this.onItemClickListener);
        this.rv_comments.setAdapter(commentAdapter);
    }


    @Override
    public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
        this.rl_empty.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
        this.rl_empty.setVisibility(View.GONE);
    }

    @Override
    public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public void renderComments(List<Comment> commentList, int page) {
        if (page == 0) {
            if (commentList == null || commentList.size() == 0) {
                this.showEmpty();
            }
            else {
                this.hideEmpty();
                commentAdapter.setComments(commentList);
                this.rv_comments.scrollToPosition(commentAdapter.getItemCount()-1);
            }
        }
        else {
            commentAdapter.addComments(commentList);
            this.rv_comments.scrollToPosition(CommentAdapter.PAGE_SIZE * (page) );
        }
    }

    @Override
    public void addComment(Comment comment) {
        this.commentAdapter.addComment(comment);
        send.setEnabled(true);
        this.dismissProgressDialog();
        comment_input.setText("");
        this.rl_empty.setVisibility(View.GONE);
    }

    @Override
    public void onSendCommentError() {
        send.setEnabled(true);
        this.dismissProgressDialog();
    }

    @Override
    public void showEmpty() {
        this.rl_empty.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmpty() {
        this.rl_empty.setVisibility(View.GONE);
    }

    @Override
    public void showAnonymousDialog() {
        AnonymousDialog dialog = new AnonymousDialog(this.getActivity(),
                getLoggedUser().getAliasName(), mPresenter);
        dialog.show();
    }

    @Override
    public void showLoadingDialog() {
        this.showProgressDialog(R.string.sending_comment);
    }

    @Override
    public void setCommentConfig(boolean hasAnswered, boolean isAnonymous) {
        this.hasAnswered = hasAnswered;
        this.isAnonymous = isAnonymous;
    }

    public void setCommentConfigFromChildActivity(boolean hasAnswered, boolean isAnonymous) {
        this.hasAnswered = hasAnswered;
        this.isAnonymous = isAnonymous;
        this.mPresenter.setCommentConfiguration(hasAnswered, isAnonymous);
    }

    @Override
    public void showAlreadyCommentedDialog() {
        int textResource = (isAnonymous) ? R.string.commented_as_anonymous : R.string.commented_as_user;
        String message = getString(textResource);
        hideKeyboard();
        showToastMessageWithLongDuration(message);
        mPresenter.continueSending();
    }

    @OnClick(R.id.send) void sendComment() {
        String comment = comment_input.getText().toString();
        if (! comment.isEmpty()) {
            send.setEnabled(false);
            mPresenter.onSendClick(comment);
        }
    }

    @OnClick(R.id.retry_button) void retry() {
        if (mPresenter != null) mPresenter.start();
    }

    private CommentAdapter.OnItemClickListener onItemClickListener = new CommentAdapter.OnItemClickListener() {
        @Override
        public void onAnswerButtonClick(int commentId) {
            if (commentLevel < CommentFragment.this.parentComments.size()) {
                CommentFragment.this.parentComments.remove(
                        CommentFragment.this.parentComments.size()-1);
            }
            CommentFragment.this.parentComments.add(commentId);
            CommentFragment.this.commentListener.openCommentChildren(subjectId, parentComments,
                    hasAnswered, isAnonymous);
        }

        @Override
        public void onLoadMoreButtonClick() {
            CommentFragment.this.mPresenter.loadMoreComments();
        }

        @Override
        public void onLikeButtonClick(int commentId) {
            CommentFragment.this.mPresenter.addLike(subjectId, commentId);
        }

        @Override
        public void onRemoveLikeButtonClick(int commentId) {
            CommentFragment.this.mPresenter.removeLike(subjectId, commentId);
        }

        @Override
        public void onDislikeButtonClick(int commentId) {
            CommentFragment.this.mPresenter.addDislike(subjectId, commentId);
        }

        @Override
        public void onRemoveDislikeButtonClick(int commentId) {
            CommentFragment.this.mPresenter.removeDislike(subjectId, commentId);
        }
    };
}
