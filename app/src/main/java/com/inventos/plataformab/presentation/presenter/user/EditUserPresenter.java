package com.inventos.plataformab.presentation.presenter.user;

import android.support.annotation.NonNull;

import com.inventos.plataformab.MudamosApplication;
import com.inventos.plataformab.domain.exception.DefaultErrorBundle;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.EditUserUseCase;
import com.inventos.plataformab.domain.interactor.GetSignUpComponentsUseCase;
import com.inventos.plataformab.domain.interactor.GetStatesUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.State;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.EditUserView;

import java.util.List;

import retrofit.RetrofitError;

public class EditUserPresenter implements Presenter {

    private EditUserView mEditUserView;
    private String userPassword;
    private final EditUserUseCase mEditUserUseCase;
    private final SaveUserUseCase mSaveUserUseCase;
    private final GetStatesUseCase getStatesUseCase;
    private final GetSignUpComponentsUseCase getSignUpComponentsUseCase;

    public EditUserPresenter(EditUserUseCase editUserUseCase, SaveUserUseCase saveUserUseCase,
                           GetStatesUseCase getStatesUseCase,
                             GetSignUpComponentsUseCase getSignUpComponentsUseCase) {
        this.mEditUserUseCase = editUserUseCase;
        mSaveUserUseCase = saveUserUseCase;
        this.getStatesUseCase = getStatesUseCase;
        this.getSignUpComponentsUseCase = getSignUpComponentsUseCase;
    }

    public void setView(@NonNull EditUserView view){
        this.mEditUserView = view;
    }

    @Override
    public void start() {
        getProfiles();
        getStates();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mEditUserUseCase.unsubscribe();
        mSaveUserUseCase.unsubscribe();
        getStatesUseCase.unsubscribe();
        getSignUpComponentsUseCase.unsubscribe();
    }

    private void getStates() {
        getStatesUseCase.execute(new StateSubscriber());
    }

    public void editUser(User newUser) {
        User user = mEditUserView.getNewUserFields();
        userPassword = user.getPassword();
        mEditUserUseCase.setUser(newUser);
        mEditUserUseCase.execute(new EditUserSubscriber());
    }

    private void getProfiles() {
        this.mEditUserView.hideMain();
        this.mEditUserView.showLoading();
        this.mEditUserView.hideRetry();
        getSignUpComponentsUseCase.execute(new ProfileSubscriber());
    }


    private final class EditUserSubscriber extends DefaultSubscriber<User> {
        @Override public void onError(Throwable e) {
            EditUserPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
        }

        @Override public void onNext(User user) {
            if (user != null){
                user.setPassword(userPassword);
                user.setAuthToken(MudamosApplication.getAuthenticationToken());
                mSaveUserUseCase.setUser(user);
                mSaveUserUseCase.execute(new DefaultSubscriber());
                mEditUserView.redirectToMain();
            }
        }
    }

    private void renderStates(List<State> states) {
        this.mEditUserView.renderStateAndCitySpinners(states);
    }

    private final class StateSubscriber extends DefaultSubscriber<List<State>> {
        @Override public void onError(Throwable e) {
            EditUserPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
        }

        @Override public void onNext(List<State> states) {
            if (states != null){
                EditUserPresenter.this.renderStates(states);
            }
        }
    }

    private void hideMainView() {
        mEditUserView.hideMain();
    }

    private void showMainView() {
        mEditUserView.showMain();
    }


    private void hideViewLoading() {
        mEditUserView.hideLoading();
    }

    private void showViewRetry() {
        mEditUserView.showRetry();
    }

    private void showErrorMessage(DefaultErrorBundle errorBundle) {
        mEditUserView.showError(errorBundle.getErrorMessage());
    }

    private void renderProfiles(SignUpComponents components) {
        mEditUserView.renderSignUpComponents(components);
    }

    private final class ProfileSubscriber extends DefaultSubscriber<SignUpComponents> {
        @Override public void onCompleted() {
            EditUserPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            EditUserPresenter.this.hideMainView();
            EditUserPresenter.this.hideViewLoading();
            EditUserPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
            EditUserPresenter.this.showViewRetry();
        }

        @Override public void onNext(SignUpComponents components) {
            if (components != null){
                EditUserPresenter.this.renderProfiles(components);
                EditUserPresenter.this.showMainView();
            }
        }
    }

}
