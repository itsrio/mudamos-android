package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.model.User;

public interface MainView {
    void showLoading();
    void hideRetry();
    void hideLoading();
    void showRetry();
    void renderData(Settings settings);
    void showMain();
    void hideMain();
}

