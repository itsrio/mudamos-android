package com.inventos.plataformab.presentation.view.adapter.phase;

import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.inventos.plataformab.presentation.model.Phase;

import java.util.List;

public class MaterialPhaseBinder {
    PhasesAdapter mPhaseAdapter;
    RecyclerViewMaterialAdapter mViewAdapter;

    public MaterialPhaseBinder(RecyclerViewMaterialAdapter viewAdapter,
                               PhasesAdapter phasesAdapter) {
        mPhaseAdapter = phasesAdapter;
        mViewAdapter = viewAdapter;
    }

    public void setOnItemClickListener (PhasesAdapter.OnItemClickListener onItemClickListener) {
        this.mPhaseAdapter.setOnItemClickListener(onItemClickListener);
    }

    public void setPhaseList(List<Phase> phaseList) {
        this.mPhaseAdapter.setPhaseList(phaseList);
        this.mViewAdapter.notifyDataSetChanged();
    }

    public RecyclerViewMaterialAdapter getAdapter() {
        return mViewAdapter;
    }


    public int getItemCount() {
        return this.mPhaseAdapter.getItemCount();
    }

}
