package com.inventos.plataformab.presentation.presenter.signup;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.login.LoginResult;
import com.inventos.plataformab.MudamosApplication;
import com.inventos.plataformab.domain.exception.DefaultErrorBundle;
import com.inventos.plataformab.domain.interactor.CreateUserUseCase;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetSignUpComponentsUseCase;
import com.inventos.plataformab.domain.interactor.GetStatesUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.State;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.SignUpView;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.List;

import retrofit.RetrofitError;

public class SignUpPresenter implements Presenter {

    private SignUpView mSignUpView;
    private String userPassword;
    private final CreateUserUseCase mCreateUserUseCase;
    private final SaveUserUseCase mSaveUserUseCase;
    private final GetStatesUseCase getStatesUseCase;
    private final GetSignUpComponentsUseCase getSignUpComponentsUseCase;

    public SignUpPresenter(CreateUserUseCase createUserUseCase, SaveUserUseCase saveUserUseCase,
                           GetStatesUseCase getStatesUseCase, GetSignUpComponentsUseCase getSignUpComponentsUseCase) {
        mCreateUserUseCase = createUserUseCase;
        mSaveUserUseCase = saveUserUseCase;
        this.getStatesUseCase = getStatesUseCase;
        this.getSignUpComponentsUseCase = getSignUpComponentsUseCase;
    }

    public void setView(@NonNull SignUpView view){
        this.mSignUpView = view;
    }

    @Override
    public void start() {
        getProfiles();
        mSignUpView.loadLoginButtons();
        getStates();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        mCreateUserUseCase.unsubscribe();
        mSaveUserUseCase.unsubscribe();
        getStatesUseCase.unsubscribe();
        getSignUpComponentsUseCase.unsubscribe();
    }

    private void getStates() {
        getStatesUseCase.execute(new StateSubscriber());
    }

    public void createUser() {
        User user = mSignUpView.getNewUserFields();
        userPassword = user.getPassword();
        mCreateUserUseCase.setUser(user);
        mCreateUserUseCase.execute(new CreateUserSubscriber());
    }

    public void onFacebookLogin(LoginResult loginResult) {
        AccessToken token = loginResult.getAccessToken();
        User newUser = User.newUser(token.getToken(), token.getUserId());
        mCreateUserUseCase.setSocialNetwork(CreateUserUseCase.FACEBOOK);
        mCreateUserUseCase.setUser(newUser);
        mCreateUserUseCase.execute(new CreateUserSubscriber());
    }

    public void onTwitterLogin(Result<TwitterSession> result) {
        User newUser = User.newUser(result.data.getAuthToken().token, result.data.getUserId());
        mCreateUserUseCase.setSocialNetwork(CreateUserUseCase.TWITTER);
        mCreateUserUseCase.setUser(newUser);
        mCreateUserUseCase.execute(new CreateUserSubscriber());
    }

    private void getProfiles() {
        this.mSignUpView.hideMain();
        this.mSignUpView.showLoading();
        this.mSignUpView.hideRetry();
        getSignUpComponentsUseCase.execute(new ProfileSubscriber());
    }

    private final class CreateUserSubscriber extends DefaultSubscriber<User> {
        @Override public void onError(Throwable e) {
            SignUpPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
        }

        @Override public void onNext(User user) {
            if (user != null){
                MudamosApplication.setAuthenticationToken(user.getAuthToken());
                user.setPassword(userPassword);
                mSaveUserUseCase.setUser(user);
                mSaveUserUseCase.execute(new DefaultSubscriber());
                mSignUpView.redirectToMain();
            }
        }
    }

    private void renderStates(List<State> states) {
        this.mSignUpView.renderStateAndCitySpinners(states);
    }

    private final class StateSubscriber extends DefaultSubscriber<List<State>> {
        @Override public void onError(Throwable e) {
            SignUpPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
        }

        @Override public void onNext(List<State> states) {
            if (states != null){
                SignUpPresenter.this.renderStates(states);
            }
        }
    }

    private void hideMainView() {
        mSignUpView.hideMain();
    }

    private void showMainView() {
        mSignUpView.showMain();
    }


    private void hideViewLoading() {
        mSignUpView.hideLoading();
    }

    private void showViewRetry() {
        mSignUpView.showRetry();
    }

    private void showErrorMessage(DefaultErrorBundle errorBundle) {
        mSignUpView.showError(errorBundle.getErrorMessage());
    }

    private void renderProfiles(SignUpComponents components) {
        mSignUpView.renderSignUpComponents(components);
    }

    private final class ProfileSubscriber extends DefaultSubscriber<SignUpComponents> {
        @Override public void onCompleted() {
            SignUpPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            SignUpPresenter.this.hideMainView();
            SignUpPresenter.this.hideViewLoading();
            SignUpPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
            SignUpPresenter.this.showViewRetry();
        }

        @Override public void onNext(SignUpComponents components) {
            if (components != null){
                SignUpPresenter.this.renderProfiles(components);
                SignUpPresenter.this.showMainView();
            }
        }
    }
}
