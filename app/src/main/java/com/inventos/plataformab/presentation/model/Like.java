package com.inventos.plataformab.presentation.model;

public class Like {
    private int id;
    private int userId;
    private int commentId;
    private boolean isLike;

    public Like() {}

    public Like(int commentId, int userId, boolean isLike) {
        this.commentId = commentId;
        this.userId = userId;
        this.isLike = isLike;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }
}
