package com.inventos.plataformab.presentation.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.inventos.plataformab.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlreadyCommentedDialog extends Dialog {

    @Bind(R.id.commented_as_text_view) TextView commented_as_text_view;

    public interface AlreadyCommentedDialogClickListener{
        void continueSending();
    }

    private final AlreadyCommentedDialogClickListener listener;

    public AlreadyCommentedDialog(Context context, AlreadyCommentedDialogClickListener
            listener, boolean isAnonymous) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog_already_commented);
        ButterKnife.bind(this, getWindow().getDecorView());
        this.listener = listener;
        String text;
        if (isAnonymous) {
            text = context.getString(R.string.commented_as_anonymous);
        }
        else {
            text = context.getString(R.string.commented_as_user);
        }
        commented_as_text_view.setText(text);
    }

    @OnClick(R.id.ok_button) void onOkPressed() {
        listener.continueSending();
        this.dismiss();
    }
}
