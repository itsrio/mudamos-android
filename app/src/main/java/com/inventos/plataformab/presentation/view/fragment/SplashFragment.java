package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.splash.SplashPresenter;
import com.inventos.plataformab.presentation.view.SplashView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class SplashFragment extends BaseFragment implements SplashView {

    @Bind(R.id.progress)
    RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;
    @Bind(R.id.retry_button) ImageButton retry_button;
    @Bind(R.id.ll_splash_button) LinearLayout ll_splash_button;
    @Bind(R.id.main_image) ImageView main_image;
    @Bind(R.id.header_logo) ImageView header_logo;
    @Bind(R.id.home_sub_title) TextView home_sub_title;


    public interface SplashListener {
        void onLoginClick();
        void onSignupClick();
        void onExploreClick();
        void onUserAlreadyLogged(User user);
    }

    SplashPresenter mSplashPresenter;
    SplashListener mSplashListener;

    public static SplashFragment newInstance() {
        SplashFragment splashFragment = new SplashFragment();
        Bundle argumentsBundle = new Bundle();
        splashFragment.setArguments(argumentsBundle);
        return splashFragment;
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof SplashListener) {
            this.mSplashListener = (SplashListener) activity;
        }
    }

    @OnClick(R.id.login_button) void onLoginClick() {
        mSplashPresenter.loginButtonPressed();
    }

    @OnClick(R.id.explore_button) void onExploreClick() {
        mSplashPresenter.exploreButtonPressed();
    }

    @OnClick(R.id.signup_button) void onSignupClick() {
        mSplashPresenter.signupButtonPressed();
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_splash, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mSplashPresenter == null) {
            setPresenter(PresenterFactory.newSplashPresenter(this.getActivity()));
        }
        mSplashPresenter.setView(this);
        mSplashPresenter.start();
    }

    public void setPresenter(@NonNull SplashPresenter presenter) { mSplashPresenter = presenter; }

    @Override
    public void navigateToMain() {
        mSplashListener.onExploreClick();
    }

    @Override
    public void navigateToLogin() {
        mSplashListener.onLoginClick();
    }

    @Override
    public void navigateToSignUp() {
        mSplashListener.onSignupClick();
    }

    @Override
    public void navigateToMainWithUser(User user) {
        mSplashListener.onUserAlreadyLogged(user);
    }



    @Override public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMain() {
        this.ll_splash_button.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMain() {
        this.ll_splash_button.setVisibility(View.GONE);
    }

    @Override
    public void renderData(Settings settings) {
        Picasso.with(this.getActivity()).load(settings.getHeader()).into(main_image);
        Picasso.with(this.getActivity()).load(settings.getLogo()).into(header_logo);
        home_sub_title.setText(settings.getSubtitle());
    }

    @Override public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @OnClick(R.id.retry_button) void retryGetCycles(){
        mSplashPresenter.makeMainRequest();
    }
}
