package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.Comment;

import java.util.List;

public interface SubjectView extends LoadDataView {
    void renderComments(List<Comment> comment, int page);
    void addComment(Comment comment);
    void onSendCommentError();
    void showEmpty();
    void hideEmpty();
    void showAnonymousDialog();
    void showLoadingDialog();
    void setCommentConfig(boolean hasAnswered, boolean isAnonymous);
    void showAlreadyCommentedDialog();
}
