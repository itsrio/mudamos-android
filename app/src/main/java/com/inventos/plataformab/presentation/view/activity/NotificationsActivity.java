package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.fragment.NotificationsFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.FragmentFactory;

public class NotificationsActivity extends BaseActivity
        implements NotificationsFragment.NotificationFragmentListener {

    public final static String NOTIFICATIONS_FRAGMENT_FLAG = "config.fragment";

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    public static Intent getCallingIntent(Context context) {
        return new Intent(context, NotificationsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "NotificationsActivity";
        super.onCreate(savedInstanceState);
        //instantiate callbacks manager
        setContentView(R.layout.activity_notifications);
        initializeActivity();
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.notifications));
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFragment(R.id.fragment_container, FragmentFactory.newNotificationsFragment(),
                NOTIFICATIONS_FRAGMENT_FLAG);
    }

    @Override
    public void openCommentChildren(int subjectId, int commentId, ArrayList<Integer> parents) {
        this.navigator.navigateToCommentActivity(this, subjectId,
                getResources().getColor(R.color.about_blue),
                parents,
                false, false, false);
    }
}
