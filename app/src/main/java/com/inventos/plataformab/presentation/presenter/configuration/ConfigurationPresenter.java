package com.inventos.plataformab.presentation.presenter.configuration;

import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.ConfigurationView;

public class ConfigurationPresenter implements Presenter {

    private ConfigurationView mView;

    public void setView(ConfigurationView view) {
        mView = view;
    }
    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }
}
