package com.inventos.plataformab.presentation.view.adapter.state;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inventos.plataformab.presentation.model.State;


import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class StateSpinnerAdapter extends ArrayAdapter<State> {

    private List<State> stateList;
    public StateSpinnerAdapter(Context context, List<State> stateList) {
        super(context, android.R.layout.simple_spinner_dropdown_item);
        this.stateList = stateList;
        this.addAll(stateList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        SpinnerHolder holder = new SpinnerHolder(v);
        holder.spinnerLabel.setText(stateList.get(position).getName());
        return v;
    }

    public List<State> getStateList() {
        return this.stateList;
    }

    public int getSelectedState(String state) {
        int size = this.stateList.size();
        State tempState;
        int i;
        for (i=0; i < size; i++){
            tempState = this.stateList.get(i);
            if (tempState.getName().equals(state) || tempState.getUf().equals(state)) return i;
        }
        return 0;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }
    static class SpinnerHolder {
        @Bind(android.R.id.text1) TextView spinnerLabel;
        public SpinnerHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
