package com.inventos.plataformab.presentation.presenter.login;

import android.support.annotation.NonNull;

import com.inventos.plataformab.domain.exception.DefaultErrorBundle;
import com.inventos.plataformab.domain.interactor.ChangePasswordUseCase;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.view.ChangePasswordView;

import retrofit.RetrofitError;

public class ChangePasswordPresenter {

    private final ChangePasswordUseCase changePasswordUseCase;
    private final SaveUserUseCase saveUserUseCase;

    private ChangePasswordView mView;

    private String newPassword;

    public ChangePasswordPresenter(ChangePasswordUseCase changePasswordUseCase,
                                   SaveUserUseCase saveUserUseCase) {
        this.changePasswordUseCase = changePasswordUseCase;
        this.saveUserUseCase = saveUserUseCase;
    }

    public void changePassword(String newPassword, int userId) {
        User user = new User();
        this.newPassword = newPassword;
        user.setId(userId);
        user.setPassword(newPassword);
        this.changePasswordUseCase.setUser(user);
        this.changePasswordUseCase.execute(new ChangePasswordSubscriber());
    }

    public void setView(@NonNull ChangePasswordView view) {
        mView = view;
    }

    private class ChangePasswordSubscriber extends DefaultSubscriber<User> {
        @Override public void onError(Throwable e) {
            ChangePasswordPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
        }

        @Override public void onNext(User user) {
            if (user != null){
                user.setPassword(ChangePasswordPresenter.this.newPassword);
                saveUserUseCase.setUser(user);
                saveUserUseCase.execute(new DefaultSubscriber());
                ChangePasswordPresenter.this.finishActivity();
            }
        }
    }

    private void finishActivity() {
        mView.finishActivity();
    }

    private void showErrorMessage(DefaultErrorBundle error) {
        mView.showErrorMessage(error.getErrorMessage());
    }
}
