package com.inventos.plataformab.presentation.view.adapter.state;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.inventos.plataformab.presentation.model.City;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CitySpinnerAdapter extends ArrayAdapter<City> implements Filterable{

    private List<City> cityList;
    private List<City> citySuggestionsList;

    public CitySpinnerAdapter(Context context, List<City> cityList) {
        super(context, android.R.layout.simple_spinner_dropdown_item);
        this.cityList = cityList;
        this.citySuggestionsList = new ArrayList<>();
        this.addAll(cityList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        SpinnerHolder holder = new SpinnerHolder(v);
        holder.spinnerLabel.setText(citySuggestionsList.get(position).getName());
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    public void setCityList(List<City> cities) {
        this.cityList = cities;
        this.clear();
        this.addAll(cityList);
        this.notifyDataSetChanged();
    }

    static class SpinnerHolder {
        @Bind(android.R.id.text1)
        TextView spinnerLabel;
        public SpinnerHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    Filter nameFilter = new Filter() {
        public String convertResultToString(Object resultValue) {
            return ((City) (resultValue)).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                citySuggestionsList.clear();
                int cityListSize = cityList.size();
                for (int i = 0; i < cityListSize; i++) {
                    String cityName = cityList.get(i).getName().toLowerCase();
                    if (cityName.startsWith(constraint.toString().toLowerCase())) {
                        citySuggestionsList.add(cityList.get(i));
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = citySuggestionsList;
                filterResults.count = citySuggestionsList.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            @SuppressWarnings("unchecked")
            ArrayList<City> filteredList = (ArrayList<City>) results.values;
            if (results.count > 0) {
                clear();
                addAll(filteredList);
                notifyDataSetChanged();
            }
        }
    };

}
