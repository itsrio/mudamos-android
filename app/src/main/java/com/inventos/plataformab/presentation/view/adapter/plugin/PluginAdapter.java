package com.inventos.plataformab.presentation.view.adapter.plugin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.Plugin;
import com.inventos.plataformab.presentation.model.Subject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PluginAdapter extends RecyclerView.Adapter<PluginAdapter.PhaseViewHolder>{

    private int cycleColor;
    private List<Plugin> pluginList;
    private final LayoutInflater layoutInflater;
    private Phase phase;
    private OnItemClickListener onItemClickListener;

    public PluginAdapter(Context context, int cycleColor, List<Plugin> pluginList) {
        this.validatePluginList(pluginList);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.pluginList = pluginList;
        this.cycleColor = cycleColor;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
        this.notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onSubjectItemClicked(Subject subject, int cycleColor, boolean phaseHasEnded);
        //void onBlogItemClicked(Subject subject, int cycleColor);
    }

    @Override
    public PhaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.card_subject, parent, false);
        return new PhaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhaseViewHolder viewHolder, int position) {
        Plugin plugin = pluginList.get(position);
        viewHolder.cardLine.setBackgroundColor(cycleColor);
        viewHolder.cardButton.setTextColor(cycleColor);

        if (plugin instanceof Subject) {
            final Subject subject = (Subject) plugin;
            viewHolder.cardTitle.setText(subject.getTitle().toUpperCase());
            viewHolder.cardBody.setText(subject.getEnunciation());
            viewHolder.cardQuestion.setText(subject.getQuestion());

            int buttonLabel = (phase != null && ! phase.hasEnded())
                    ? R.string.comment : R.string.see_how_it_was;
            viewHolder.cardButton.setText(buttonLabel);

            viewHolder.cardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (PluginAdapter.this.onItemClickListener != null) {
                        PluginAdapter.this.onItemClickListener.onSubjectItemClicked(
                                subject, cycleColor, phase.hasEnded());
                    }
                }
            });
        }
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setPluginList(List<Plugin> pluginList) {
        this.validatePluginList(pluginList);
        this.pluginList = pluginList;
        this.notifyDataSetChanged();
    }

    public void addSubjects(List<Subject> subjects) {
        int size = subjects.size();
        for (int i = 0; i < size; i++) {
            this.pluginList.add(subjects.get(i));
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (this.pluginList != null) ? this.pluginList.size() : 0;
    }

    private void validatePluginList(List<Plugin> pluginsList) {
        if (pluginsList == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class PhaseViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.card_title) TextView cardTitle;
        @Bind(R.id.card_description) TextView cardBody;
        @Bind(R.id.card_line) View cardLine;
        @Bind(R.id.card_button) Button cardButton;
        @Bind(R.id.card_question) TextView cardQuestion;
        public PhaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
