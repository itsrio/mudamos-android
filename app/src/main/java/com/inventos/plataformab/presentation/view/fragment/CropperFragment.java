package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.activity.SignUpActivity;
import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.OnClick;

public class CropperFragment extends BaseFragment {

    public final static int REQUEST_CAMERA = 0;
    public final static int SELECT_FILE = 1;

    public interface CropListener {
        void showImageDialog();
    }

    private CropListener cropListener;

    protected String newProfileImage;

    @Bind(R.id.iv_image_container) ImageView iv_image_container;
    @Bind(R.id.bt_image_plus) Button bt_image_plus;
    @Bind(R.id.iv_icon) ImageView iv_icon;

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CropListener) {
            this.cropListener= (CropListener) activity;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                beginCrop(createImage(data));
            }
            else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                beginCrop(selectedImageUri);
            }
            else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        iv_icon.setColorFilter(R.color.about_blue);
    }

    private Uri createImage(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            return Uri.fromFile(destination);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void beginCrop(Uri source) {
        File thumbFile = new File(this.getActivity().getCacheDir(), "cropped");
        Uri destination = Uri.fromFile(thumbFile);
        Crop.of(source, destination).asSquare().start(this.getActivity());
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            File thumbFile = new File(this.getActivity().getCacheDir(), "cropped");
            Bitmap thumbnail = BitmapFactory.decodeFile(thumbFile.getAbsolutePath());
            setProfileImage(thumbnail);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this.getActivity(),
                    Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setProfileImage(Bitmap thumbnail) {
        iv_image_container.setImageBitmap(thumbnail);
        iv_image_container.setVisibility(View.VISIBLE);
        bt_image_plus.setVisibility(View.GONE);
        iv_icon.setVisibility(View.GONE);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        newProfileImage = "data:image/jpg;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @OnClick(R.id.rl_profile_picture_button) void pickProfileImage() {
        cropListener.showImageDialog();
    }
}
