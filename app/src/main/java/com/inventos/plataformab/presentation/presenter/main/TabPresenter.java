package com.inventos.plataformab.presentation.presenter.main;

import android.support.annotation.NonNull;
import android.util.Log;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.MainRequestUseCase;
import com.inventos.plataformab.domain.interactor.SaveCyclesUseCase;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.TabView;

import java.util.List;

public class TabPresenter implements Presenter {

    private final String TAG = "TabPresenter";

    MainRequestUseCase mMainRequestUseCase;
    TabView mTabView;

    public void setView(@NonNull TabView view) {
        mTabView = view;
    }

    public TabPresenter(MainRequestUseCase mainRequestUseCase) {
        this.mMainRequestUseCase = mainRequestUseCase;
    }

    @Override
    public void start() {}

    @Override
    public void resume() {}

    @Override
    public void pause() {}

    @Override
    public void destroy() {}

    public void initCycles() {
        this.loadCycles();
    }

    private void loadCycles() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getCycleList();
    }

    private void showViewLoading() {
        this.mTabView.showLoading();
    }

    private void hideViewRetry() {
        this.mTabView.hideRetry();
    }

    private void showViewRetry() {
        this.mTabView.showRetry();
    }

    private void hideViewLoading() {
        this.mTabView.hideLoading();
    }

    public void getCycleList() {
        this.mMainRequestUseCase
                .execute(new MainRequestSubscriber());
    }



    private final class MainRequestSubscriber extends DefaultSubscriber<MainRequest> {
        @Override public void onCompleted() {
            TabPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            hideViewLoading();
            //UserListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            showViewRetry();
            Log.e(TAG, "onError " + e.toString());
        }

        @Override public void onNext(MainRequest mainRequest) {
            TabPresenter.this.showCyclesInTabs(mainRequest);
        }
    }

    private void showCyclesInTabs(MainRequest request) {
        this.mTabView.renderTabs(request);
    }
}
