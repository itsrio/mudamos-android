package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.User;

public interface SplashView extends MainView{
    void navigateToMain();
    void navigateToLogin();
    void navigateToSignUp();
    void navigateToMainWithUser(User user);
}
