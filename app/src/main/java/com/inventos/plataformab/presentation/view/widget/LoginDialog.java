package com.inventos.plataformab.presentation.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.presenter.login.LoginPresenter;
import com.inventos.plataformab.presentation.view.LoginView;
import com.inventos.plataformab.presentation.view.activity.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class LoginDialog extends Dialog implements LoginView {

    @Bind(R.id.email_edit_text) EditText mUserName;
    @Bind(R.id.password_edit_text) EditText mUserPassword;
    @Bind(R.id.forgot_password_email_edit_text) EditText mRetrievePassEmail;

    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.main_view) LinearLayout main_view;
    @Bind(R.id.ll_forgot_password) LinearLayout ll_forgot_password;

    LoginPresenter mPresenter;
    LoginListener mLoginListener;

    Context mContext;

    public LoginDialog(Context context) {
        super(context);
        mContext = context;
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.view_login);
        ButterKnife.bind(this, getWindow().getDecorView());
        if (context instanceof LoginListener) {
            this.mLoginListener = (LoginListener) context;
        }
        mPresenter = PresenterFactory.newLoginPresenter(context);
        mPresenter.setView(this);
        mPresenter.start();
    }

    @OnClick(R.id.submit_button) void onLogin() {
        String email = mUserName.getText().toString();
        String password = mUserPassword.getText().toString();
        mPresenter.authenticate(email, password);
    }

    @OnClick(R.id.forgot_password_button) void onForgotPassword() {
        ll_forgot_password.setVisibility(View.VISIBLE);
        main_view.setVisibility(View.GONE);
    }

    @OnClick(R.id.bt_retrieve_password) void OnRetrievePassword() {
        String email = mRetrievePassEmail.getText().toString();
        if (!email.isEmpty()) {
            mPresenter.retrievePassword(email);
        }
    }

    @Override
    public void showError(String message) {
        this.mLoginListener.showError(message);
    }

    @Override
    public void redirectToMain() {
        mLoginListener.redirectActivityToMain();
    }

    @Override
    public void showLoading() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void hideForgotPassword() {
        ll_forgot_password.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showForgotPassword() {
        ll_forgot_password.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMain() {
        main_view.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMain() {
        main_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void close() {
        this.dismiss();
    }

    @Override
    public void setFacebookCallbackManager(CallbackManager callbackManager) {
        this.mLoginListener.setCallbackManager(callbackManager);
    }

    @Override
    public void redirectToSignUp(String user) {
        mLoginListener.redirectToSignUpWithUserInfo(user);
    }

    public interface LoginListener {
        void redirectActivityToMain();
        void showError(String message);
        void setCallbackManager(CallbackManager manager);
        void redirectToSignUpWithUserInfo(String user);
    }

    @OnClick(R.id.facebook_button) void onFacebookClick() {
        mPresenter.authenticateUsingFacebook();
    }
}
