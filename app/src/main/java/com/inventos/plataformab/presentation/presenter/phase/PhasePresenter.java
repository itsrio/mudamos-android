package com.inventos.plataformab.presentation.presenter.phase;

import android.support.annotation.NonNull;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetPhasePluginsUseCase;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.PhaseDetailsView;

public class PhasePresenter implements Presenter {

    private int mPhaseId;
    private PhaseDetailsView mDetailsView;
    private GetPhasePluginsUseCase mGetPhasePluginUseCase;

    public PhasePresenter(GetPhasePluginsUseCase getPhasePluginsUseCase) {
        mGetPhasePluginUseCase = getPhasePluginsUseCase;
    }

    public void initialize(int phaseId) {
        this.mPhaseId = phaseId;
        loadPhasePlugins();
    }

    public void setView(@NonNull PhaseDetailsView view) {
        this.mDetailsView = view;
    }

    private void loadPhasePlugins() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getPhasePlugins();
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private void showPluginsInView(Phase phase) {
        mDetailsView.renderPlugins(phase);
    }

    private void showViewLoading() {
        mDetailsView.showLoading();
    }

    private void hideViewLoading() {
        mDetailsView.hideLoading();
    }

    private void showViewRetry() {
        mDetailsView.showRetry();
    }

    private void hideViewRetry() {
        mDetailsView.hideRetry();
    }

    private void getPhasePlugins() {
        mGetPhasePluginUseCase.execute(new PluginsSubscriber());
    }

    private final class PluginsSubscriber extends DefaultSubscriber<Phase> {

        @Override public void onCompleted() {
            PhasePresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            PhasePresenter.this.hideViewLoading();
            //PhasePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            PhasePresenter.this.showViewRetry();
        }

        @Override public void onNext(Phase phase) {
            PhasePresenter.this.showPluginsInView(phase);
        }
    }

}
