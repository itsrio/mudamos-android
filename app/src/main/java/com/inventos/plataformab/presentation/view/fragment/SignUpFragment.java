package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.inventos.plataformab.R;
import com.inventos.plataformab.data.net.RestClient;
import com.inventos.plataformab.presentation.model.City;
import com.inventos.plataformab.presentation.model.Profile;
import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.State;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.login.LoginPresenter;
import com.inventos.plataformab.presentation.presenter.signup.SignUpPresenter;
import com.inventos.plataformab.presentation.view.LoginView;
import com.inventos.plataformab.presentation.view.SignUpView;
import com.inventos.plataformab.presentation.view.adapter.profile.ProfileSpinnerAdapter;
import com.inventos.plataformab.presentation.view.adapter.state.CitySpinnerAdapter;
import com.inventos.plataformab.presentation.view.adapter.state.StateSpinnerAdapter;
import com.inventos.plataformab.presentation.view.widget.LabelledSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import factory.PresenterFactory;

public class SignUpFragment extends CropperFragment implements SignUpView, LoginView {

    public final static String USER_ARGUMENT = "user";


    private SignUpPresenter mSignUpPresenter;
    private LoginPresenter mLoginPresenter;
    private SignUpListener mSignUpListener;
    private StateSpinnerAdapter stateSpinnerAdapter;
    private CitySpinnerAdapter citySpinnerAdapter;
    private ProfileSpinnerAdapter profileSpinnerAdapter;
    private ProfileSpinnerAdapter subProfileSpinnerAdapter;
    private HashMap<String, Integer> genders;
    private List<String> aliasList;
    private int aliasIndex = 0;

    public CallbackManager mFacebookCallbackManager;

    public interface SignUpListener {
        void redirectToMain();
    }

    @Bind(R.id.main_view) LinearLayout main_view;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;
    @Bind(R.id.username_edit_text) EditText username_edit_text;
    @Bind(R.id.password_edit_text) EditText password_edit_text;
    @Bind(R.id.new_email_edit_text) EditText email_edit_text;
    @Bind(R.id.password_confirmation_edit_text) EditText password_confirmation_edit_text;
    @Bind(R.id.birth_date_edit_text) EditText birth_date_edit_text;
    @Bind(R.id.state_spinner) LabelledSpinner state_spinner;
    @Bind(R.id.gender_spinner) LabelledSpinner gender_spinner;
    @Bind(R.id.city_text_view) AutoCompleteTextView city_text_view;
    @Bind(R.id.profile_spinner) LabelledSpinner profile_spinner;
    @Bind(R.id.sub_profile_spinner) LabelledSpinner sub_profile_spinner;
    @Bind(R.id.profile_caption_text_view) TextView profile_caption_text_view;
    @Bind(R.id.alias_by_default_checkbox) CheckBox anonymous_by_default_checkbox;
    @Bind(R.id.alias_name_text_view) TextView alias_name_text_view;
    @Bind(R.id.terms_and_conditions) CheckBox terms_and_conditions_checkbox;
    @Bind(R.id.terms_and_conditions_text_view) TextView terms_and_conditions_text_view;
    @Bind(R.id.facebook_button) RelativeLayout facebook_button;

    public SignUpFragment() { super(); }

    public static SignUpFragment newInstance(String userJson) {
        SignUpFragment signUpFragment = new SignUpFragment();
        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putString(USER_ARGUMENT, userJson);
        signUpFragment.setArguments(argumentsBundle);
        return signUpFragment;
    }

    public void setPresenter(@NonNull SignUpPresenter presenter, @NonNull LoginPresenter loginPresenter) {
        mSignUpPresenter = presenter;
        mLoginPresenter = loginPresenter;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_signup, container, false);
        mFacebookCallbackManager = CallbackManager.Factory.create();
        ButterKnife.bind(this, fragmentView);
        String user = getArguments().getString(USER_ARGUMENT, null) ;
        if (user != null) {
            try {
                this.fillUserFields(new JSONObject(user));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        facebook_button.setVisibility(View.VISIBLE);
        initTermsLinks();
        return fragmentView;
    }

    private void initTermsLinks() {
        String termsLabel = getResources().getString(R.string.terms_and_conditions_label);
        String fixedString = termsLabel.replaceAll("href=\"/", "href=\"" + RestClient.HOME_URL);
        Spannable spannable = (Spannable) Html.fromHtml(fixedString);
        for (URLSpan u: spannable.getSpans(0, spannable.length(), URLSpan.class)) {
            spannable.setSpan(new UnderlineSpan() {
                public void updateDrawState(TextPaint tp) {
                    tp.setUnderlineText(false);
                }
            }, spannable.getSpanStart(u), spannable.getSpanEnd(u), 0);
        }

        terms_and_conditions_text_view.setText(spannable);
        terms_and_conditions_text_view.setClickable(true);
        terms_and_conditions_text_view.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mSignUpListener == null || mLoginPresenter == null)
            this.setPresenter(PresenterFactory.newSignUpPresenter(this.getActivity()),
                    PresenterFactory.newLoginPresenter(this.getActivity()));
        mSignUpPresenter.setView(this);
        mLoginPresenter.setView(this);
        mSignUpPresenter.start();
        mLoginPresenter.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        mTwitterButton.onActivityResult(requestCode, resultCode, data);
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof SignUpListener) {
            this.mSignUpListener = (SignUpListener) activity;
        }
    }

    @Override public void onResume() {
        super.onResume();
        mSignUpPresenter.resume();
        mLoginPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        mSignUpPresenter.pause();
        mLoginPresenter.pause();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        mSignUpPresenter.destroy();
        mLoginPresenter.destroy();
    }

    @Override
    public void loadLoginButtons() {
        initTwitterButton();
    }

    @Override
    public User getNewUserFields() {
        User user = new User();
        user.setName(username_edit_text.getText().toString());
        user.setPassword(password_edit_text.getText().toString());
        user.setEmail(email_edit_text.getText().toString());
        user.setAliasName(this.aliasList.get(aliasIndex));
        user.setPasswordConfirmation(password_confirmation_edit_text.getText().toString());
        user.setState(state_spinner.getSpinner().getSelectedItem().toString());
        user.setCity(city_text_view.getText().toString());
        user.setBirthday(birth_date_edit_text.getText().toString());
        user.setAliasAsDefault(anonymous_by_default_checkbox.isChecked());
        user.setPicture(newProfileImage);

        Profile profile = (Profile) profile_spinner.getSpinner().getSelectedItem();
        user.setProfile(profile);

        if (sub_profile_spinner.getVisibility() == View.VISIBLE) {
            profile = (Profile) sub_profile_spinner.getSpinner().getSelectedItem();
            user.setSubProfile(profile);
        }

        Integer gender = this.genders.get(gender_spinner.getSpinner().getSelectedItem().toString());
        user.setGender(gender.toString());

        return user;
    }

    @Override
    public void renderStateAndCitySpinners(List<State> stateList) {
         stateSpinnerAdapter = new StateSpinnerAdapter(this.getActivity(), stateList);
        state_spinner.setCustomAdapter(stateSpinnerAdapter);
        citySpinnerAdapter = new CitySpinnerAdapter(this.getActivity(), new ArrayList<City>());
        city_text_view.setAdapter(citySpinnerAdapter);
        state_spinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                city_text_view.setText("");
                citySpinnerAdapter.setCityList(
                        stateSpinnerAdapter.getStateList().get(position).getCities());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void renderSignUpComponents(SignUpComponents components) {
        initProfileSpinners(components.getProfiles());
        initGenderSpinner(components.getGenders());
        initAliasSpinner(components.getAliasNames());
    }

    private void initGenderSpinner(HashMap<String, Integer> genders) {
        this.genders = genders;
        List<String> keys = new ArrayList<>(genders.keySet());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, keys);
        gender_spinner.setCustomAdapter(adapter);
    }

    private void initAliasSpinner(List<String> alias) {
        aliasList = alias;
        alias_name_text_view.setText(aliasList.get(aliasIndex).toUpperCase());
    }

    public void initProfileSpinners(List<Profile> profiles) {
        profileSpinnerAdapter = new ProfileSpinnerAdapter(this.getActivity(), profiles);
        profile_spinner.setCustomAdapter(profileSpinnerAdapter);
        profile_spinner.getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Profile currentProfile = profileSpinnerAdapter.getProfileList().get(position);
                profile_caption_text_view.setVisibility(View.VISIBLE);
                profile_caption_text_view.setText(currentProfile.getDescription());
                subProfileSpinnerAdapter.setProfileList(currentProfile.getChildren());
                if (currentProfile.getChildrenCount() > 0) SignUpFragment.this.showSubProfile();

                else SignUpFragment.this.hideSubProfile();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        subProfileSpinnerAdapter = new ProfileSpinnerAdapter(this.getActivity(),
                new ArrayList<Profile>());
        sub_profile_spinner.setCustomAdapter(subProfileSpinnerAdapter);
    }

    @OnClick(R.id.submit_button) void onCreateUserButtonClick() {
        if (terms_and_conditions_checkbox.isChecked()) mSignUpPresenter.createUser();
        else this.showError("Por favor aceite os termos de uso");
    }
    @OnClick(R.id.retry_button) void retryConnection() {
        mSignUpPresenter.start();
    }
    @OnClick(R.id.anonymous_by_default_text_view) void checkAliasAsDefault() {
        this.anonymous_by_default_checkbox.toggle();
    }

    @OnClick(R.id.reload_alias_button) void getNextAlias() {
        aliasIndex++;
        if ( aliasIndex == aliasList.size()) {
            aliasIndex = 0;
        }
        alias_name_text_view.setText(aliasList.get(aliasIndex).toUpperCase());
    }

    @OnFocusChange(R.id.birth_date_edit_text) void onDateFocus() {
        if (birth_date_edit_text.isFocused()) {
            setDate();
        }
    }

    @OnClick(R.id.birth_date_edit_text) void onDateClick() {
        setDate();
    }

    @OnClick(R.id.terms_and_conditions_text_view) void onTermsClick() {
        terms_and_conditions_checkbox.toggle();
    }

    private void setDate() {
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setEditText(birth_date_edit_text);
            newFragment.show(getActivity().getSupportFragmentManager(), "date picker");
    }

    private void hideSubProfile() {
        sub_profile_spinner.setVisibility(View.GONE);
    }

    private void showSubProfile() {
        sub_profile_spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMain() {
        main_view.setVisibility(View.GONE);
    }

    @Override
    public void showMain() {
        main_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void close() {

    }

    @Override
    public void setFacebookCallbackManager(CallbackManager callbackManager) {
        mFacebookCallbackManager = callbackManager;
    }

    @Override
    public void redirectToSignUp(String user) {
        try {
            this.fillUserFields(new JSONObject(user));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fillUserFields(JSONObject object) {
        if (object != null) {
            try {
                if (object.has("name")) username_edit_text.setText(object.getString("name"));
                if (object.has("email")) email_edit_text.setText(object.getString("email"));
                if (object.has("birthday")) {
                    String[] birthdayValues = object.getString("birthday").split("/");
                    String correctedBirthday = birthdayValues[1] + "/" + birthdayValues[0] +
                            "/" + birthdayValues[2];
                    birth_date_edit_text.setText(correctedBirthday);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override
    public void hideForgotPassword() {

    }

    @Override
    public void showForgotPassword() {

    }

    @Override public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public void redirectToMain() {
        mSignUpListener.redirectToMain();
    }

    public void initTwitterButton() {
//        mTwitterButton.setCallback(new Callback<TwitterSession>() {
//            @Override
//            public void success(Result<TwitterSession> result) {
//                editUserPresenter.onTwitterLogin(result);
//            }
//
//            @Override
//            public void failure(TwitterException exception) {
//                Crashlytics.log("Bad: deactivating accounts");
//                Twitter.getSessionManager().clearActiveSession();
//                showToastMessage("Something wrong =(");
//            }
//        });
    }
    @OnClick(R.id.facebook_button) void onFacebookClick() {
        mLoginPresenter.authenticateUsingFacebook();
    }
}
