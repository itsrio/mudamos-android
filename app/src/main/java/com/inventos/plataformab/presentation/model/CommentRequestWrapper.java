package com.inventos.plataformab.presentation.model;

import java.util.ArrayList;
import java.util.List;

public class CommentRequestWrapper {

    public CommentRequestWrapper() {
        this.comments = new ArrayList<>();
    }


    List<Comment> comments;
    boolean isAnonymous;
    boolean hasAnswered; //Check if user has already commented in this subject
    boolean hasCommentConfig;

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public boolean isAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public boolean hasAnswered() {
        return hasAnswered;
    }

    public void setHasAnswered(boolean hasAnswered) {
        this.hasAnswered = hasAnswered;
    }

    public boolean hasCommentConfig() {
        return hasCommentConfig;
    }

    public void setHasCommentConfig(boolean hasCommentConfig) {
        this.hasCommentConfig = hasCommentConfig;
    }
}
