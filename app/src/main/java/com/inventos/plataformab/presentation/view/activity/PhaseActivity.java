package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Subject;
import com.inventos.plataformab.presentation.view.fragment.PhaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.FragmentFactory;

public class PhaseActivity extends BaseActivity implements PhaseFragment.PluginListener {
    public static final String PHASE_FRAGMENT_TAG = "activity.phase";
    private static final String INTENT_EXTRA_PARAM_PHASE_ID = "param.phase.id";
    private static final String INTENT_EXTRA_PARAM_CYCLE_NAME = "param.cycle.name";
    private static final String INTENT_EXTRA_PARAM_CYCLE_COLOR= "param.cycle.color";

    @Bind(R.id.toolbar) Toolbar mToolbar;
    int phaseId;
    int mToolbarColor;

    public static Intent getCallingIntent(Context context, int phaseId, String cycleName,
                                          int cycleColor) {
        Intent callingIntent = new Intent(context, PhaseActivity.class);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_PHASE_ID, phaseId);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_CYCLE_NAME, cycleName);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_CYCLE_COLOR, cycleColor);
        return callingIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "PhaseActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phase);
        ButterKnife.bind(this);
        initializeActivity();
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        this.initToolbar();
        addFragment(R.id.fragment_container, FragmentFactory.newPhaseFragment(phaseId,
                        mToolbarColor), PHASE_FRAGMENT_TAG);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Intent intent = getIntent();
        String cycleName = intent.getStringExtra(INTENT_EXTRA_PARAM_CYCLE_NAME);
        phaseId = intent.getIntExtra(INTENT_EXTRA_PARAM_PHASE_ID, 0);
        mToolbarColor = intent.getIntExtra(INTENT_EXTRA_PARAM_CYCLE_COLOR,
                R.color.toolbar_color);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(cycleName);
        }
        mToolbar.setBackgroundColor(mToolbarColor);
    }

    @Override
    public void onSubjectClicked(Subject subject, int cycleColor, boolean phaseHasEnded) {
        this.navigator.navigateToCommentActivity(this, subject, cycleColor, phaseHasEnded);
    }
}
