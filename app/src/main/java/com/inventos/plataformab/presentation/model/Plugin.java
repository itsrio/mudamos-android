package com.inventos.plataformab.presentation.model;

public class Plugin extends BaseComponent{
    protected int id;
    protected int pluginRelationId;
    protected String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPluginRelationId() {
        return pluginRelationId;
    }

    public void setPluginRelationId(int pluginRelationId) {
        this.pluginRelationId = pluginRelationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
