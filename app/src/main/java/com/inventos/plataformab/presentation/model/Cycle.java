package com.inventos.plataformab.presentation.model;

import com.google.gson.JsonObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Cycle extends PluginContainer{
    private int id;
    private String name;
    private String title;
    private String about;
    private String initialDate;
    private String finalDate;
    private String picture;
    private String color;
    private List<Phase> phases;
    private String description;
    private String compilationPdf;

    public Cycle() {
    }

    public Cycle(JsonObject jsonCycle) {
        this.id = jsonCycle.get("id").getAsInt();
        this.name = jsonCycle.get("name").getAsString();
        this.title = jsonCycle.get("title").getAsString();
        if (! jsonCycle.get("about").isJsonNull()) this.about = jsonCycle.get("about").getAsString();
        if (! jsonCycle.get("compilation_pdf").isJsonNull()) this.compilationPdf =
                jsonCycle.get("compilation_pdf").getAsString();
        this.initialDate = jsonCycle.get("initial_date").getAsString();
        this.finalDate = jsonCycle.get("final_date").getAsString();
        this.color = jsonCycle.get("color").getAsString();
        this.picture = jsonCycle.get("picture").getAsString();
        this.description = jsonCycle.get("description").getAsString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(String initialDate) {
        this.initialDate = initialDate;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(String finalDate) {
        this.finalDate = finalDate;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Phase> getPhases() {
        return phases;
    }

    public void setPhases(List<Phase> phases) {
        this.phases = sortPhases(phases);
    }

    private List<Phase> sortPhases(List<Phase> phases) {
        if (phases != null) {
            Collections.sort(phases, new Comparator<Phase>() {
                @Override
                public int compare(Phase lhs, Phase rhs) {
                    if (lhs.hasEnded()) return 1;
                    else if (lhs.isCurrent()) return -1;
                    else return 0;
                }
            });
        }
        return phases;
    }

    /* public List<PluginRelation> getPluginRelations() {
        return pluginRelations;
    }

    public void setPluginRelations(List<PluginRelation> pluginRelations) {
        this.pluginRelations = pluginRelations;
    } */

    public Phase getPhaseById(int id) {
        int phaseListSize = this.phases.size();
        Phase tempPhase;
        for(int i = 0; i < phaseListSize; i++) {
            tempPhase = this.phases.get(i);
            if (tempPhase.getId() == id) {
                return tempPhase;
            }
        }
        return null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompilationPdf() {
        return compilationPdf;
    }

    public void setCompilationPdf(String compilationPdf) {
        this.compilationPdf = compilationPdf;
    }
}
