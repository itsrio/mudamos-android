package com.inventos.plataformab.presentation.presenter.subject;

import android.support.annotation.NonNull;

import com.inventos.plataformab.domain.exception.DefaultErrorBundle;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetCommentsUseCase;
import com.inventos.plataformab.domain.interactor.LikeDislikeUseCase;
import com.inventos.plataformab.domain.interactor.RemoveLikeDislikeUseCase;
import com.inventos.plataformab.domain.interactor.SendCommentUseCase;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Like;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.SubjectView;
import com.inventos.plataformab.presentation.view.widget.AlreadyCommentedDialog;
import com.inventos.plataformab.presentation.view.widget.AnonymousDialog;

import java.util.List;

import retrofit.RetrofitError;

public class CommentPresenter implements Presenter, AnonymousDialog.AnonymousDialogClickListener,
        AlreadyCommentedDialog.AlreadyCommentedDialogClickListener {

    private GetCommentsUseCase getCommentsUseCase;
    private SendCommentUseCase sendCommentUseCase;
    private LikeDislikeUseCase likeDislikeUseCase;
    private RemoveLikeDislikeUseCase removeLikeDislikeUseCase;

    private int page = 0;

    public int getPage() {
        return page;
    }

    private boolean hasAnswered;
    private boolean isAnonymous;

    private SubjectView mView;

    private String userComment;

    public CommentPresenter(GetCommentsUseCase getCommentsUseCase,
                            LikeDislikeUseCase likeDislikeUseCase,
                            RemoveLikeDislikeUseCase removeLikeDislikeUseCase,
                            SendCommentUseCase sendCommentUseCase) {
        this.getCommentsUseCase = getCommentsUseCase;
        this.sendCommentUseCase = sendCommentUseCase;
        this.likeDislikeUseCase = likeDislikeUseCase;
        this.removeLikeDislikeUseCase = removeLikeDislikeUseCase;
    }

    public void setView(@NonNull SubjectView subjectView) {
        this.mView = subjectView;
    }

    public void initialize() {
        this.showViewLoading();
        this.hideViewRetry();
        this.loadComments();
    }

    public void loadMoreComments() {
        page++;
        this.loadComments();
    }

    public void loadComments() {
        this.getCommentsUseCase.setPage(page);
        this.getCommentsUseCase.execute(new CommentSubscriber());
    }

    public void setCommentConfiguration(boolean hasAnswered, boolean isAnonymous) {
        this.hasAnswered = hasAnswered;
        this.isAnonymous = isAnonymous;
    }


    public void setCommentViewConfiguration() {
        this.mView.setCommentConfig(this.hasAnswered, this.isAnonymous);
    }


    @Override
    public void start() {
        this.initialize();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        getCommentsUseCase.unsubscribe();
        sendCommentUseCase.unsubscribe();
        likeDislikeUseCase.unsubscribe();
        removeLikeDislikeUseCase.unsubscribe();
    }

    private void showCommentsInView(List<Comment> comments) {
        mView.renderComments(comments, page);
    }

    private void showViewLoading() {
        mView.showLoading();
    }

    private void hideViewLoading() {
        mView.hideLoading();
    }

    private void showViewRetry() {
        mView.showRetry();
    }

    private void hideViewRetry() {
        mView.hideRetry();
    }

    public void sendComment() {
        this.mView.showLoadingDialog();
        this.sendCommentUseCase.setComment(this.userComment, isAnonymous);
        this.sendCommentUseCase.execute(new CommentSenderSubscriber());
    }

    public void onSendClick(String comment) {
        this.userComment = comment;
        if (hasAnswered){
            this.mView.showAlreadyCommentedDialog();
        }
        else {
            this.mView.showAnonymousDialog();
        }
    }

    private void onSendCommentError() {
        this.mView.onSendCommentError();
    }

    private void showErrorMessage(DefaultErrorBundle error){
        mView.showError(error.getErrorMessage());
    }

    private void showEmptyView() {
        this.mView.showEmpty();
    }

    @Override
    public void send(boolean isAnonymous) {
        this.hasAnswered = true;
        this.isAnonymous = isAnonymous;
        this.sendComment();
    }

    @Override
    public void continueSending() {
        sendComment();
    }

    public void addDislike(int subjectId, int commentId) {
        this.likeDislikeUseCase.setSubjectId(subjectId);
        this.likeDislikeUseCase.setCommentId(commentId);
        this.likeDislikeUseCase.setLike(false);
        this.likeDislikeUseCase.execute(new LikeSubscriber());
    }

    public void addLike(int subjectId, int commentId) {
        this.likeDislikeUseCase.setSubjectId(subjectId);
        this.likeDislikeUseCase.setCommentId(commentId);
        this.likeDislikeUseCase.setLike(true);
        this.likeDislikeUseCase.execute(new LikeSubscriber());
    }

    public void removeDislike(int subjectId, int commentId) {
        this.removeLikeDislikeUseCase.setSubjectId(subjectId);
        this.removeLikeDislikeUseCase.setCommentId(commentId);
        this.removeLikeDislikeUseCase.setLike(false);
        this.removeLikeDislikeUseCase.execute(new LikeSubscriber());
    }

    public void removeLike(int subjectId, int commentId) {
        this.removeLikeDislikeUseCase.setSubjectId(subjectId);
        this.removeLikeDislikeUseCase.setCommentId(commentId);
        this.removeLikeDislikeUseCase.setLike(true);
        this.removeLikeDislikeUseCase.execute(new LikeSubscriber());
    }

    private final class CommentSubscriber extends DefaultSubscriber<CommentRequestWrapper> {
        @Override public void onCompleted() {
            CommentPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            CommentPresenter.this.hideViewLoading();
            CommentPresenter.this.showErrorMessage(new DefaultErrorBundle((RetrofitError) e));
            CommentPresenter.this.showViewRetry();
        }

        @Override public void onNext(CommentRequestWrapper wrapper) {
            if (wrapper == null) wrapper = new CommentRequestWrapper();
            CommentPresenter.this.showCommentsInView(wrapper.getComments());
            if (wrapper.hasCommentConfig()) {
                CommentPresenter.this.hasAnswered = wrapper.hasAnswered();
                CommentPresenter.this.isAnonymous = wrapper.isAnonymous();
                CommentPresenter.this.setCommentViewConfiguration();
            }
        }
    }

    public final class CommentSenderSubscriber extends DefaultSubscriber<Comment> {
        @Override public void onError(Throwable e) {
            CommentPresenter.this.onSendCommentError();
            //PhasePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
        }

        @Override public void onNext(Comment comment) {
            if (comment != null) {
                CommentPresenter.this.mView.addComment(comment);
                CommentPresenter.this.hasAnswered = true;
                CommentPresenter.this.isAnonymous = comment.isAnonymous();
                CommentPresenter.this.setCommentViewConfiguration();
            }
        }
    }
    public class LikeSubscriber extends DefaultSubscriber<Like> {
        @Override
        public void onError(Throwable e) {
            CommentPresenter.this.onSendCommentError();
            //PhasePresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
        }
    }
}
