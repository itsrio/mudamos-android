package com.inventos.plataformab.presentation.model;

import com.google.gson.JsonObject;

public class PluginRelation {
    private int id;
    private int relatedId;
    private int pluginId;
    private String relatedType;

    public PluginRelation(JsonObject jsonPluginRelation) {
        this.id = jsonPluginRelation.get("id").getAsInt();
        this.relatedId = jsonPluginRelation.get("related_id").getAsInt();
        this.pluginId = jsonPluginRelation.get("plugin_id").getAsInt();
        this.relatedType = jsonPluginRelation.get("related_type").getAsString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(int relatedId) {
        this.relatedId = relatedId;
    }

    public String getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(String relatedType) {
        this.relatedType = relatedType;
    }

    public int getPluginId() {
        return pluginId;
    }

    public void setPluginId(int pluginId) {
        this.pluginId = pluginId;
    }
}
