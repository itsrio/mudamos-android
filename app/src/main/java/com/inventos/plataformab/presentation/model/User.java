package com.inventos.plataformab.presentation.model;

public class User extends BaseComponent {

    private int id;
    private Profile profile;
    private String name;
    private String email;
    private String password;
    private String authToken;
    private String uid;
    private String passwordConfirmation;
    private Boolean aliasAsDefault;
    private String aliasName;
    private String picture;
    private String anonymousPictureUrl;
    private String gender;
    private String city;
    private String state;
    private String birthday;
    private Profile subProfile;
    private String provider;
    private String oauth;

    public User() {}

    public User(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public User(String name, String password, String email, String passwordConfirmation) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.passwordConfirmation = passwordConfirmation;
    }

    public static User newUser(String authToken, String uId) {
        User newUser = new User();
        newUser.setAuthToken(authToken);
        newUser.setUid(uId);
        return newUser;
    }

    public static User newUserFromLogin(String email, String password) {
        User newUser = new User();
        newUser.setEmail(email);
        newUser.setPassword(password);
        return newUser;
    }

    public static User newUser(String authToken, long uId) {
        User newUser = new User();
        newUser.setAuthToken(authToken);
        newUser.setUid("" + uId);
        return newUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isAliasAsDefault() {
        return aliasAsDefault;
    }

    public void setAliasAsDefault(boolean aliasAsDefault) {
        this.aliasAsDefault = aliasAsDefault;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAnonymousPictureUrl() {
        return anonymousPictureUrl;
    }

    public void setAnonymousPictureUrl(String anonymousPictureUrl) {
        this.anonymousPictureUrl = anonymousPictureUrl;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Profile getSubProfile() {
        return subProfile;
    }

    public void setSubProfile(Profile subProfile) {
        this.subProfile = subProfile;
    }

    public Boolean getAliasAsDefault() {
        return aliasAsDefault;
    }

    public void setAliasAsDefault(Boolean aliasAsDefault) {
        this.aliasAsDefault = aliasAsDefault;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getOauth() {
        return oauth;
    }

    public void setOauth(String oauth) {
        this.oauth = oauth;
    }
}
