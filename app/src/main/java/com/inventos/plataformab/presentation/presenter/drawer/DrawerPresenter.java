package com.inventos.plataformab.presentation.presenter.drawer;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import com.inventos.plataformab.MudamosApplication;
import com.inventos.plataformab.R;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetLoggedUserUseCase;
import com.inventos.plataformab.domain.interactor.LogoutUserUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.DrawerView;

public class DrawerPresenter  implements NavigationView.OnNavigationItemSelectedListener, Presenter {

    DrawerLayout mDrawerLayout;
    DrawerView mDrawerView;
    LogoutUserUseCase mLogoutUserUseCase;
    GetLoggedUserUseCase mGetLoggedUserUseCase;
    SaveUserUseCase mSaveUserUseCase;
    String userPassword;

    public DrawerPresenter(LogoutUserUseCase logoutUserUseCase,
                           GetLoggedUserUseCase getLoggerUserUseCase,
                           SaveUserUseCase saveUserUseCase) {
        mLogoutUserUseCase = logoutUserUseCase;
        mGetLoggedUserUseCase = getLoggerUserUseCase;
        mSaveUserUseCase = saveUserUseCase;
    }

    public void setDrawerLayout(@NonNull DrawerLayout drawerLayout){
        mDrawerLayout = drawerLayout;
    }

    public void setView(@NonNull DrawerView drawerView){
        this.mDrawerView = drawerView;
        mDrawerView.updateNavigationView();
    }

    private void openNotifications() {
        mDrawerView.openNotificationsActivity();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        mDrawerLayout.closeDrawers();

        switch (menuItem.getItemId()){
            case R.id.login:
                mDrawerView.login();
                break;
            case R.id.logout:
                this.logoutUser();
                break;
            case R.id.terms_and_conditions:
                this.openTerms();
                break;
            case R.id.configurations:
                this.openConfigurations();
                break;
            case R.id.notifications:
                this.openNotifications();
                break;
            case R.id.blog:
                this.openBlog();
                break;
            case R.id.signup:
                this.openSignup();
                break;
            default:
                return true;
        }
        return false;
    }

    private void openSignup() {
        mDrawerView.openSignUpActivity();
    }

    private void openConfigurations() {
        mDrawerView.openConfigurationActivity();
    }

    private void openTerms() {
        mDrawerView.showTerms();
    }

    private void openBlog() {
        mDrawerView.openBlogActivity();
    }

    private void logoutUser() {
        mLogoutUserUseCase.execute(new DefaultSubscriber());
        mDrawerView.logoutUser();
    }

    @Override
    public void start() {
        if (! MudamosApplication.getAuthenticationToken().isEmpty()){
            mGetLoggedUserUseCase.execute(new LoggedUserSubscriber());
        }
        else {
            mDrawerView.initContentView();
        }
        User user = mDrawerView.getUserAsObject();
        if (user != null) {
            userPassword = user.getPassword();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    private class LoggedUserSubscriber extends DefaultSubscriber<User> {

        @Override public void onError(Throwable e) {
            DrawerPresenter.this.logoutUser();
            DrawerPresenter.this.mDrawerView.initContentView();
        }

        @Override public void onNext(User user) {
            user.setAuthToken(MudamosApplication.getAuthenticationToken());
            mSaveUserUseCase.setUser(user);
            user.setPassword(userPassword);
            mSaveUserUseCase.execute(new SaveUserSubscriber());
        }
    }

    private class SaveUserSubscriber extends DefaultSubscriber {
        @Override public void onCompleted() {
            DrawerPresenter.this.mDrawerView.initContentView();
        }
    }

}
