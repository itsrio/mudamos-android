package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.MainRequest;

public interface TabView {
    void showLoading();
    void hideRetry();
    void hideLoading();
    void showRetry();
    void renderTabs(MainRequest requestObject);
}
