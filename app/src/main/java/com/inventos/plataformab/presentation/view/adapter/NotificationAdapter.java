package com.inventos.plataformab.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Notifications;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private List<Notifications> notificationsList;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private final NotificationItemClickListener listener;

    public interface NotificationItemClickListener {
        void openComment(int commentId, int subjectId, Integer parentId);
    }

    public NotificationAdapter(Context context, NotificationItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.notificationsList = new ArrayList<>();
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(
                R.layout.row_notification, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        final Notifications notification = this.notificationsList.get(position);
        holder.tv_notification_content.setText(notification.getTitle());
        Picasso.with(context).load(notification.getPictureUrl()).into(holder.profile_image);
        holder.setDate(notification.getCreatedAt());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openComment(notification.getCommentId(), notification.getSubjectId(),
                        notification.getParentId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (this.notificationsList == null) ? 0 : this.notificationsList.size() ;
    }

    public void setNotificationsList(List<Notifications> notificationsList) {
        this.notificationsList = notificationsList;
        this.notifyDataSetChanged();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.profile_image) ImageView profile_image;
        @Bind(R.id.tv_notification_content) TextView tv_notification_content;
        @Bind(R.id.tv_notification_date) TextView tv_notification_date;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setDate(Date createdAt) {
            tv_notification_date.setText(
                    DateUtils.getRelativeTimeSpanString(createdAt.getTime()));
        }
    }
}
