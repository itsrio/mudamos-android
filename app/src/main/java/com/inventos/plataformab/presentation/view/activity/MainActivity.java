package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.inventos.plataformab.R;
import com.inventos.plataformab.data.net.RestClient;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.drawer.DrawerPresenter;
import com.inventos.plataformab.presentation.presenter.main.MainPresenter;
import com.inventos.plataformab.presentation.presenter.main.TabPresenter;
import com.inventos.plataformab.presentation.util.DownloadHandler;
import com.inventos.plataformab.presentation.view.DrawerView;
import com.inventos.plataformab.presentation.view.MainView;
import com.inventos.plataformab.presentation.view.TabView;
import com.inventos.plataformab.presentation.view.adapter.CyclePagerAdapter;
import com.inventos.plataformab.presentation.view.fragment.CycleFragment;
import com.inventos.plataformab.presentation.view.listener.MaterialViewPagerListener;
import com.inventos.plataformab.presentation.view.widget.LoginDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.FragmentFactory;
import factory.PresenterFactory;

public class MainActivity extends BaseActivity implements DrawerView, TabView, MainView,
        CycleFragment.PhaseListListener, LoginDialog.LoginListener {

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    Toolbar mToolbar;
    @Bind(R.id.navigation_view) NavigationView mNavigationView;
    @Bind(R.id.drawer) DrawerLayout mDrawerLayout;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;
    @Bind(R.id.retry_button) ImageButton retry_button;
    @Bind(R.id.cycle_viewpager) MaterialViewPager mCycleViewPager;
    @Bind(R.id.header_logo) ImageView headerLogo;
    @Bind(R.id.profile_image) ImageView userPicture;
    @Bind(R.id.username) TextView userName;


    // Presenter for drawer interactions
    DrawerPresenter mDrawerPresenter;
    // Presenter for tab interaction
    TabPresenter mTabPresenter;
    MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "MainActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        initToolbar();
        initDrawerPresenter();
        initMainPresenter();
    }

    private void initToolbar() {
        mToolbar = mCycleViewPager.getToolbar();

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setHomeButtonEnabled(false);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mDrawerPresenter.resume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home){
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initDrawerLayout() {
        // Initializing Drawer Layout and ActionBarToggle
        ActionBarDrawerToggle actionBarDrawerToggle =
                new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                        R.string.open_drawer, R.string.close_drawer){

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Code here will be triggered once the drawer
                        // closes as we dont want anything to happen so we leave this blank
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Code here will be triggered once the drawer open
                        // as we dont want anything to happen so we leave this blank

                        super.onDrawerOpened(drawerView);
                    }
                };

        //Setting the actionbarToggle to drawer layout
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void login() {
        LoginDialog dialog = new LoginDialog(this);
        dialog.show();
    }

    @Override
    public String getUser() {
        return this.getLoggedUser();
    }

    @Override
    public User getUserAsObject() {
        return getLoggedUserAsObject();
    }

    @Override
    public void logoutUser() {
        this.navigator.navigateToSplashActivityClearTop(this);
    }

    @Override
    public void updateNavigationView() {
        User user = this.getLoggedUserAsObject();
        boolean logged = user != null;
        Menu navMenu = mNavigationView.getMenu();
        int navMenuSize = navMenu.size();
        MenuItem tempMenu;
        for (int i = 0; i < navMenuSize; i++) {
            tempMenu = navMenu.getItem(i);
            if (tempMenu.getItemId() == R.id.login || tempMenu.getItemId() == R.id.signup) {
                tempMenu.setVisible(!logged);
                continue;
            }
            if (tempMenu.getItemId() != R.id.blog){
                tempMenu.setVisible(logged);
            }
        }

        if (user != null) {
            if (user.getName() != null) {
                userName.setText(user.getName().toUpperCase());
            }
            Picasso.with(this).load(user.getPicture()).into(userPicture);
        }
        else {
            userName.setText("");
            userPicture.setImageDrawable(null);
        }

    }

    @Override
    public void initContentView() {
        updateNavigationView();
        initDrawerLayout();
        mNavigationView.setNavigationItemSelectedListener(mDrawerPresenter);
    }

    @Override
    public void showTerms() {
        Uri uri = Uri.parse(RestClient.HOME_URL + "termos-de-uso");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void openConfigurationActivity() {
        this.navigator.navigateToConfigurationActivity(this);
    }

    @Override
    public void openBlogActivity() {
        Uri uri = Uri.parse(RestClient.HOME_URL + "blog");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void openSignUpActivity() {
        this.navigator.navigateToSignupActivity(this, null);
    }

    @Override
    public void openNotificationsActivity() {
        this.navigator.navigateToNotificationsActivity(this);
    }

    private void initMainPresenter() {
        if (mMainPresenter == null) {
            mMainPresenter = PresenterFactory.newMainPresenter(this);
            mMainPresenter.setView(this);
            mMainPresenter.start();
        }
    }

    private void initDrawerPresenter() {
        mDrawerPresenter = PresenterFactory.newDrawerPresenter(this);
        mDrawerPresenter.setDrawerLayout(mDrawerLayout);
        mDrawerPresenter.setView(this);
        mDrawerPresenter.start();
    }

    private void initTabPresenter() {
        mTabPresenter = PresenterFactory.newTabPresenter(this);
        mTabPresenter.setView(this);
        mTabPresenter.initCycles();
    }


    public void initTabs(MainRequest requestObject) {
        List<Cycle> cyclesList = requestObject.getCycles();
        Settings settings = requestObject.getSettings();
        int size = cyclesList.size();
        Cycle tempCycle;
        CyclePagerAdapter adapter = new CyclePagerAdapter(getSupportFragmentManager());
        //adapter.addFragment(FragmentFactory.newAboutFragment(this), "sobre");
        adapter.addFragment(FragmentFactory.newAboutFragment(settings), "sobre");

        for (int i = 0; i < size; i++) {
            tempCycle = cyclesList.get(i);
            adapter.addFragment(FragmentFactory.newCycleFragment(tempCycle)
                    , tempCycle.getTitle());
        }
        MaterialViewPagerListener pagerListener = new MaterialViewPagerListener(cyclesList,
                adapter, headerLogo,settings.getLogo() ,this);
        mCycleViewPager.getViewPager().setAdapter(adapter);
        mCycleViewPager.getViewPager().addOnPageChangeListener(pagerListener);
        mCycleViewPager.setMaterialViewPagerListener(pagerListener);
        mCycleViewPager.getPagerTitleStrip().setViewPager(mCycleViewPager.getViewPager());
    }

    @Override public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
        this.setProgressBarIndeterminateVisibility(true);
    }

    @Override public void hideLoading() {
        this.progress.setVisibility(View.GONE);
        this.setProgressBarIndeterminateVisibility(false);
    }

    @Override public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderData(Settings settings) {

    }

    @Override
    public void showMain() {
        initTabPresenter();
    }

    @Override
    public void hideMain() {

    }

    @Override public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @OnClick(R.id.retry_button) void retryGetCycles(){
        mTabPresenter.initCycles();
    }

    @Override public void renderTabs(MainRequest request) {
        if (request != null) {
            this.initTabs(request);
        }
    }

    @Override
    public void onSubjectPhaseClicked(Phase phase, String cycleName, int cycleColor) {
        this.navigator.navigateToPhaseActivity(this, phase.getId(), cycleName, cycleColor);
    }

    @Override
    public void onReportPhaseClicked(Phase phase, String compilationPdf, String name) {
        showToastMessage("Conectando ao servidor...");
        DownloadHandler.download(this, compilationPdf, name);
    }


    @Override
    public void redirectActivityToMain() {
        this.navigator.navigateToMainActivityClearTop(this);
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void setCallbackManager(CallbackManager manager) {

    }

    @Override
    public void redirectToSignUpWithUserInfo(String user) {
        this.navigator.navigateToSignupActivity(this, user);
    }
}
