package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.CallbackManager;
import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.view.fragment.SplashFragment;
import com.inventos.plataformab.presentation.view.widget.LoginDialog;

import butterknife.ButterKnife;
import factory.FragmentFactory;

public class SplashActivity extends BaseActivity implements SplashFragment.SplashListener,
        LoginDialog.LoginListener {

    public static String SPLASH_FRAGMENT_TAG = "splash.fragment";

    public CallbackManager mFacebookCallbackManager;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "SplashActivity";
        super.onCreate(savedInstanceState);
        String user = this.getLoggedUser();
        if (user != null) {
            navigator.navigateToMainActivity(this, user);
            finish();
        }
        else {
            setContentView(R.layout.activity_splash);
            initializeActivity();
        }
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        addFragment(R.id.fragment_container, FragmentFactory.newSplashFragment(this),
                SPLASH_FRAGMENT_TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mFacebookCallbackManager != null)
            mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onLoginClick() {
        LoginDialog dialog = new LoginDialog(this);
        dialog.show();
    }

    @Override
    public void onSignupClick() {
        navigator.navigateToSignupActivity(this, null);
    }

    @Override
    public void onExploreClick() {
        navigator.navigateToMainActivity(this);
    }

    @Override
    public void onUserAlreadyLogged(User user) {

    }

    @Override
    public void redirectToSignUpWithUserInfo(String user) {
        navigator.navigateToSignupActivity(this, user);
    }

    @Override
    public void redirectActivityToMain() {
        this.navigator.navigateToMainActivityClearTop(this);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public void setCallbackManager(CallbackManager manager) {
        mFacebookCallbackManager = manager;
    }
}
