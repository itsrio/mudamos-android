package com.inventos.plataformab.presentation.view.adapter.profile;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inventos.plataformab.presentation.model.Profile;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileSpinnerAdapter extends ArrayAdapter<Profile> {

    private List<Profile> profileList;

    public ProfileSpinnerAdapter(Context context, List<Profile> profileList) {
        super(context, android.R.layout.simple_spinner_dropdown_item);
        this.profileList = profileList;
        this.addAll(profileList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        SpinnerHolder holder = new SpinnerHolder(v);
        holder.spinnerLabel.setText(profileList.get(position).getName());
        return v;
    }

    public List<Profile> getProfileList() {
        return this.profileList;
    }

    public void setProfileList(List<Profile> profileList) {
        this.profileList = profileList;
        this.clear();
        this.addAll(profileList);
        notifyDataSetChanged();
    }

    public int getSelectedProfile(int profileId) {
        int size = this.profileList.size();
        int tempId;
        int i;
        for (i=0; i < size; i++){
            tempId = this.profileList.get(i).getId();
            if (tempId == profileId) return i;
        }
        return 0;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }
    static class SpinnerHolder {
        @Bind(android.R.id.text1)
        TextView spinnerLabel;
        public SpinnerHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
