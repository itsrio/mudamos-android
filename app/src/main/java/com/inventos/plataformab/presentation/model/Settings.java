package com.inventos.plataformab.presentation.model;

import java.util.ArrayList;
import java.util.List;

public class Settings {
    private String logo;
    private String header;
    private String subtitle;
    private List<String> titles;
    private List<String> content;

    public Settings() {
        this.titles = new ArrayList<>();
        this.content = new ArrayList<>();
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}
