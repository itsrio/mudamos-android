package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.google.gson.Gson;
import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.presenter.cycle.PhaseListPresenter;
import com.inventos.plataformab.presentation.view.CycleView;
import com.inventos.plataformab.presentation.view.activity.MainActivity;
import com.inventos.plataformab.presentation.view.adapter.phase.MaterialPhaseBinder;
import com.inventos.plataformab.presentation.view.adapter.phase.PhaseLayoutManager;
import com.inventos.plataformab.presentation.view.adapter.phase.PhasesAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class CycleFragment extends BaseFragment implements CycleView {

    public static final String CYCLE_OBJECT = "cycle.object";

    @Bind(R.id.rv_phases) RecyclerView rv_phases;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;

    private PhaseListListener phaseListListener;
    private PhaseListPresenter phaseListPresenter;
    private MaterialPhaseBinder phasesAdapter;

    Cycle mCycle;


    private PhasesAdapter.OnItemClickListener onItemClickListener;

    public static CycleFragment newInstance(String cycle) {
        CycleFragment fragment = new CycleFragment();
        Bundle cycleBundle = new Bundle();
        cycleBundle.putSerializable(CYCLE_OBJECT, cycle);
        fragment.setArguments(cycleBundle);
        return fragment;
    }

    public interface PhaseListListener {
        void onSubjectPhaseClicked(final Phase phase, String cycleName, int cycleColor);

        void onReportPhaseClicked(Phase phase, String compilationPdf, String name);
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            this.phaseListListener = (PhaseListListener) activity;
        }
    }

    public void setPresenter(PhaseListPresenter presenter) {
        phaseListPresenter = presenter;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_cycle, container, false);
        Gson gson = new Gson();
        mCycle = gson.fromJson(getArguments().getString(CYCLE_OBJECT), Cycle.class);
        ButterKnife.bind(this, fragmentView);
        setupUI();
        return fragmentView;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
        this.loadPhaseList();
    }

    private void loadPhaseList() {
        if (mCycle.getPhases().size() > 0) {
            this.renderPhaseList(mCycle.getPhases());
        }
        else {
            this.phaseListPresenter.getPhaseList();
        }
    }

    private void setupUI() {
        PhaseLayoutManager phaseLayoutManager = new PhaseLayoutManager(getActivity());
        this.rv_phases.setLayoutManager(phaseLayoutManager);
        setupRecyclerViewAdapter();
    }

    private void setupRecyclerViewAdapter() {
        PhasesAdapter phaseAdapter = new PhasesAdapter(getActivity(), mCycle,
                new ArrayList<Phase>());
        RecyclerViewMaterialAdapter viewAdapter = new RecyclerViewMaterialAdapter(phaseAdapter);
        this.phasesAdapter = new MaterialPhaseBinder(viewAdapter, phaseAdapter);
        onItemClickListener = new PhasesAdapter.OnItemClickListener() {
            @Override public void onPhaseClicked(Phase phase, String cycleName
                    , int cycleColor) {
                if (CycleFragment.this.phaseListListener != null && phase != null) {
                    if (phase.getType().equals(Phase.TYPE_DISCUSSION)) {
                        CycleFragment.this.phaseListListener.onSubjectPhaseClicked(phase, cycleName,
                                cycleColor);
                    }
                    else {
                        if (mCycle.getCompilationPdf() != null) {
                            CycleFragment.this.phaseListListener.onReportPhaseClicked(
                                    phase, mCycle.getCompilationPdf(), mCycle.getName());
                        }
                    }
                }
            }
        };
        this.phasesAdapter.setOnItemClickListener(onItemClickListener);
        this.rv_phases.setAdapter(phasesAdapter.getAdapter());
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), rv_phases, null);
    }

    private void initialize() {
        if (phaseListPresenter == null) phaseListPresenter = PresenterFactory.newPhaseListPresenter(mCycle);
        this.phaseListPresenter.setView(this);
        this.phaseListPresenter.initialize();
    }

    @Override public void onResume() {
        super.onResume();
        this.phaseListPresenter.resume();
    }

    @Override public void onPause() {
        super.onPause();
        this.phaseListPresenter.pause();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        this.phaseListPresenter.destroy();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @OnClick(R.id.retry_button)
    void onButtonRetryClick() {
        CycleFragment.this.loadPhaseList();
    }

    @Override
    public void renderPhaseList(List<Phase> phaseList) {
        if (phaseList != null) this.phasesAdapter.setPhaseList(phaseList);
    }


}
