package com.inventos.plataformab.presentation.view.adapter.comment;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.Like;
import com.inventos.plataformab.presentation.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int LOAD_MORE_BUTTON = 0;
    public static final int COMMENT = 1;
    public static final int PAGE_SIZE = 10;

    public interface OnItemClickListener {
        void onAnswerButtonClick(int commentId);
        void onLoadMoreButtonClick();
        void onLikeButtonClick(int commentId);
        void onRemoveLikeButtonClick(int commentId);
        void onDislikeButtonClick(int commentId);
        void onRemoveDislikeButtonClick(int commentId);
    }

    private List<Comment> commentList;
    private final LayoutInflater layoutInflater;
    private final int cycleColor;
    private final Context context;
    private final User currentUser;
    private final int commentLevel;
    private OnItemClickListener onItemClickListener;
    private int page = 0;
    private int blackColor;
    private final boolean phaseHasEnded;

    public CommentAdapter(Context context, List<Comment> commentList,
                          User currentUser, int commentLevel,
                          int cycleColor, boolean phaseHasEnded) {
        this.validateCommentList(commentList);
        this.context = context;
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.cycleColor = cycleColor;
        this.commentList = commentList;
        this.currentUser = currentUser;
        this.commentLevel = commentLevel;
        this.blackColor = this.context.getResources().getColor(R.color.input_black);
        this.phaseHasEnded = phaseHasEnded;
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        if (viewType == COMMENT) {
            view = this.layoutInflater.inflate(R.layout.row_comment, parent, false);
            viewHolder = new CommentViewHolder(view);
        }
        else {
            view = this.layoutInflater.inflate(R.layout.view_load_more_comments, parent, false);
            viewHolder = new LoadMoreViewHolder(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof CommentViewHolder) {
            final Comment comment = this.commentList.get(position-1);
            CommentViewHolder holder = (CommentViewHolder) viewHolder;
            this.bindCommentView(holder, comment);

        }
        else {
            final LoadMoreViewHolder holder = (LoadMoreViewHolder) viewHolder;
            this.bindLoadMoreView(holder);
        }
    }

    @Override
    public int getItemCount() {
        return (this.commentList == null) ? 0 : this.commentList.size() + 1;
    }

    private void validateCommentList(List<Comment> commentList) {
        if (commentList == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    public void setComments(List<Comment> comments) {
        this.commentList = comments;
        if (comments.size() < PAGE_SIZE && page != 0) {
            page = -1;
        }
        this.notifyDataSetChanged();
    }

    public void addComments(List<Comment> comments) {
        if (comments != null){
            this.commentList.addAll(0, comments);
            if (comments.size() < PAGE_SIZE) {
                page = -1;
            }
            this.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return LOAD_MORE_BUTTON;
        }
        return COMMENT;
    }

    public void addComment(Comment comment) {
        if (this.commentList == null)
            this.commentList = new ArrayList<>();
        this.commentList.add(comment);
        this.notifyDataSetChanged();
    }

    private void bindLoadMoreView(final LoadMoreViewHolder holder) {
        holder.rl_progress.setVisibility(View.GONE);
        holder.bt_load_more.setVisibility(View.GONE);
        if (page != -1 && commentList.size()/PAGE_SIZE == page + 1){
            holder.bt_load_more.setVisibility(View.VISIBLE);
            holder.bt_load_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommentAdapter.this.onItemClickListener != null) {
                        holder.rl_progress.setVisibility(View.VISIBLE);
                        holder.bt_load_more.setVisibility(View.GONE);
                        CommentAdapter.this.onItemClickListener.onLoadMoreButtonClick();
                    }
                }
            });
        }
    }

    private Spannable getFormattedHtml(String content) {
        Spannable spannable = (Spannable) Html.fromHtml(content);
        for (URLSpan u: spannable.getSpans(0, spannable.length(), URLSpan.class)) {
            spannable.setSpan(new UnderlineSpan() {
                public void updateDrawState(TextPaint tp) {
                    tp.setUnderlineText(false);
                }
            }, spannable.getSpanStart(u), spannable.getSpanEnd(u), 0);
        }
        return spannable;
    }


    private void bindCommentView(final CommentViewHolder holder, final Comment comment){

        holder.dateDivider.setTextColor(cycleColor);
        holder.hourDivider.setTextColor(cycleColor);
        holder.date.setText(comment.getCommentDate());
        holder.hour.setText(comment.getCommentTime());
        holder.tv_like_count.setText(Integer.toString(comment.getLikesCount()));
        holder.tv_dislike_count.setText(Integer.toString(comment.getDislikesCount()));
        holder.bt_arrow_like.setColorFilter(blackColor);
        holder.bt_arrow_dislike.setColorFilter(blackColor);
        holder.comment.setText(getFormattedHtml(comment.getContent()));

        if (comment.getUser() == null) comment.setUser(currentUser);
        String userName;
        String pictureUrl;
        if (comment.isAnonymous()){
            userName = comment.getUser().getAliasName();
            pictureUrl = comment.getUser().getAnonymousPictureUrl();
        }
        else {
            userName = comment.getUser().getName();
            pictureUrl = comment.getUser().getPicture();
        }
        holder.userName.setText(userName);
        Picasso.with(context).load(pictureUrl).into(holder.userPicture);

        if (holder.comment.getLineCount() > 0) {
            holder.initComment();
        }
        else {
            holder.comment.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (holder.comment.getLineCount() != 0) {
                        holder.initComment();
                        holder.comment.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            });
        }

        if (currentUser != null) {
            if (comment.getHasLiked(currentUser.getId())) {
                holder.bt_arrow_like.setColorFilter(cycleColor);
            }
            if (comment.getHasDisliked(currentUser.getId())) {
                holder.bt_arrow_dislike.setColorFilter(cycleColor);
            }

            holder.like_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int likes = Integer.parseInt(holder.tv_like_count.getText().toString());
                    if (comment.getHasLiked(currentUser.getId())){
                        CommentAdapter.this.onItemClickListener.onRemoveLikeButtonClick(comment.getId());
                        likes--;
                        comment.removeLike(currentUser.getId());
                        comment.setHasLiked(false);
                        holder.bt_arrow_like.setColorFilter(blackColor);
                    }
                    else {
                        CommentAdapter.this.onItemClickListener.onLikeButtonClick(comment.getId());
                        likes++;
                        comment.setHasLiked(true);
                        comment.removeDislike(currentUser.getId());
                        comment.addLike(new Like(comment.getId(), currentUser.getId(), true));
                        int dislikes = comment.getDislikes().size();
                        holder.tv_dislike_count.setText(Integer.toString(dislikes));
                        holder.bt_arrow_like.setColorFilter(cycleColor);
                        holder.bt_arrow_dislike.setColorFilter(blackColor);
                    }
                    holder.tv_like_count.setText(Integer.toString(likes));
                }
            });

            holder.dislike_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = Integer.parseInt(holder.tv_dislike_count.getText().toString());
                    if (comment.getHasDisliked(currentUser.getId())) {
                        CommentAdapter.this.onItemClickListener.onRemoveDislikeButtonClick(comment.getId());
                        count--;
                        comment.removeDislike(currentUser.getId());
                        comment.setHasDisliked(false);
                        holder.bt_arrow_dislike.setColorFilter(blackColor);
                    }
                    else {
                        CommentAdapter.this.onItemClickListener.onDislikeButtonClick(comment.getId());
                        count++;
                        comment.setHasDisliked(true);
                        comment.removeLike(currentUser.getId());
                        comment.addDislike(new Like(comment.getId(), currentUser.getId(), true));
                        int likes = comment.getLikes().size();
                        holder.tv_like_count.setText(Integer.toString(likes));
                        holder.bt_arrow_dislike.setColorFilter(cycleColor);
                        holder.bt_arrow_like.setColorFilter(blackColor);
                    }
                    holder.tv_dislike_count.setText(Integer.toString(count));
                }
            });
        }
        if ( comment.getChildrenCount() > 0 && this.commentLevel < 3){
            holder.seeAnswers.setVisibility(View.VISIBLE);
            String answers = "ver + " + comment.getChildrenCount() + " respostas";
            holder.seeAnswers.setText(answers);
            holder.seeAnswers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommentAdapter.this.onItemClickListener != null) {
                        CommentAdapter.this.onItemClickListener.onAnswerButtonClick(
                                comment.getId());
                    }
                }
            });
        }
        else {
            holder.seeAnswers.setVisibility(View.GONE);
        }
        if (! phaseHasEnded && this.commentLevel < 3) {
            holder.answer.setVisibility(View.VISIBLE);
            holder.answer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommentAdapter.this.onItemClickListener != null) {
                        CommentAdapter.this.onItemClickListener.onAnswerButtonClick(
                                comment.getId());
                    }
                }
            });
        }
        else {
            holder.answer.setVisibility(View.GONE);
        }
    }


    static class CommentViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.user_name) TextView userName;
        @Bind(R.id.user_picture) ImageView userPicture;
        @Bind(R.id.comment) TextView comment;
        @Bind(R.id.date) TextView date;
        @Bind(R.id.hour) TextView hour;
        @Bind(R.id.date_divider) TextView dateDivider;
        @Bind(R.id.hour_divider) TextView hourDivider;
        @Bind(R.id.see_answers) TextView seeAnswers;
        @Bind(R.id.answer_button) Button answer;
        @Bind(R.id.expand_button) Button expand_button;
        @Bind(R.id.tv_like_count) TextView tv_like_count;
        @Bind(R.id.tv_dislike_count) TextView tv_dislike_count;

        @Bind(R.id.bt_arrow_like) ImageView bt_arrow_like;
        @Bind(R.id.bt_arrow_dislike) ImageView bt_arrow_dislike;

        @Bind(R.id.like_button) RelativeLayout like_button;
        @Bind(R.id.dislike_button) RelativeLayout dislike_button;

        int commentLineNumber = 4;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            like_button.setClickable(true);
            dislike_button.setClickable(true);
        }

        public void initComment() {
            comment.setMaxLines(commentLineNumber);
            collapseComment();
            int visibility;
            if (comment.getLineCount() > commentLineNumber) visibility = View.VISIBLE;
            else visibility = View.GONE;
            expand_button.setVisibility(visibility);
        }

        @OnClick({R.id.comment, R.id.expand_button}) void onClick() {
            if (comment.getLineCount() > commentLineNumber &&
                    comment.getMaxLines() == commentLineNumber){
                expandComment();
            }
            else if (comment.getMaxLines() != commentLineNumber){
                collapseComment();
            }
        }

        private void expandComment() {
            ObjectAnimator animation = ObjectAnimator.ofInt(comment, "maxLines", comment.getLineCount());
            expand_button.setText("Voltar");
            animation.setDuration(200).start();
        }

        private void collapseComment() {
            ObjectAnimator animation = ObjectAnimator.ofInt(comment, "maxLines", commentLineNumber);
            expand_button.setVisibility(View.VISIBLE);
            expand_button.setText("Continue Lendo …");
            animation.setDuration(200).start();
        }
    }

    static class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progress) RelativeLayout rl_progress;
        @Bind(R.id.load_more_comments) Button bt_load_more;

        public LoadMoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
