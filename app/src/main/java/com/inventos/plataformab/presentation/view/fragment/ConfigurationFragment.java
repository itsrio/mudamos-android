package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.presenter.configuration.ConfigurationPresenter;
import com.inventos.plataformab.presentation.presenter.signup.SignUpPresenter;
import com.inventos.plataformab.presentation.view.ConfigurationView;
import com.inventos.plataformab.presentation.view.activity.ConfigurationActivity;
import com.inventos.plataformab.presentation.view.adapter.configuration.ConfigurationAdapter;
import com.inventos.plataformab.presentation.view.adapter.configuration.ConfigurationAdapter.ItemClickListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.PresenterFactory;

public class ConfigurationFragment extends BaseFragment implements ConfigurationView, ItemClickListener {

    @Bind(R.id.rv_configurations)
    ListView rv_configurations;

    public static ConfigurationFragment newInstance() {
        return new ConfigurationFragment();
    }

    public interface FragmentClickListener {
        void openProfileEdit();
        void openChangePassword();
    }

    private FragmentClickListener fragmentClickListener;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_configuration, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ConfigurationActivity) {
            this.fragmentClickListener = (ConfigurationActivity) activity;
        }
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override public void onStart() {
        super.onStart();
        ArrayList<String> options = new ArrayList<>();
        options.add("Editar"); options.add("Senha");
        ConfigurationAdapter adapter = new ConfigurationAdapter(this.getActivity(), options);
        adapter.setItemClickListener(this);
        this.rv_configurations.setAdapter(adapter);
        this.rv_configurations.setOnItemClickListener(adapter);
    }

    @Override
    public void openProfileEdit() {
        this.fragmentClickListener.openProfileEdit();
    }

    @Override
    public void openChangePassword() {
        this.fragmentClickListener.openChangePassword();
    }
}
