package com.inventos.plataformab.presentation.view;

import com.facebook.CallbackManager;

public interface LoginView {
    void showError(String message);
    void redirectToMain();
    void showLoading();
    void hideLoading();
    void hideForgotPassword();
    void showForgotPassword();
    void hideMain();
    void showMain();
    void close();
    void setFacebookCallbackManager(CallbackManager callbackManager);
    void redirectToSignUp(String user);
}
