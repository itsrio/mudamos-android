package com.inventos.plataformab.presentation.presenter.main;

import android.support.annotation.NonNull;
import android.util.Log;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetUserUseCase;
import com.inventos.plataformab.domain.interactor.MainRequestUseCase;
import com.inventos.plataformab.domain.interactor.SaveCyclesUseCase;
import com.inventos.plataformab.domain.interactor.SaveSettingsUseCase;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.MainView;

import java.util.List;

public class MainPresenter implements Presenter {

    private final String TAG = "SplashPresenter";

    protected MainView mView;

    protected GetUserUseCase getUserUseCase;
    protected MainRequestUseCase mainRequestUseCase;
    protected SaveCyclesUseCase mSaveCyclesUseCase;
    protected SaveSettingsUseCase mSaveSettingsUseCase;

    public MainPresenter() {}

    public MainPresenter(
            GetUserUseCase getUserUseCase, MainRequestUseCase mainRequestUseCase
            , SaveCyclesUseCase saveCyclesUseCase, SaveSettingsUseCase saveSettingsUseCase) {
        this.getUserUseCase = getUserUseCase;
        this.mainRequestUseCase = mainRequestUseCase;
        this.mSaveCyclesUseCase = saveCyclesUseCase;
        this.mSaveSettingsUseCase= saveSettingsUseCase;
    }

    public void setView(@NonNull MainView view) {
        mView = view;
    }

    @Override
    public void start() {
        makeMainRequest();
    }

    public void makeMainRequest() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getAllData();
    }

    private void getAllData() {
        mainRequestUseCase.execute(new MainRequestSubscriber());
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


    private void showViewLoading() {
        this.mView.showLoading();
    }

    private void hideViewRetry() {
        this.mView.hideRetry();
    }

    private void showViewRetry() {
        this.mView.showRetry();
    }

    private void hideViewLoading() {
        this.mView.hideLoading();
    }

    private void saveCycles(List<Cycle> cycles) {
        if (mSaveCyclesUseCase != null) {
            this.mSaveCyclesUseCase.setCycles(cycles);
            this.mSaveCyclesUseCase.execute(new DefaultSubscriber());
        }
    }

    private void saveSettings(Settings settings) {
        if (mSaveSettingsUseCase != null) {
            this.mSaveSettingsUseCase.setSettings(settings);
            this.mSaveSettingsUseCase.execute(new DefaultSubscriber());
        }
    }

    private void renderMainData(Settings settings){
        mView.showMain();
        this.mView.renderData(settings);
    }

    private final class MainRequestSubscriber extends DefaultSubscriber<MainRequest> {
        @Override public void onCompleted() {
            hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            //UserListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            hideViewLoading();
            showViewRetry();
            Log.e(TAG, "onError " + e.toString());
        }

        @Override public void onNext(MainRequest mainRequest) {
            saveCycles(mainRequest.getCycles());
            saveSettings(mainRequest.getSettings());
            renderMainData(mainRequest.getSettings());
        }
    }
}
