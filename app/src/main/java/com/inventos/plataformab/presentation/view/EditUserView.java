package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.State;
import com.inventos.plataformab.presentation.model.User;

import java.util.List;

public interface EditUserView extends LoadDataView{
    User getNewUserFields();
    void redirectToMain();
    void renderStateAndCitySpinners(List<State> stateList);
    void renderSignUpComponents(SignUpComponents components);
    void hideMain();
    void showMain();
}
