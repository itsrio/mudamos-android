package com.inventos.plataformab.presentation.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.inventos.plataformab.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AnonymousDialog extends Dialog {

    public interface AnonymousDialogClickListener {
        void send(boolean asAnonymous);
    }

    @Bind(R.id.alias_text_view) TextView alias;

    private final AnonymousDialogClickListener listener;

    public AnonymousDialog(Context context, String aliasName,
                           AnonymousDialogClickListener listener) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog_confirm_anonymous);
        ButterKnife.bind(this, getWindow().getDecorView());
        alias.setText(aliasName.toUpperCase());
        this.listener = listener;
    }

    @OnClick(R.id.yes_button) void sendAsAnonymous() {
        this.listener.send(true);
        this.dismiss();
    }

    @OnClick(R.id.no_button) void sendNormally() {
        this.listener.send(false);
        this.dismiss();
    }

}
