package com.inventos.plataformab.presentation.presenter.splash;

import android.support.annotation.NonNull;
import android.util.Log;

import com.inventos.plataformab.MudamosApplication;
import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetUserUseCase;
import com.inventos.plataformab.domain.interactor.MainRequestUseCase;
import com.inventos.plataformab.domain.interactor.SaveCyclesUseCase;
import com.inventos.plataformab.domain.interactor.SaveSettingsUseCase;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.main.MainPresenter;
import com.inventos.plataformab.presentation.view.SplashView;

import java.util.List;

public class SplashPresenter extends MainPresenter {

    private final String TAG = "SplashPresenter";

    SplashView splashView;


    public SplashPresenter(GetUserUseCase getUserUseCase, MainRequestUseCase mainRequestUseCase) {
        this.getUserUseCase = getUserUseCase;
        this.mainRequestUseCase = mainRequestUseCase;
    }

    public void setView(@NonNull SplashView view) {
        mView = view;
        splashView = view;
    }

    @Override
    public void start() {
        checkIfUserIsLogged();
        makeMainRequest();
    }

    private void checkIfUserIsLogged() {
        getUserUseCase.execute(new GetUserSubscriber());
    }


    public void loginButtonPressed() {
        splashView.navigateToLogin();
    }

    public void exploreButtonPressed() {
        splashView.navigateToMain();
    }

    public void signupButtonPressed() {
        splashView.navigateToSignUp();
    }

    private final class GetUserSubscriber extends DefaultSubscriber<User> {
        @Override public void onCompleted() {
            //SplashPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            //UserListPresenter.this.hideViewLoading();
            //UserListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            //UserListPresenter.this.showViewRetry();
        }

        @Override public void onNext(User user) {
            //UserListPresenter.this.showUsersCollectionInView(users);
            if (user != null){
                MudamosApplication.setAuthenticationToken(user.getAuthToken());
                splashView.navigateToMainWithUser(user);
            }
        }
    }

    private void showViewLoading() {
        this.splashView.showLoading();
    }

    private void hideViewRetry() {
        this.splashView.hideRetry();
    }

    private void showViewRetry() {
        this.splashView.showRetry();
    }

    private void hideViewLoading() {
        this.splashView.hideLoading();
    }

    private void hideButtons() {
        this.splashView.hideMain();
    }

    private void showButtons() {
        this.splashView.showMain();
    }

    private void saveCycles(List<Cycle> cycles) {
        this.showButtons();
        this.mSaveCyclesUseCase.setCycles(cycles);
        this.mSaveCyclesUseCase.execute(new DefaultSubscriber());
    }

    private void saveSettings(Settings settings) {
        this.mSaveSettingsUseCase.setSettings(settings);
        this.mSaveSettingsUseCase.execute(new DefaultSubscriber());
    }

    private void renderMainData(Settings settings){
        this.splashView.showMain();
        this.splashView.renderData(settings);
    }

    private final class MainRequestSubscriber extends DefaultSubscriber<MainRequest> {
        @Override public void onCompleted() {
            SplashPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            //UserListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            SplashPresenter.this.hideViewLoading();
            SplashPresenter.this.showViewRetry();
            SplashPresenter.this.hideButtons();
            Log.e(TAG, "onError " + e.toString());
        }

        @Override public void onNext(MainRequest mainRequest) {
            SplashPresenter.this.renderMainData(mainRequest.getSettings());
        }
    }
}
