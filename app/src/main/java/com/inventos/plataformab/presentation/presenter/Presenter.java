package com.inventos.plataformab.presentation.presenter;

public interface Presenter {
    void start();
    void resume();
    void pause();
    void destroy();
}
