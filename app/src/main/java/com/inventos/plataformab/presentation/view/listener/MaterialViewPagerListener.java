package com.inventos.plataformab.presentation.view.listener;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.view.adapter.CyclePagerAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MaterialViewPagerListener implements MaterialViewPager.Listener,
        ViewPager.OnPageChangeListener{
    List<Cycle> cyclesList;
    CyclePagerAdapter pagerAdapter;
    ImageView headerLogo;
    int initialCycleIndex = 0;
    String logoUrl;
    Context context;

    public MaterialViewPagerListener(List<Cycle> cyclesList, CyclePagerAdapter pagerAdapter,
                                     ImageView headerLogo, String logoUrl, Context context) {
        this.cyclesList = cyclesList;
        this.pagerAdapter = pagerAdapter;
        this.initialCycleIndex = this.pagerAdapter.getCount() - this.cyclesList.size();
        this.headerLogo = headerLogo;
        this.logoUrl = logoUrl;
        this.context = context;
    }

    @Override
    public HeaderDesign getHeaderDesign(int page) {
        if (page == 0) {
            return HeaderDesign.fromColorResAndUrl(R.color.toolbar_color,
                    "http://testes.mudamos.org/assets/Floresta_1-e237b524a0dc24398dd51df13c6d718055480fcf487858d48114b03ead893be9.jpg");
        }
        else {
            Cycle currentCycle = cyclesList.get(page-1);
            String imageUrl= currentCycle.getPicture();
            return HeaderDesign.fromColorAndUrl(
                    Color.parseColor(currentCycle.getColor()), imageUrl);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        this.changeViewPagerTitle(position);
    }

    @Override
    public void onPageSelected(int position) {
        this.changeViewPagerTitle(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void changeViewPagerTitle(int position) {
        Picasso.with(context).load(logoUrl).into(headerLogo);
    }

}
