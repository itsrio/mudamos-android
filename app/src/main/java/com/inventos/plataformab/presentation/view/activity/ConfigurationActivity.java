package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.view.fragment.ConfigurationFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import factory.FragmentFactory;

public class ConfigurationActivity extends BaseActivity implements ConfigurationFragment.FragmentClickListener{
    public final static String CONFIG_FRAGMENT_FLAG = "config.fragment";
    @Bind(R.id.toolbar) Toolbar mToolbar;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, ConfigurationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "ConfigurationActivity";
        super.onCreate(savedInstanceState);
        //instantiate callbacks manager
        setContentView(R.layout.activity_configuration);
        initializeActivity();
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.configurations));
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFragment(R.id.fragment_container, FragmentFactory.newConfigurationFragment(),
                CONFIG_FRAGMENT_FLAG);
    }

    @Override
    public void openProfileEdit() {
        this.navigator.navigateToEditUserActivity(this);
    }

    @Override
    public void openChangePassword() {
        this.navigator.navigatoToChangePasswordActivity(this);
    }
}
