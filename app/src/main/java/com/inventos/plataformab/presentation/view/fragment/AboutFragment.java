package com.inventos.plataformab.presentation.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.google.gson.Gson;
import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.About;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.view.adapter.about.AboutCardAdapter;
import com.inventos.plataformab.presentation.view.adapter.about.AboutLayoutManager;
import com.inventos.plataformab.presentation.view.adapter.about.MaterialAboutBinder;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AboutFragment extends BaseFragment{
    public static final String SETTINGS = "settings";

    @Bind(R.id.rv_about) RecyclerView rv_about;

    MaterialAboutBinder aboutBinder;

    Settings settings;

    public static AboutFragment newInstance(Settings settings) {
        Gson gson = new Gson();
        AboutFragment splashFragment = new AboutFragment();
        Bundle argumentsBundle = new Bundle();
        String serializedSettings = gson.toJson(settings, Settings.class);
        argumentsBundle.putString(SETTINGS, serializedSettings);
        splashFragment.setArguments(argumentsBundle);
        return splashFragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, fragmentView);
        initSettings(getArguments().getString(SETTINGS));
        return fragmentView;
    }

    private void initSettings(String serializedSettings) {
        if (serializedSettings != null) {
            Gson gson = new Gson();
            this.settings = gson.fromJson(serializedSettings, Settings.class);
        }
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.initialize();
    }


    private void initialize() {
        this.setupAboutCards();
    }

    public void setupAboutCards() {
        AboutLayoutManager phaseLayoutManager = new AboutLayoutManager(getActivity());
        this.rv_about.setLayoutManager(phaseLayoutManager);
        AboutCardAdapter aboutAdapter = new AboutCardAdapter(getActivity(), this.settings);
        RecyclerViewMaterialAdapter viewAdapter = new RecyclerViewMaterialAdapter(aboutAdapter);
        this.aboutBinder = new MaterialAboutBinder(viewAdapter, aboutAdapter);
        this.rv_about.setAdapter(aboutBinder.getAdapter());
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), rv_about, null);
    }

    private ArrayList<About> getAboutList() {
        ArrayList<About> aboutList = new ArrayList<>(3);
        About about = new About("O QUE É?", "Somos uma plataforma online para você opinar com liberdade e segurança sobre temas importantes de interesse público, ajudando na construção democrática de soluções. Nosso objetivo é criar um debate informado com a participação de pessoas de vários setores da sociedade.");
        aboutList.add(about);
        about = new About("COMO FUNCIONA?", "Os temas são tratados em ciclos, nos quais é possível defender suas ideias e interagir com outros participantes. Todo o processo é aberto e será acompanhado de perto por um Comitê que garante transparência no debate. Ao final de cada ciclo, as contribuições são organizadas em documentos e entregues aos responsáveis pela implementação das mudanças, como gestores públicos, representantes do Legislativo, Judiciário e setor privado.");
        aboutList.add(about);
        about = new About("QUEM ESTÁ POR TRÁS?", "Mudamos foi idealizado e desenvolvido pelo Instituto de Tecnologia e Sociedade (ITS Rio) com o apoio de parceiros para a sua realização e o acompanhamento de consultores técnicos para cada tema debatido.\n" +
                "O projeto é financiado pela Open Society Foundations, pelo Instituto Arapyaú e quem mais quiser apoiar. Caso tenha interesse, acesse este link.");
        aboutList.add(about);
        return aboutList;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
