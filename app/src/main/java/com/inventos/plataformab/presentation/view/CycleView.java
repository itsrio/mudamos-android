package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.Phase;

import java.util.List;

public interface CycleView extends LoadDataView {
    void renderPhaseList(List<Phase> phaseList);
}
