package com.inventos.plataformab.presentation.view;

import com.inventos.plataformab.presentation.model.Phase;

public interface PhaseDetailsView extends LoadDataView {
    void renderPlugins(Phase phase);
}
