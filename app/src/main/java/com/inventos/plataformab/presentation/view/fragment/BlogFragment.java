package com.inventos.plataformab.presentation.view.fragment;

import com.inventos.plataformab.presentation.view.BlogView;

public class BlogFragment extends BaseFragment implements BlogView {
    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

}
