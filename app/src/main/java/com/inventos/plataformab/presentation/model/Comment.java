package com.inventos.plataformab.presentation.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Comment extends BaseComponent {

    private int id;
    private User user;
    private int subject;
    private Date createdAt;
    private int likesCount;
    private int dislikesCount;
    private int childrenCount;
    private List<Like> likes;
    private List<Like> dislikes;
    private boolean isAnonymous;
    private String content;
    private Boolean hasLiked;
    private Boolean hasDisliked;

    public Comment() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getSubjectId() {
        return subject;
    }

    public void setSubjectId(int subject) {
        this.subject = subject;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getLikesCount() {
        return (this.likes == null) ? likesCount : likes.size();
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getDislikesCount() {
        return (this.dislikes == null) ? dislikesCount : dislikes.size();
    }

    public void setDislikesCount(int dislikesCount) {
        this.dislikesCount = dislikesCount;
    }

    public int getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(int childrenCount) {
        this.childrenCount = childrenCount;
    }

    public boolean isAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCommentTime() {
        DateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return df.format(this.createdAt);
    }

    public String getCommentDate() {
        DateFormat df = new SimpleDateFormat("d MMM, yy", Locale.getDefault());
        return df.format(this.createdAt);
    }

    public Boolean getUserHasLiked(int userId) {
        int likeSize = this.likes.size();
        Like tempLike;
        for (int i = 0; i < likeSize; i++) {
            tempLike = this.likes.get(i);
            if (tempLike.getUserId() == userId) {
                return true;
            }
        }

        return null;
    }

    public Boolean getHasLiked(int userId) {
        if (hasLiked != null) return hasLiked;
        int likeSize = this.likes.size();
        Like tempLike;
        for (int i = 0; i < likeSize; i++) {
            tempLike = this.likes.get(i);
            if (tempLike.getUserId() == userId) {
                hasLiked = true;
                break;
            }
        }
        if (hasLiked == null) hasLiked = false;
        return hasLiked;
    }

    public Boolean getHasDisliked(int userId) {
        if (hasDisliked != null) return hasDisliked;
        int dislikeSize = this.dislikes.size();
        Like tempLike;
        for (int i = 0; i < dislikeSize; i++) {
            tempLike = this.dislikes.get(i);
            if (tempLike.getUserId() == userId) {
                hasDisliked = false;
                break;
            }
        }
        if (hasDisliked == null) hasDisliked = false;
        return hasDisliked;
    }

    public void setHasLiked(Boolean hasLiked) {
        this.hasLiked = hasLiked;
    }

    public void setHasDisliked(Boolean hasDisliked) {
        this.hasDisliked = hasDisliked;
    }

    public void removeLike(int userId) {
        int likeSize = this.likes.size();
        Like tempLike;
        for (int i = 0; i < likeSize; i++) {
            tempLike = this.likes.get(i);
            if (tempLike.getUserId() == userId) {
                this.likes.remove(i);
                this.hasLiked = false;
                break;
            }
        }
    }

    public void removeDislike(int userId) {
        int dislikeSize = this.dislikes.size();
        Like tempLike;
        for (int i = 0; i < dislikeSize; i++) {
            tempLike = this.dislikes.get(i);
            if (tempLike.getUserId() == userId) {
                this.dislikes.remove(i);
                this.hasDisliked = false;
                break;
            }
        }
    }

    public void addLike(Like like) {
        this.likes.add(like);
    }
    public void addDislike(Like dislike) {
        this.dislikes.add(dislike);
    }

    public List<Like> getLikes() {
        return likes;
    }

    public List<Like> getDislikes() {
        return dislikes;
    }
}
