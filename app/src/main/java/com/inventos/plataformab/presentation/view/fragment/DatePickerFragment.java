package com.inventos.plataformab.presentation.view.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    EditText textInput;

    int day; int month; int year;

    public void setEditText(EditText input) {
        this.textInput = input;
    }

    public void setDate() {
        if (textInput != null && !textInput.getText().toString().isEmpty() ) {
            String date = textInput.getText().toString();
            String[] parts = date.split("/");
            if (parts.length == 3) {
                day = Integer.parseInt(parts[0]);
                month = Integer.parseInt(parts[1]) - 1;
                year = Integer.parseInt(parts[2]);
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        setDate();
        if (year == 0){
            final Calendar c = Calendar.getInstance();
            day = c.get(Calendar.DAY_OF_MONTH);
            month = c.get(Calendar.MONTH);
            year = c.get(Calendar.YEAR);
        }

        // Create a new instance of TimePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        textInput.setText("");
        String date = String.format("%02d/%02d/%d", day, month + 1, year);
        textInput.setText(date);
    }
}
