package com.inventos.plataformab.presentation.presenter.notification;

import android.support.annotation.NonNull;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.GetNotificationsUseCase;
import com.inventos.plataformab.presentation.model.Notifications;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.NotificationView;

import java.util.List;

public class NotificationsPresenter implements Presenter {


    private GetNotificationsUseCase getNotificationsUseCase;
    private NotificationView notificationView;

    public NotificationsPresenter(GetNotificationsUseCase getNotificationsUseCase) {
        this.getNotificationsUseCase = getNotificationsUseCase;
    }

    public void setView(@NonNull NotificationView notificationView) {
        this.notificationView = notificationView;
    }

    @Override
    public void start() {
        notificationView.hideMain();
        notificationView.hideMain();
        notificationView.hideRetry();
        this.notificationView.showLoading();
        this.getNotificationsUseCase.execute(new NotificationsSubscriber());
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.getNotificationsUseCase.unsubscribe();
    }

    private void addNotifications(List<Notifications> notifications) {
        notificationView.addNotifications(notifications);
        notificationView.hideLoading();
        notificationView.showMain();
    }

    private class NotificationsSubscriber extends DefaultSubscriber<List<Notifications>> {
        @Override public void onError(Throwable e) {
            notificationView.hideLoading();
            notificationView.showError("Erro ao carregar notificações");
            notificationView.showRetry();
        }

        @Override public void onNext(List<Notifications> notifications) {
            if (notifications != null) {
                NotificationsPresenter.this.addNotifications(notifications);
            }
        }
    }
}
