package com.inventos.plataformab.presentation.view.adapter.phase;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Phase;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PhasesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int CYCLE_DESCRIPTION = 0;
    public static final int PHASE = 1;

    private Cycle mCycle;
    private int cycleColor;
    private List<Phase> phaseList;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;

    public PhasesAdapter(Context context, Cycle cycle, Collection<Phase> phasesCollection) {
        this.validatePhaseCollection(phasesCollection);
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mCycle = cycle;
        this.phaseList = cycle.getPhases();
        this.cycleColor = Color.parseColor(mCycle.getColor());
    }


    public interface OnItemClickListener {
        void onPhaseClicked(Phase phase, String cycleName, int cycleColor);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;
        if (viewType == PHASE) {
            view = this.layoutInflater.inflate(R.layout.card_phase, parent, false);
            viewHolder = new PhaseViewHolder(view);
        }
        else {
            view = this.layoutInflater.inflate(R.layout.card_base, parent, false);
            viewHolder = new BaseCardViewHolder(view);
        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return CYCLE_DESCRIPTION;
        }
        return PHASE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof BaseCardViewHolder) {
            BaseCardViewHolder holder = (BaseCardViewHolder) viewHolder;
            holder.cardTitle.setText(mCycle.getName().toUpperCase());
            holder.cardBody.setText(Html.fromHtml(mCycle.getDescription()));
            holder.cardLine.setBackgroundColor(cycleColor);
        }
        else{
            final Phase phase = this.phaseList.get(position-1);
            bindPhase(phase, (PhaseViewHolder) viewHolder, position);
        }
    }

    private void bindPhase(final Phase phase, PhaseViewHolder holder, int position) {
        holder.cardLine.setBackgroundColor(cycleColor);
        holder.cardButton.setTextColor(cycleColor);
        holder.cardTitle.setText(phase.getName().toUpperCase());
        holder.cardBody.setText(phase.getDescription());
        holder.cardButton.setText(phase.getButtonLabel());
        holder.cardButton.setClickable(true);
        if (mCycle.getCompilationPdf() == null && phase.getType().equals(Phase.TYPE_REPORT)) {
            holder.cardButton.setText("");
            holder.cardButton.setClickable(false);
        }
        holder.cardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PhasesAdapter.this.onItemClickListener != null) {
                    PhasesAdapter.this.onItemClickListener.onPhaseClicked(
                            phase, mCycle.getName(), cycleColor);
                }
            }
        });
        holder.phaseStatus.setTextColor(cycleColor);
        if (phase.isCurrent()) {
            holder.phaseStatus.setText(R.string.phase_in_progress);
        }
        else if (phase.hasEnded()) {
            holder.phaseStatus.setText("encerrada");
        }
        else {
            holder.phaseStatus.setText("em breve");
        }
        holder.phaseNumber.setText("fase " + position);
        holder.initialDate.setText("de " + phase.getFormatedDate(Phase.INITIAL));
        holder.finalDate.setText("até " + phase.getFormatedDate(Phase.FINAL));
        holder.phaseIcon.setBackgroundResource(phase.getIcon());
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setPhaseList(List<Phase> phaseList) {
        this.validatePhaseCollection(phaseList);
        this.phaseList = phaseList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (this.phaseList != null) ? this.phaseList.size()+1 : 0;
    }

    private void validatePhaseCollection(Collection<Phase> phasesCollection) {
        if (phasesCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class PhaseViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.card_title) TextView cardTitle;
        @Bind(R.id.phase_number) TextView phaseNumber;
        @Bind(R.id.phase_status) TextView phaseStatus;
        @Bind(R.id.initial_date) TextView initialDate;
        @Bind(R.id.final_date) TextView finalDate;
        @Bind(R.id.phase_icon) ImageView phaseIcon;
        @Bind(R.id.card_description) TextView cardBody;
        @Bind(R.id.card_line) View cardLine;
        @Bind(R.id.card_button) Button cardButton;
        public PhaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class BaseCardViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.card_line) View cardLine;
        @Bind(R.id.card_title) TextView cardTitle;
        @Bind(R.id.card_description) TextView cardBody;

        public BaseCardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
