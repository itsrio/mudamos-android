package com.inventos.plataformab.presentation.model;

import java.util.HashMap;
import java.util.List;

public class SignUpComponents {
    // Wrapper for request done in Signup activity
    List<String> aliasNames;
    HashMap<String, Integer> genders;
    List<Profile> profiles;

    public List<String> getAliasNames() {
        return aliasNames;
    }

    public void setAliasNames(List<String> aliasNames) {
        this.aliasNames = aliasNames;
    }

    public HashMap<String, Integer> getGenders() {
        return genders;
    }

    public void setGenders(HashMap<String, Integer> genders) {
        this.genders = genders;
    }

    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }
}
