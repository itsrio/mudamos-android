package com.inventos.plataformab.presentation.presenter.cycle;

import android.support.annotation.NonNull;
import android.util.Log;

import com.inventos.plataformab.domain.interactor.DefaultSubscriber;
import com.inventos.plataformab.domain.interactor.UseCase;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.presenter.Presenter;
import com.inventos.plataformab.presentation.view.CycleView;

import java.util.List;

public class PhaseListPresenter extends DefaultSubscriber<List<Phase>> implements Presenter {

    UseCase getPhaseListUseCase;
    private CycleView mCycleView;

    public PhaseListPresenter(UseCase getPhaseListUseCase) {
        this.getPhaseListUseCase = getPhaseListUseCase;
    }

    public void setView(@NonNull CycleView view) {
        this.mCycleView = view;
    }

    public void initialize() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getPhaseList();
    }

    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        getPhaseListUseCase.unsubscribe();
    }

    public void showViewLoading() {
        this.mCycleView.showLoading();
    }

    public void hideViewLoading() {
        this.mCycleView.hideLoading();
    }

    public void showViewRetry() {
        this.mCycleView.showRetry();
    }

    public void hideViewRetry() {
        this.mCycleView.hideRetry();
    }

    private void showPhasesCollectionInView(List<Phase> phasesCollection) {
        this.mCycleView.renderPhaseList(phasesCollection);
    }

    public void getPhaseList() {
        this.getPhaseListUseCase.execute(new PhaseListSubscriber());
    }

    private final class PhaseListSubscriber extends DefaultSubscriber<List<Phase>> {

        @Override public void onCompleted() {
            PhaseListPresenter.this.hideViewLoading();
        }

        @Override public void onError(Throwable e) {
            PhaseListPresenter.this.hideViewLoading();
            PhaseListPresenter.this.showViewRetry();
            Log.e("Phase List Presenter", e.getMessage());
        }

        @Override public void onNext(List<Phase> phases) {
            PhaseListPresenter.this.showPhasesCollectionInView(phases);
        }
    }


}
