package com.inventos.plataformab.presentation.view.widget;


import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;


import com.inventos.plataformab.R;

import java.util.ArrayList;

public class LabelledSpinner extends LinearLayout implements AdapterView.OnItemSelectedListener {

    private TextView mLabel;
    private Spinner mSpinner;
    private View mDivider;
    private OnItemChosenListener mOnItemChosenListener;
    private int mWidgetColor;


    public LabelledSpinner(Context context) {
        this(context, null);
    }

    public LabelledSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context, attrs);
    }

    private void initializeViews(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.LabelledSpinner,
                0,
                0);
        String labelText = typedArray.getString(R.styleable.LabelledSpinner_labelText);
        mWidgetColor = typedArray.getColor(R.styleable.LabelledSpinner_widgetColor,
                getResources().getColor(R.color.about_blue));
        typedArray.recycle();

        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.widget_labelled_spinner, this, true);

        setOrientation(LinearLayout.VERTICAL);
        setLayoutParams(new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        mLabel = (TextView) getChildAt(0);
        mLabel.setText(labelText);
        mLabel.setPadding(0, dpToPixels(16), 0, 0);
        mLabel.setTextColor(mWidgetColor);

        mSpinner = (Spinner) getChildAt(1);
        mSpinner.setPadding(0, dpToPixels(8), 0, dpToPixels(8));
        mSpinner.setOnItemSelectedListener(this);

        mDivider = getChildAt(2);
        MarginLayoutParams dividerParams =
                (MarginLayoutParams) mDivider.getLayoutParams();
        dividerParams.rightMargin = dpToPixels(4);
        dividerParams.bottomMargin = dpToPixels(8);
        mDivider.setLayoutParams(dividerParams);
        mDivider.setBackgroundColor(mWidgetColor);
        alignLabelWithSpinnerItem(4);
    }


    public TextView getLabel() {
        return mLabel;
    }

    public Spinner getSpinner() {
        return mSpinner;
    }

    public View getDivider() {
        return mDivider;
    }

    public void setLabelText(CharSequence labelText) {
        mLabel.setText(labelText);
    }

    public void setLabelText(@StringRes int labelTextId) {
        mLabel.setText(getResources().getString(labelTextId));
    }

    public CharSequence getLabelText() {
        return mLabel.getText();
    }

    public void setColor(@ColorRes int colorRes) {
        mLabel.setTextColor(getResources().getColor(colorRes));
        mDivider.setBackgroundColor(getResources().getColor(colorRes));
    }

    public int getColor() {
        return mWidgetColor;
    }

    public void setItemsArray(@ArrayRes int arrayResId) {
        setItemsArray(
                arrayResId,
                R.layout.spinner_item,
                android.R.layout.simple_spinner_dropdown_item
        );
    }

    public void setItemsArray(ArrayList<?> arrayList) {
        ArrayAdapter<?> adapter = new ArrayAdapter<>(
                getContext(),
                R.layout.spinner_item,
                arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
    }

    private void setItemsArray(@ArrayRes int arrayResId, @LayoutRes int spinnerItemRes, @LayoutRes int dropdownViewRes) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                getContext(),
                arrayResId,
                spinnerItemRes);
        adapter.setDropDownViewResource(dropdownViewRes);
        mSpinner.setAdapter(adapter);
    }

    public void setCustomAdapter(SpinnerAdapter adapter) {
        mSpinner.setAdapter(adapter);
    }

    public void setSelection(int position) {
        mSpinner.setSelection(position);
    }

    public void setSelection(int position, boolean animate) {
        mSpinner.setSelection(position, animate);
    }

    public interface OnItemChosenListener {

        void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id);

        void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView);
    }

    public void setOnItemChosenListener(OnItemChosenListener onItemChosenListener) {
        mOnItemChosenListener = onItemChosenListener;
    }

    public OnItemChosenListener getOnItemChosenListener() {
        return mOnItemChosenListener;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (mOnItemChosenListener != null) {
            // 'this' refers to this LabelledSpinner component
            mOnItemChosenListener.onItemChosen(this, parent, view, position, id);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        if (mOnItemChosenListener != null) {
            // 'this' refers to this LabelledSpinner component
            mOnItemChosenListener.onNothingChosen(this, parent);
        }
    }

    public void alignLabelWithSpinnerItem(boolean indentLabel) {
        if (indentLabel) {
            alignLabelWithSpinnerItem(8);
        } else {
            alignLabelWithSpinnerItem(4);
        }
    }

    private void alignLabelWithSpinnerItem(int indentDps) {
        MarginLayoutParams labelParams =
                (MarginLayoutParams) mLabel.getLayoutParams();
        labelParams.leftMargin = dpToPixels(indentDps);
        mLabel.setLayoutParams(labelParams);

        MarginLayoutParams dividerParams =
                (MarginLayoutParams) mDivider.getLayoutParams();
        dividerParams.leftMargin = dpToPixels(indentDps);
        mDivider.setLayoutParams(dividerParams);
    }

    private int dpToPixels(int dps) {
        if (dps == 0) {
            return 0;
        }
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }

}