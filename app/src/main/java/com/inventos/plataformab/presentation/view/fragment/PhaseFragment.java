package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.Plugin;
import com.inventos.plataformab.presentation.model.Subject;
import com.inventos.plataformab.presentation.presenter.phase.PhasePresenter;
import com.inventos.plataformab.presentation.view.PhaseDetailsView;
import com.inventos.plataformab.presentation.view.activity.PhaseActivity;
import com.inventos.plataformab.presentation.view.adapter.phase.PhaseLayoutManager;
import com.inventos.plataformab.presentation.view.adapter.plugin.PluginAdapter;
import com.inventos.plataformab.presentation.view.adapter.plugin.PluginAdapter.OnItemClickListener;
import com.inventos.plataformab.presentation.view.adapter.plugin.PluginLayoutManager;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class PhaseFragment extends BaseFragment implements PhaseDetailsView {

    public static final String PHASE_ID = "phase.id";
    public static final String CYCLE_COLOR = "cycle.color";

    @Bind(R.id.rv_plugins) RecyclerView rv_plugins;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;

    PhasePresenter mPresenter;

    PluginAdapter mPluginAdapter;

    private int phaseId;
    private int cycleColor;

    public interface PluginListener {
        void onSubjectClicked(Subject subject, int cycleColor, boolean phaseHasEnded);
    }

    PluginListener pluginListListener;

    public static PhaseFragment newInstance(int phaseId, int color) {
        PhaseFragment fragment = new PhaseFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(PHASE_ID, phaseId);
        bundle.putInt(CYCLE_COLOR, color);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PhaseActivity) {
            this.pluginListListener = (PluginListener) activity;
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_phase, container, false);
        phaseId = getArguments().getInt(PHASE_ID);
        cycleColor = getArguments().getInt(CYCLE_COLOR);
        ButterKnife.bind(this, fragmentView);
        setupRecyclerViewAdapter();
        loadPlugins();
        return fragmentView;
    }

    private void setupRecyclerViewAdapter() {
        mPluginAdapter = new PluginAdapter(getActivity(), cycleColor,
                new ArrayList<Plugin>());
        this.rv_plugins.setLayoutManager(new PluginLayoutManager(this.getActivity()));
        mPluginAdapter.setOnItemClickListener(onItemClickListener);
        this.rv_plugins.setAdapter(mPluginAdapter);
    }

    public void loadPlugins() {
        if (mPresenter == null)
            setPresenter(PresenterFactory.newPhasePresenter(phaseId));
        mPresenter.initialize(phaseId);
    }

    public void setPresenter(@NonNull PhasePresenter presenter) {
        this.mPresenter = presenter;
        mPresenter.setView(this);
    }


    @Override
    public void renderPlugins(Phase phase) {
        mPluginAdapter.addSubjects(phase.getSubjects());
        mPluginAdapter.setPhase(phase);
        if (phase.hasEnded() && phase.getType().equals(Phase.TYPE_DISCUSSION)) {
            showEndedDialog();
        }
    }

    private void showEndedDialog() {
        Dialog dialog = new AlertDialog.Builder(this.getActivity())
                .setMessage("Essa fase já está encerrada, portanto, as discussões estão modo somente leitura.")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}})
                .create();
        dialog.show();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @OnClick(R.id.retry_button)
    void onButtonRetryClick() {
        PhaseFragment.this.loadPlugins();
    }

    private OnItemClickListener onItemClickListener= new OnItemClickListener() {
        @Override
        public void onSubjectItemClicked(Subject subject, int cycleColor, boolean phaseHasEnded) {
            PhaseFragment.this.pluginListListener.onSubjectClicked(subject, cycleColor, phaseHasEnded);
        }
    };
}
