package com.inventos.plataformab.presentation.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.Notifications;
import com.inventos.plataformab.presentation.presenter.notification.NotificationsPresenter;
import com.inventos.plataformab.presentation.view.NotificationView;
import com.inventos.plataformab.presentation.view.adapter.NotificationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class NotificationsFragment extends BaseFragment implements NotificationView, NotificationAdapter.NotificationItemClickListener {

    @Bind(R.id.rv_notifications) RecyclerView rv_notifications;
    @Bind(R.id.progress) RelativeLayout progress;
    @Bind(R.id.rl_retry) RelativeLayout rl_retry;

    private NotificationsPresenter notificationPresenter;

    private NotificationFragmentListener notificationFragmentListener;

    @Override
    public void openComment(int commentId, int subjectId, Integer parentId) {
        ArrayList<Integer> parents = new ArrayList<>(2);

        if (parentId != null) parents.add(parentId);
        parents.add(commentId);

        this.notificationFragmentListener.openCommentChildren(subjectId, commentId, parents);
    }

    public interface NotificationFragmentListener {
        void openCommentChildren(int subjectId, int commentId, ArrayList<Integer> parents);
    }

    public static NotificationsFragment newInstance() {
        NotificationsFragment notificationsFragment = new NotificationsFragment();
        Bundle argumentsBundle = new Bundle();
        notificationsFragment.setArguments(argumentsBundle);
        return notificationsFragment;
    }

    public void setPresenter(@NonNull NotificationsPresenter presenter) {
        notificationPresenter = presenter;
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof NotificationFragmentListener) {
            this.notificationFragmentListener = (NotificationFragmentListener) activity;
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initNotificationsList();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (notificationPresenter != null) notificationPresenter.destroy();
    }

    private void initNotificationsList() {
        if (notificationPresenter == null)
            notificationPresenter = PresenterFactory.newNotificationsPresenter();
        notificationPresenter.setView(this);
        notificationPresenter.start();
    }

    @Override
    public void hideMain() {
        rv_notifications.setVisibility(View.GONE);
    }

    @Override
    public void showMain() {
        rv_notifications.setVisibility(View.VISIBLE);
    }

    @Override
    public void addNotifications(List<Notifications> notifications) {
        rv_notifications.setLayoutManager(new LinearLayoutManager(getActivity()));
        NotificationAdapter adapter = new NotificationAdapter(getActivity(), this);
        rv_notifications.setAdapter(adapter);
        adapter.setNotificationsList(notifications);
    }

    @Override
    public void showLoading() {
        this.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.progress.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @OnClick(R.id.retry_button) void retry() {
        initNotificationsList();
    }
}
