package com.inventos.plataformab.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import com.inventos.plataformab.R;
import com.inventos.plataformab.presentation.model.User;
import com.inventos.plataformab.presentation.presenter.login.ChangePasswordPresenter;
import com.inventos.plataformab.presentation.view.ChangePasswordView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import factory.PresenterFactory;

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordView {

    @Bind(R.id.toolbar) Toolbar mToolbar;
    @Bind(R.id.new_password_edit_text) EditText new_password_edit_text;
    @Bind(R.id.new_password_confirmation_edit_text) EditText new_password_confirmation_edit_text;
    @Bind(R.id.old_password_edit_text) EditText old_password_edit_text;

    ChangePasswordPresenter mPresenter;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, ChangePasswordActivity.class);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        this.TAG = "ChangePasswordActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initializeActivity();
    }

    private void initializeActivity() {
        ButterKnife.bind(this);
        initToolbar();
        initPresenter();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.change_password);
        }
    }

    private void initPresenter() {
        mPresenter = PresenterFactory.newChangePasswordPresenter(this);
        mPresenter.setView(this);
    }

    @Override
    public String getNewPassword(String newPassword) {
        return null;
    }

    @Override
    public void showErrorMessage(String message) {
        showToastMessage(message);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @OnClick(R.id.ok_button) void onChangePasswordClick() {
        String newPassword = new_password_edit_text.getText().toString();
        String passwordConfirmation = new_password_confirmation_edit_text.getText().toString();
        String oldPassword = old_password_edit_text.getText().toString();
        User user = getLoggedUserAsObject();
        if (newPassword.equals(passwordConfirmation)){
            if (oldPassword.equals(user.getPassword())) {
                mPresenter.changePassword(newPassword, user.getId());
            }
            else {
                showToastMessage("Senhas atual inválida");
            }
        }
        else {
            showToastMessage("Senhas não coincidem");
        }
    }
}
