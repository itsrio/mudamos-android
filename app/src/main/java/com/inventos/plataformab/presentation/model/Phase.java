package com.inventos.plataformab.presentation.model;

import com.google.gson.JsonObject;
import com.inventos.plataformab.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Phase extends PluginContainer {

    public final static String INITIAL = "initial";
    public final static String FINAL = "final";
    public final static String TYPE_DISCUSSION = "discussao";
    public final static String TYPE_REPORT = "relatoria";

    private int id;
    private String name;
    private String description;
    private String tooltip;
    private Date initialDate;
    private Date finalDate;
    private String pictureFileName;
    private String pictureContentType;
    private String pictureFileSize;
    private int cycleId;
    private boolean current;
    private String type;

    public Phase () {
    }

    public Phase (JsonObject jsonPhase, Date initialDate, Date finalDate) {
        this.id = jsonPhase.get("id").getAsInt();
        this.name = jsonPhase.get("name").getAsString();
        this.description = jsonPhase.get("description").getAsString();
        this.tooltip = jsonPhase.get("tooltip").getAsString();
        this.initialDate = initialDate;
        this.finalDate = finalDate;
        this.cycleId = jsonPhase.get("cycle_id").getAsInt();
        this.pictureFileName = jsonPhase.get("picture_file_name").getAsString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getPictureFileName() {
        return pictureFileName;
    }

    public void setPictureFileName(String pictureFileName) {
        this.pictureFileName = pictureFileName;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public String getPictureFileSize() {
        return pictureFileSize;
    }

    public void setPictureFileSize(String pictureFileSize) {
        this.pictureFileSize = pictureFileSize;
    }

    public int getCycleId() {
        return cycleId;
    }

    public void setCycleId(int cycleId) {
        this.cycleId = cycleId;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public String getFormattedTime(String type) {
        DateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
        if (type == INITIAL){
            return df.format(this.initialDate);
        }
        else {
            return df.format(this.finalDate);
        }
    }

    public String getFormatedDate(String type) {
        DateFormat df = new SimpleDateFormat("d MMM, yy", Locale.getDefault());
        if (type == INITIAL){
            return df.format(this.initialDate);
        }
        else {
            return df.format(this.finalDate);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIcon() {
        if (this.type == null || this.type.equals("discussao")) {
            return R.drawable.phase1;
        }
        else {
            return R.drawable.phase2;
        }
    }

    public int getButtonLabel() {
        if (this.type == null || this.type.equals("discussao")) {
            if (current) {
                return R.string.participate;
            }
            else {
                return R.string.see_how_it_was;
            }
        }
        else {
            return R.string.download_pdf;
        }
    }

    public boolean hasEnded() {
        return this.finalDate.before(Calendar.getInstance().getTime());
    }
}
