package com.inventos.plataformab.presentation.view.adapter.about;

import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;

public class MaterialAboutBinder {
    AboutCardAdapter mAboutAdapter;
    RecyclerViewMaterialAdapter mViewAdapter;

    public MaterialAboutBinder(RecyclerViewMaterialAdapter viewAdapter,
                               AboutCardAdapter aboutAdapter){
        mAboutAdapter = aboutAdapter;
        mViewAdapter = viewAdapter;
    }

    public RecyclerViewMaterialAdapter getAdapter() {
        return mViewAdapter;
    }

    public int getItemCount() {
        return this.mAboutAdapter.getItemCount();
    }

}
