package com.inventos.plataformab.presentation.model;

import com.google.gson.JsonObject;

public class Subject extends Plugin {
    private String question;
    private String enunciation;
    private boolean isAnonymous;

    public Subject() {

    }

    public Subject(JsonObject subjectJsonObject) {
        this.id = subjectJsonObject.get("id").getAsInt();
        this.pluginRelationId = subjectJsonObject.get("plugin_relation_id").getAsInt();
        this.title = subjectJsonObject.get("title").getAsString();
        this.question = subjectJsonObject.get("question").getAsString();
        this.enunciation = subjectJsonObject.get("enunciation").getAsString();
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getEnunciation() {
        return enunciation;
    }

    public void setEnunciation(String enunciation) {
        this.enunciation = enunciation;
    }

    public boolean isAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }
}
