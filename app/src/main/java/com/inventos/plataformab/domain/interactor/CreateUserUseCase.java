package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class CreateUserUseCase extends UseCase {

    public static final String TWITTER = "twitter";
    public static final String FACEBOOK = "facebook";
    public static final String GPLUS = "gplus";

    private final UserDataStore userRepository;
    private User user;
    private String mSocialNetwork;

    public CreateUserUseCase(UserDataStore userRepository, ThreadExecutor threadExecutor,
                       PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setSocialNetwork(String socialNetwork) {
        this.mSocialNetwork = socialNetwork;
    }

    @Override
    public Observable buildUseCaseObservable() {
        if (mSocialNetwork == null) mSocialNetwork = "";
        return this.userRepository.createUser(user, mSocialNetwork);
    }
}
