package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;

import rx.Observable;

public class SendCommentUseCase extends UseCase {

    private final CommentDataStore commentDataStore;
    private final int subjectId;
    private final int parentId;
    private boolean isAnonymous;
    private String comment;

    public SendCommentUseCase(CommentDataStore repository, int subjectId, int parentId, ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.commentDataStore = repository;
        this.subjectId = subjectId;
        this.parentId = parentId;
    }

    public void setComment(String comment, boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
        this.comment = comment;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.commentDataStore.sendComment(subjectId, parentId, comment, isAnonymous);
    }
}
