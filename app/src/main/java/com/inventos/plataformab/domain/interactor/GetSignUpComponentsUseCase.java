package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.SignUpRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.SignUpComponents;

import rx.Observable;

public class GetSignUpComponentsUseCase extends UseCase {
    SignUpRepository repository;
    public GetSignUpComponentsUseCase(SignUpRepository repository,
                                      ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public Observable<SignUpComponents> buildUseCaseObservable() {
        return this.repository.getComponents();
    }
}
