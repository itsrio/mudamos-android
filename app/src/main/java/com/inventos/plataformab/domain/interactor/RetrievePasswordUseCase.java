package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.UserDataRepository;
import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class RetrievePasswordUseCase extends UseCase {
    private UserDataStore repository;
    private User user;

    public RetrievePasswordUseCase(UserDataStore repository, ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void setEmail(String email) {
        user = new User();
        user.setEmail(email);
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.repository.retrievePassword(user);
    }
}
