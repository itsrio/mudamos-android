package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class GetLoggedUserUseCase extends UseCase {

    private final UserDataStore repository;

    public GetLoggedUserUseCase(UserDataStore repository,
                                   ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public Observable<User> buildUseCaseObservable() {
        return this.repository.getLoggedUser();
    }
}
