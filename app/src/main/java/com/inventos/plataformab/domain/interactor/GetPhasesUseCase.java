package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.PhaseRepository;
import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.Cycle;

import rx.Observable;

public class GetPhasesUseCase extends UseCase {

    private final PhaseDataStore phaseRepository;
    private final Cycle cycle;

    public GetPhasesUseCase(PhaseRepository repository, Cycle cycle,
                            ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.phaseRepository = repository;
        this.cycle = cycle;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return phaseRepository.phasesByCycle(cycle);
    }
}
