package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.MainRequestRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.MainRequest;

import rx.Observable;

public class MainRequestUseCase extends UseCase {

    private final MainRequestRepository repository;

    public MainRequestUseCase(MainRequestRepository repository,
                                 ThreadExecutor threadExecutor,
                                 PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public Observable<MainRequest> buildUseCaseObservable() {
        return this.repository.getMainRequest();
    }
}
