package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.notifications.NotificationDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;

import rx.Observable;

public class GetNotificationsUseCase extends UseCase {
    private NotificationDataStore notificationsRepository;
    public GetNotificationsUseCase(NotificationDataStore notificationsRepository,
                                   ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.notificationsRepository = notificationsRepository;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.notificationsRepository.notifications();
    }
}
