package com.inventos.plataformab.domain.interactor;


import com.inventos.plataformab.data.repository.StateRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.State;

import java.util.List;

import rx.Observable;

public class GetStatesUseCase extends UseCase {
    public final StateRepository repository;
    public GetStatesUseCase(StateRepository repository, ThreadExecutor threadExecutor,
                               PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public Observable<List<State>> buildUseCaseObservable() {
        return this.repository.getStates();
    }
}
