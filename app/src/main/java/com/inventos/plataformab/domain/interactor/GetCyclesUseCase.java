package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.CycleRepository;
import com.inventos.plataformab.data.repository.datasource.cycle.CycleDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;

import rx.Observable;


public class GetCyclesUseCase extends UseCase {

    private final CycleDataStore cycleDataStore;

    public GetCyclesUseCase(CycleRepository repository, ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.cycleDataStore = repository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.cycleDataStore.cycles();
    }
}
