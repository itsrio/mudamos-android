package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;

import rx.Observable;

public class RemoveLikeDislikeUseCase extends UseCase {

    private boolean like = false;

    private final CommentDataStore commentRepository;
    private int commentId;
    private int subjectId;

    public RemoveLikeDislikeUseCase(CommentDataStore commentRepository,
                                    ThreadExecutor threadExecutor,
                                    PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.commentRepository = commentRepository;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.commentRepository.removeLike(subjectId, commentId, like);
    }
}
