package com.inventos.plataformab.domain.interactor;

public class DefaultSubscriber<T> extends rx.Subscriber<T> {
    @Override public void onCompleted() {
        // no-op by defaultdb.
    }

    @Override public void onError(Throwable e) {
        // no-op by defaultdb.
    }

    @Override public void onNext(T t) {
        // no-op by defaultdb.
    }
}