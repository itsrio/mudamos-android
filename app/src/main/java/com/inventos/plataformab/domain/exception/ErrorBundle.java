package com.inventos.plataformab.domain.exception;

public interface ErrorBundle {
    String getErrorMessage();
}
