package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.CycleRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.Cycle;

import java.util.List;

import rx.Observable;

public class SaveCyclesUseCase extends UseCase {

    private final CycleRepository cycleDataStore;

    private List<Cycle> cycles;

    public SaveCyclesUseCase(CycleRepository repository, ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.cycleDataStore = repository;
    }

    public void setCycles(List<Cycle> cycleList) {
        this.cycles = cycleList;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.cycleDataStore.saveCycles(cycles);
    }
}
