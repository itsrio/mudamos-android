package com.inventos.plataformab.domain.exception;

import com.inventos.plataformab.presentation.model.BaseComponent;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.RetrofitError;

public class DefaultErrorBundle implements ErrorBundle {

    private static final String DEFAULT_ERROR_MSG = "Ocorreu algum erro";

    private String message;

    private static final HashMap<String, String> fieldName;
    static {
        fieldName = new HashMap<>();
        fieldName.put("email", "Email");
        fieldName.put("password", "Email");
        fieldName.put("password_confirmation", "Confirmação");
        fieldName.put("name", "Nome");
        fieldName.put("cpf", "CPF");
        fieldName.put("birthday", "Data de nascimento");
        fieldName.put("city", "Cidade");
        fieldName.put("alias_name", "Pseudônimo");
        fieldName.put("login", "");
    }

    public DefaultErrorBundle(String message) {
        this.message = message;
    }

    public DefaultErrorBundle(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            message = "Sem conexão";
        }
        else if (error.getResponse().getStatus() == 401) {
            message = "Não autorizado";
        }
        else if (error.getResponse().getStatus() == 422) {
            BaseComponent base = (BaseComponent) error.getBody();
            HashMap<String, ArrayList<String>> errors = base.getErrors();
            for (String errorKey : errors.keySet()) {
                this.message = fieldName.get(errorKey);
                this.message += " " + errors.get(errorKey).get(0);
                break;
            }
        }
        else if (error.getResponse().getStatus() == 404) {
            message = "Algum erro ocorreu";
        }
        else if (error.getResponse().getStatus() == 500) {
            message = "Erro do servidor";
        }
        else {
            BaseComponent base = (BaseComponent) error.getBody();
            HashMap<String, ArrayList<String>> errors = base.getErrors();
            for (String errorKey : errors.keySet()) {
                this.message = errors.get(errorKey).get(0);
                break;
            }
        }
    }

    @Override
    public String getErrorMessage() {
        return (this.message != null) ? this.message : DEFAULT_ERROR_MSG;
    }
}