package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class SaveUserUseCase extends UseCase {

    UserDataStore userRepository;

    User user;

    public SaveUserUseCase(UserDataStore userRepository, ThreadExecutor threadExecutor,
                          PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.userRepository.saveUser(user);
    }
}
