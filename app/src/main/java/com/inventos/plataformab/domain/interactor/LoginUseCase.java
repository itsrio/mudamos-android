package com.inventos.plataformab.domain.interactor;

import android.support.annotation.NonNull;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class LoginUseCase extends UseCase {

    public static final String TWITTER = "twitter";
    public static final String FACEBOOK = "facebook";

    private final UserDataStore userRepository;
    private String socialNetwork;

    private User user;

    public LoginUseCase(UserDataStore userRepository, ThreadExecutor threadExecutor,
                           PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    public void setUser(@NonNull User user) {
        this.user = user;
    }

    public void setSocialNetwork(String socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    @Override
    public Observable buildUseCaseObservable() {
        if (this.socialNetwork == null)
            return this.userRepository.authenticateUser(user);
        else return this.userRepository.authenticateUser(user, socialNetwork);
    }
}
