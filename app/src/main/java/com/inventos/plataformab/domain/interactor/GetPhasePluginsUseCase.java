package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.PhaseRepository;
import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.Phase;

import rx.Observable;

public class GetPhasePluginsUseCase extends UseCase {

    private final int phaseId;
    private final PhaseDataStore phaseRepository;

    public GetPhasePluginsUseCase(PhaseRepository repository, int phaseId,
                            ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.phaseRepository = repository;
        this.phaseId = phaseId;
    }

    @Override
    public Observable<Phase> buildUseCaseObservable() {
        return this.phaseRepository.phaseById(phaseId);
    }
}
