package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;

import rx.Observable;

public class LogoutUserUseCase extends UseCase {

    private final UserDataStore userRepository;

    public LogoutUserUseCase(UserDataStore userRepository, ThreadExecutor threadExecutor,
                          PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        this.userRepository.logout();
        return this.userRepository.removeUser();
    }
}
