package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.CommentRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;

import java.util.ArrayList;

import rx.Observable;

public class GetCommentsUseCase extends UseCase {

    private final CommentRepository commentRepository;
    private final int subjectId;
    private final int commentParent;
    private int pageNumber;

    public GetCommentsUseCase(CommentRepository repository, int subjectId,
                              int commentParent,
                              ThreadExecutor threadExecutor,
                            PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.commentRepository = repository;
        this.subjectId = subjectId;
        this.commentParent = commentParent;
        this.pageNumber = 1;
    }

    public void setPage(int page){
        this.pageNumber = page;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.commentRepository.getComments(subjectId, commentParent, pageNumber);
    }
}
