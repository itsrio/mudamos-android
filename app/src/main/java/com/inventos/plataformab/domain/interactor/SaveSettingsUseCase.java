package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.settings.SettingsDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.Settings;

import rx.Observable;

public class SaveSettingsUseCase extends UseCase {

    SettingsDataStore repository;
    Settings settings;

    public SaveSettingsUseCase(SettingsDataStore repository,
                                  ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.repository.save(this.settings);
    }
}
