package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class EditUserUseCase extends UseCase {

    private User user;

    private final UserDataStore repository;

    public EditUserUseCase(UserDataStore userRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = userRepository;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.repository.editUser(user);
    }
}
