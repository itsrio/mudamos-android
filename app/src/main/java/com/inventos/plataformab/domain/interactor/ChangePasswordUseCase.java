package com.inventos.plataformab.domain.interactor;

import com.inventos.plataformab.data.repository.UserDataRepository;
import com.inventos.plataformab.domain.executor.PostExecutionThread;
import com.inventos.plataformab.domain.executor.ThreadExecutor;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class ChangePasswordUseCase extends UseCase {

    private final UserDataRepository repository;
    private User user;

    public ChangePasswordUseCase(UserDataRepository repository,
                                 ThreadExecutor threadExecutor,
                                 PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.repository.editUser(user);
    }
}
