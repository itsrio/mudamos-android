package com.inventos.plataformab;

import android.app.Application;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.inventos.plataformab.data.cache.PreferencesHandler;
import com.inventos.plataformab.data.db.MudamosDb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import io.fabric.sdk.android.Fabric;

public class MudamosApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "cujhfu1ZIjLgxxrJ3RD5Xb15M";
    private static final String TWITTER_SECRET =
            "iWOjbqUG51TgZLGC7ZenY16pbpCgpuRcRTW2yDiksE3pwEElJ0";

    private static String AUTH_TOKEN = "";

    @Override public void onCreate() {
        super.onCreate();
//        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
//        Fabric.with(this, new Crashlytics()); //, new Twitter(authConfig));
        Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());
        FacebookSdk.sdkInitialize(this);
        initUser();
        checkFirstAppOpen();
    }

    private void checkFirstAppOpen() {
        String sdcard = getFilesDir().getPath();
        File fileList = new File(sdcard);
        // so we can list all files
        File[] filenames = fileList.listFiles();
        boolean firstTime = true;
        for (File tmpf : filenames) {
            if (tmpf.getName().contains("default")){
                firstTime = false;
                break;
            }
        }
        if (firstTime) {
            copyBundledRealmFile(this.getResources().openRawResource(R.raw.defaultdb), "default.realm");
        }
        MudamosDb.init(this, 1);
    }

    private String copyBundledRealmFile(InputStream inputStream, String outFileName) {
        try {
            File file = new File(this.getFilesDir(), outFileName);
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getAuthenticationToken() {
        if (AUTH_TOKEN == null) AUTH_TOKEN = "";
        return AUTH_TOKEN;
    }

    public static void setAuthenticationToken(String token) {
        AUTH_TOKEN = token;
    }

    private void initUser() {
        SharedPreferences preferences = getSharedPreferences(PreferencesHandler.APP_PREFERENCES, 0);
        AUTH_TOKEN = preferences.getString(PreferencesHandler.AUTH_TOKEN, "");
    }
}
