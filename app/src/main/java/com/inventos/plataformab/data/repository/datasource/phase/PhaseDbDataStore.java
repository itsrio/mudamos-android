package com.inventos.plataformab.data.repository.datasource.phase;

import com.inventos.plataformab.data.db.DbHandler;
import com.inventos.plataformab.data.db.DbHandlerImpl;
import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Phase;

import java.util.List;

import rx.Observable;
import rx.Subscriber;

public class PhaseDbDataStore implements PhaseDataStore {

    private final DbHandler db;

    public PhaseDbDataStore(DbHandlerImpl database) {
        this.db = database;
    }

    @Override
    public Observable<List<Phase>> phasesByCycle(final Cycle cycle) {
        return Observable.create(new Observable.OnSubscribe<List<Phase>>() {
            @Override
            public void call(Subscriber<? super List<Phase>> subscriber) {
                List<Phase> phases = db.phasesByCycle(cycle.getId());
                subscriber.onNext(phases);
            }
        });
    }

    @Override
    public Observable<Phase> phaseById(final int phaseId) {
        return Observable.create(new Observable.OnSubscribe<Phase>() {
            @Override
            public void call(Subscriber<? super Phase >subscriber) {
                Phase phase = db.getPhase(phaseId);
                subscriber.onNext(phase);
            }
        });
    }
}
