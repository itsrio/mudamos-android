package com.inventos.plataformab.data.db.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PluginRelations extends RealmObject {
    @PrimaryKey private int id;
    private int relatedId;
    private String relatedType;
    private RealmList<DiscussionPlugin> discussions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(int relatedId) {
        this.relatedId = relatedId;
    }

    public String getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(String relatedType) {
        this.relatedType = relatedType;
    }

    public RealmList<DiscussionPlugin> getDiscussions() {
        return discussions;
    }

    public void setDiscussions(RealmList<DiscussionPlugin> discussions) {
        this.discussions = discussions;
    }
}
