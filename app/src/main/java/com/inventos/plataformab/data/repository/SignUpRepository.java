package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.repository.datasource.signup.SignUpDataStore;
import com.inventos.plataformab.data.repository.datasource.signup.SignUpDataStoreFactory;
import com.inventos.plataformab.presentation.model.SignUpComponents;

import rx.Observable;

public class SignUpRepository implements SignUpDataStore {

    SignUpDataStore signUpDataStore;

    public SignUpRepository(SignUpDataStoreFactory factory) {
        this.signUpDataStore = factory.getCloudDataStore();
    }

    @Override
    public Observable<SignUpComponents> getComponents() {
        return signUpDataStore.getComponents();
    }
}
