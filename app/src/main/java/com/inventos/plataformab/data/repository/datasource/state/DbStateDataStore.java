package com.inventos.plataformab.data.repository.datasource.state;

import com.inventos.plataformab.data.db.DbHandler;
import com.inventos.plataformab.presentation.model.State;

import java.util.List;

import rx.Observable;
import rx.Subscriber;

public class DbStateDataStore implements StateDataStore {

    private final DbHandler db;

    public DbStateDataStore(DbHandler db) {
        this.db = db;
    }

    @Override
    public Observable<List<State>> getStates() {
        return Observable.create(new Observable.OnSubscribe<List<State>>() {
            @Override
            public void call(Subscriber<? super List<State>> subscriber) {
                List<State> states = db.allStates();
                subscriber.onNext(states);
                subscriber.onCompleted();
            }
        });
    }
}
