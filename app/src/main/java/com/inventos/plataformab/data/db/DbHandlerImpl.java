package com.inventos.plataformab.data.db;

import android.content.Context;

import com.inventos.plataformab.data.db.entities.EntityCycle;
import com.inventos.plataformab.data.db.entities.EntityPhase;
import com.inventos.plataformab.data.db.entities.EntitySettings;
import com.inventos.plataformab.data.db.entities.State;
import com.inventos.plataformab.data.db.mapper.CycleEntityMapper;
import com.inventos.plataformab.data.db.mapper.MapperFactory;
import com.inventos.plataformab.data.db.mapper.PhaseEntityMapper;
import com.inventos.plataformab.data.db.mapper.SettingsEntityMapper;
import com.inventos.plataformab.data.db.mapper.StateEntityMapper;
import com.inventos.plataformab.data.db.migrations.ReportMigration;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.Settings;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DbHandlerImpl implements DbHandler {

    private Context mContext;

    private long schemaVersion = 0;

    private RealmConfiguration realmConfiguration;

    public DbHandlerImpl(Context context, long version) {
        this.mContext = context;
        this.schemaVersion = version;
    }

    private Realm getRealmInstance() {
        if (realmConfiguration == null) {
            RealmConfiguration.Builder builder = new RealmConfiguration.Builder(this.mContext)
                    .schemaVersion(schemaVersion);
            builder.migration(new ReportMigration());
            realmConfiguration = builder.build();
        }
        return Realm.getInstance(realmConfiguration);
    }

    @Override
    public void saveCycle(Cycle cycle) {
        Realm realm = getRealmInstance();
        CycleEntityMapper mapper = MapperFactory.newCycleMapper();
        EntityCycle realmCycle = mapper.getRealmObject(cycle);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmCycle);
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCycles(List<Cycle> cycles) {
        int size = cycles.size();
        for (int i = 0; i < size; i++) {
            this.saveCycle(cycles.get(i));
        }
    }

    @Override
    public Cycle getCycle(int id) {
        Realm realm = getRealmInstance();
        CycleEntityMapper mapper = MapperFactory.newCycleMapper();
        Cycle cycle = mapper.getObject(realm.where(EntityCycle.class).equalTo("id", id).findFirst());
        realm.close();
        return cycle;
    }

    @Override
    public List<Cycle> allCycles() {
        Realm realm = getRealmInstance();
        CycleEntityMapper mapper = MapperFactory.newCycleMapper();
        List<EntityCycle> realmCycle = realm.where(EntityCycle.class).findAll();
        int size = realmCycle.size();
        List<Cycle> cycleObjList = new ArrayList<>(size);
        for (int i = 0 ; i < size; i++){
            cycleObjList.add(mapper.getObject(realmCycle.get(i)));
        }
        realm.close();
        return cycleObjList;
    }

    @Override
    public List<Phase> phasesByCycle(final int cycleId) {
        return this.getCycle(cycleId).getPhases();
    }

    @Override
    public void savePhase(Phase phase) {
        Realm realm = getRealmInstance();
        PhaseEntityMapper mapper = MapperFactory.newPhaseMapper();
        EntityPhase realmPhase = mapper.getRealmObject(phase);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmPhase);
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public Phase getPhase(int phaseId) {
        Realm realm = getRealmInstance();
        PhaseEntityMapper mapper = MapperFactory.newPhaseMapper();
        Phase phase = mapper.getObject(realm.where(EntityPhase.class).equalTo("id", phaseId).findFirst());
        realm.close();
        return phase;
    }


    @Override
    public List<Comment> getComments(int subjectId) {
        return null;
    }

    @Override
    public void saveSettings(Settings settings) {
        Realm realm = getRealmInstance();
        SettingsEntityMapper mapper = MapperFactory.newSettingsMapper();
        EntitySettings realmSettings = mapper.getRealmObject(settings);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmSettings);
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public Settings getSettings() {
        Realm realm = getRealmInstance();
        SettingsEntityMapper mapper = MapperFactory.newSettingsMapper();
        Settings settings = mapper.getObject(realm.where(EntitySettings.class).findFirst());
        realm.close();
        return settings;
    }

    @Override
    public MainRequest getMainRequest() {
        MainRequest mainRequest = new MainRequest();
        List<Cycle> cycles = this.allCycles();
        Settings settings = this.getSettings();
        mainRequest.setCycles(cycles);
        mainRequest.setSettings(settings);
        return mainRequest;
    }

    @Override
    public List<com.inventos.plataformab.presentation.model.State> allStates() {
        Realm realm = getRealmInstance();
        StateEntityMapper mapper = MapperFactory.newStateMapper();
        List<State> states = realm.where(State.class).findAll();
        int size = states.size();
        List<com.inventos.plataformab.presentation.model.State> statesObjects =
                new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            statesObjects.add(mapper.getObject(states.get(i)));
        }
        realm.close();
        return statesObjects;
    }

    @Override
    public void bumpSchema(long version) {
        schemaVersion = version;
    }
}
