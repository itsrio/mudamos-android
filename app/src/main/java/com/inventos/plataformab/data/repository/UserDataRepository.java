package com.inventos.plataformab.data.repository;

import android.content.Context;

import com.inventos.plataformab.data.cache.PreferencesDataStore;
import com.inventos.plataformab.data.repository.datasource.user.CloudUserDataStore;
import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.data.repository.datasource.user.UserDataStoreFactory;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class UserDataRepository implements UserDataStore {

    final CloudUserDataStore userCloudDataStore;
    final PreferencesDataStore userCacheDataStore;

    public UserDataRepository(UserDataStoreFactory userDataStoreFactory, Context context) {
        userCloudDataStore = userDataStoreFactory.createCloudDataStore();
        userCacheDataStore = userDataStoreFactory.createCacheRepository(context);
    }

    @Override
    public Observable<User> createUser(User user, String form) {
        return userCloudDataStore.createUser(user, form);
    }

    @Override
    public Observable<User> getUser() {
        return userCacheDataStore.getUser();
    }

    @Override
    public Observable<User> getLoggedUser() {
        return userCloudDataStore.getLoggedUser();
    }

    @Override
    public Observable saveUser(User user) {
        return userCacheDataStore.saveUser(user);
    }

    @Override
    public Observable<User> authenticateUser(User user) {
        return userCloudDataStore.authenticateUser(user);
    }

    @Override
    public Observable<User> authenticateUser(User user, String socialNetwork) {
        return this.userCloudDataStore.authenticateUser(user, socialNetwork);
    }

    @Override
    public Observable<User> editUser(User user) {
        return userCloudDataStore.editUser(user);
    }

    @Override
    public Observable<User> retrievePassword(User user) {
        return userCloudDataStore.retrievePassword(user);
    }

    @Override
    public void logout() {
        userCloudDataStore.logout();
    }

    @Override
    public Observable removeUser() {
        return userCacheDataStore.removeUser();
    }

}
