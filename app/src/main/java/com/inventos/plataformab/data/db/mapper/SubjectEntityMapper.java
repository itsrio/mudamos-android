package com.inventos.plataformab.data.db.mapper;

import com.inventos.plataformab.data.db.entities.EntitiySubject;
import com.inventos.plataformab.presentation.model.Subject;

public class SubjectEntityMapper {

    public Subject getObject(EntitiySubject subjectRealmObject) {
        Subject subject = new Subject();
        if (subjectRealmObject != null) {
            subject.setId(subjectRealmObject.getId());
            subject.setPluginRelationId(subjectRealmObject.getPluginRelationId());
            subject.setTitle(subjectRealmObject.getTitle());
            subject.setQuestion(subjectRealmObject.getQuestion());
            subject.setEnunciation(subjectRealmObject.getEnunciation());
        }
        return subject;
    }

    public EntitiySubject getRealmObject(Subject subjectObject) {
        EntitiySubject entitiySubject = new EntitiySubject();
        if (subjectObject != null) {
            entitiySubject.setId(subjectObject.getId());
            entitiySubject.setPluginRelationId(subjectObject.getPluginRelationId());
            entitiySubject.setTitle(subjectObject.getTitle());
            entitiySubject.setQuestion(subjectObject.getQuestion());
            entitiySubject.setEnunciation(subjectObject.getEnunciation());
        }
        return entitiySubject ;
    }

}
