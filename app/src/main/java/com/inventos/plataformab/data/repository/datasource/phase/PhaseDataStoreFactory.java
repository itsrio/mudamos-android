package com.inventos.plataformab.data.repository.datasource.phase;

import com.inventos.plataformab.data.db.MudamosDb;
import com.inventos.plataformab.data.repository.BaseRepositoryFactory;

public class PhaseDataStoreFactory extends BaseRepositoryFactory {
    public PhaseDataStore createCloudDataStore() {
            return new PhaseDbDataStore(MudamosDb.getDbInstance());
    }

    public PhaseDataStore createDbDataStore() {
        return new PhaseDbDataStore(MudamosDb.getDbInstance());
    }

}
