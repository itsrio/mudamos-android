package com.inventos.plataformab.data.db.mapper;

public class MapperFactory {
    public static CycleEntityMapper newCycleMapper() {
        SubjectEntityMapper subjectMapper = new SubjectEntityMapper();
        PhaseEntityMapper phaseEntityMapper = new PhaseEntityMapper(subjectMapper);
        return new CycleEntityMapper(phaseEntityMapper);

    }

    public static PhaseEntityMapper newPhaseMapper() {
        SubjectEntityMapper subjectMapper = new SubjectEntityMapper();
        return new PhaseEntityMapper(subjectMapper);
    }

    public static SubjectEntityMapper newSubjectMapper() {
        return new SubjectEntityMapper();
    }

    public static SettingsEntityMapper newSettingsMapper() {
        return new SettingsEntityMapper();
    }

    public static StateEntityMapper newStateMapper() { return new StateEntityMapper();}
}
