package com.inventos.plataformab.data.repository.datasource.notifications;

import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.Notifications;

import java.util.List;

import rx.Observable;

public class NotificationsCloudDataStore implements NotificationDataStore {

    MudamosApi api;

    public NotificationsCloudDataStore(MudamosApi api) {
        this.api = api;
    }

    @Override
    public Observable<List<Notifications>> notifications() {
        return this.api.getNotifications();
    }
}
