package com.inventos.plataformab.data.db;

import android.content.Context;
import android.content.SharedPreferences;

import com.inventos.plataformab.data.cache.PreferencesHandler;

public class MudamosDb {

    public static final String DB_VERSION_KEY = "db.version";

    public static Context appContext;
    private static DbHandlerImpl databaseInstance = null;
    private static int realmVersion;

    public static void init(Context context, int dbVersion) {
        appContext = context;
        realmVersion = dbVersion;
    }

    public static DbHandlerImpl getDbInstance() {
        if (databaseInstance == null){
            SharedPreferences preferences =
                    appContext.getSharedPreferences(PreferencesHandler.APP_PREFERENCES, 0);
            long version = preferences.getLong(DB_VERSION_KEY, realmVersion);
            databaseInstance = new DbHandlerImpl(appContext, version);
        }
        return databaseInstance;
    }

    public static void bumpSchema(long newVersion) {
        getDbInstance().bumpSchema(newVersion);
        SharedPreferences preferences =
                appContext.getSharedPreferences(PreferencesHandler.APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(DB_VERSION_KEY, newVersion);
        editor.apply();

    }
}
