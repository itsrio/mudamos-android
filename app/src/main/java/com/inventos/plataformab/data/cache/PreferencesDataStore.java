package com.inventos.plataformab.data.cache;

import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public class PreferencesDataStore implements UserDataStore {

    PreferencesHandler preferencesHandler;

    public PreferencesDataStore(PreferencesHandler preferencesHandler){
        this.preferencesHandler = preferencesHandler;
    }

    @Override
    public Observable<User> createUser(User user, String form) {
        return null;
    }

    @Override
    public Observable<User> getUser() {
        return preferencesHandler.getUser();
    }

    @Override
    public Observable<User> getLoggedUser() {
        return null;
    }

    @Override
    public void logout() {

    }

    @Override
    public Observable removeUser() {
        return preferencesHandler.removeUser();
    }

    @Override
    public Observable saveUser(final User user) {
        return preferencesHandler.saveUser(user);
    }

    @Override
    public Observable<User> authenticateUser(User user) {
        return null;
    }

    @Override
    public Observable<User> authenticateUser(User user, String socialNetwork) {
        return null;
    }

    @Override
    public Observable<User> editUser(User user) {
        return null;
    }

    @Override
    public Observable<User> retrievePassword(User user) {
        return null;
    }
}
