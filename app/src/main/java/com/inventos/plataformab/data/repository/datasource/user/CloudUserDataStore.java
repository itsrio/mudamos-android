package com.inventos.plataformab.data.repository.datasource.user;

import com.inventos.plataformab.MudamosApplication;
import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.User;

import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Observable;

public class CloudUserDataStore implements UserDataStore {

    private final MudamosApi restApi;

    public CloudUserDataStore(MudamosApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<User> createUser(User user, String form) {
        return this.restApi.createUser(form, user);
    }

    @Override
    public Observable<User> getUser() {
        return null;
    }

    @Override
    public Observable<User> getLoggedUser() {
        return this.restApi.getLoggerUser();
    }

    @Override
    public void logout() {
        this.restApi.logoutUser(new retrofit.Callback<Object>() {
            @Override
            public void success(Object o, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        MudamosApplication.setAuthenticationToken("");
    }

    @Override
    public Observable removeUser() {
        return null;
    }

    @Override
    public Observable saveUser(User user) {
        return null;
    }

    @Override
    public Observable<User> authenticateUser(User user) {
        return this.restApi.authenticateUser(user);
    }

    @Override
    public Observable<User> authenticateUser(User user, String socialNetwork) {
        return this.restApi.authenticateUser(socialNetwork, user);
    }

    @Override
    public Observable<User> editUser(User user) {
        return this.restApi.editUser(user);
    }

    @Override
    public Observable<User> retrievePassword(User user) {
        return this.restApi.recoverPassword(user);
    }
}
