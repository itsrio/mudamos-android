package com.inventos.plataformab.data.repository.datasource.comment;

import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Like;

import rx.Observable;

public interface CommentDataStore {
    Observable<CommentRequestWrapper> getComments(int subjectId, int parentId, int pageNumber);
    Observable<Comment> sendComment(int subjectId, int parentId, String comment, boolean isAnonymous);
    Observable<Like> sendLike(int subjectId, int commentId, boolean like);
    Observable removeLike(int subjectId, int commentId, boolean like);
}
