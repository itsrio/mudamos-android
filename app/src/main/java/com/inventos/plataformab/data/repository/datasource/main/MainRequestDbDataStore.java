package com.inventos.plataformab.data.repository.datasource.main;

import com.inventos.plataformab.data.db.DbHandlerImpl;
import com.inventos.plataformab.presentation.model.MainRequest;

import rx.Observable;
import rx.Subscriber;

public class MainRequestDbDataStore implements MainRequestDataStore {

    private final DbHandlerImpl db;

    public MainRequestDbDataStore(DbHandlerImpl db) {
        this.db = db;
    }


    @Override
    public Observable<MainRequest> getMainRequest() {
        return Observable.create(new Observable.OnSubscribe<MainRequest>() {
            @Override
            public void call(Subscriber<? super MainRequest> subscriber) {
                MainRequest mainRequest = db.getMainRequest();
                subscriber.onNext(mainRequest);
                subscriber.onCompleted();
            }

        });
    }
}
