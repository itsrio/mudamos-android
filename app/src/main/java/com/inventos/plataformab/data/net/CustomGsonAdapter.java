package com.inventos.plataformab.data.net;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Like;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Notifications;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.PluginRelation;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.model.Subject;
import com.inventos.plataformab.presentation.model.User;

public class CustomGsonAdapter {
    public static final String date_format = "yyyy-MM-dd'T'hh:mm:ss.sssZ";
    static List<Cycle> cycles;
    static Type cycleListType = new TypeToken<List<Cycle>>() {}.getType();
    static Type notificationsListType = new TypeToken<List<Notifications>>() {}.getType();

    static Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapter(cycleListType, new CustomGsonAdapter.CycleAdapter())
            .registerTypeAdapter(notificationsListType, new CustomGsonAdapter.NotificationListAdapter())
            .registerTypeAdapter(Notifications.class, new CustomGsonAdapter.NotificationAdapter())
            .setDateFormat(date_format)
            .create();
    public static class UserAdapter implements JsonSerializer<User>, JsonDeserializer<User> {
        public JsonElement serialize(User user, Type typeOfSrc,
                                     JsonSerializationContext context) {
            JsonElement je = CustomGsonAdapter.gson.toJsonTree(user);
            if (user.getId() == 0) {
                je.getAsJsonObject().remove("id");
            }
            if (user.getProfile() != null) {
                je.getAsJsonObject().addProperty("profile_id", user.getProfile().getId());
            }
            if (user.getSubProfile() != null) {
                je.getAsJsonObject().addProperty("sub_profile_id",
                        user.getSubProfile().getId());
            }
            if (user.getGender()!= null && ! user.getGender().isEmpty()) {
                je.getAsJsonObject().addProperty("gender",
                        Integer.parseInt(user.getGender()));
            }
            JsonObject jo = new JsonObject();
            jo.add("user", je);
            return jo;
        }

        @Override
        public User deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create();
            JsonObject userObj;
            if (json.getAsJsonObject().get("user") == null) {
                userObj = json.getAsJsonObject();
            }
            else {
                userObj = json.getAsJsonObject().get("user").getAsJsonObject();
            }
            User user = gson.fromJson(userObj, User.class);
            if (user != null && json.getAsJsonObject().has("auth_token")) {
                user.setAuthToken(json.getAsJsonObject().get("auth_token").getAsString());
            }
            return user;
        }
    }
    public static class CycleAdapter implements JsonDeserializer<List<Cycle>> {
        @Override
        public List<Cycle> deserialize(JsonElement json, Type typeOfT,
                                       JsonDeserializationContext context) throws JsonParseException {
            List<Cycle> cycles;
            JsonArray cyclesJsonArray = json.getAsJsonObject().get("cycles").getAsJsonArray();
            int arraySize = cyclesJsonArray.size();
            int currentPhaseId;
            JsonObject jsonCycle;
            Cycle tempCycle;
            cycles = new ArrayList<>(arraySize);
            for (int i = 0; i < arraySize; i++) {
                jsonCycle = cyclesJsonArray.get(i).getAsJsonObject();
                tempCycle = gson.fromJson(jsonCycle, Cycle.class);
                if (jsonCycle.get("phases") != null) {
                    currentPhaseId =
                            jsonCycle.get("current_phase").getAsJsonObject().get("id").getAsInt();
                    List<Phase> phases =
                            this.parsePhases(jsonCycle.get("phases").getAsJsonArray(), currentPhaseId);
                    tempCycle.setPhases(phases);
                }
                this.parsePluginRelations(jsonCycle, tempCycle);
                cycles.add(i, tempCycle);
            }
            return cycles;
        }

        private List<Phase> parsePhases(JsonArray phasesJsonArray, int currentPhaseId) {
            List<Phase> phases;
            if (phasesJsonArray == null) {
                 phases= new ArrayList<>();
            }
            else {
                int phasesArraySize = phasesJsonArray.size();
                phases = new ArrayList<>(phasesArraySize);
                Phase tempPhase;
                JsonObject tempJsonPhase;
                for (int i = 0; i < phasesArraySize; i++) {
                    tempJsonPhase = phasesJsonArray.get(i).getAsJsonObject();
                    tempPhase = CustomGsonAdapter.gson.fromJson(tempJsonPhase, Phase.class);
                    if (tempPhase.getId() == currentPhaseId){
                        tempPhase.setCurrent(true);
                    }
                    phases.add(i, tempPhase);
                }
            }
            return phases;
        }

        private void parsePluginRelations(JsonObject jsonCycle, Cycle cycle) {
            JsonArray jsonPluginRelationArray = jsonCycle.getAsJsonArray("plugin_relations");
            JsonArray jsonSubjectArray = jsonCycle.getAsJsonArray("subjects");
            //JsonArray jsonBlogPostsArray = jsonCycle.getAsJsonArray("plugin_relations");
            associateSubjects(cycle, jsonSubjectArray, jsonPluginRelationArray);
        }

        private void associateBlogPosts(Cycle cycle, JsonArray blogArray,
                                        JsonArray pluginRelations) {
            int blogPostArraySize = blogArray.size();
            for (int i = 0; i < blogPostArraySize; i++) {

            }

        }

        private void associateSubjects(Cycle cycle, JsonArray subjectArray,
                                       JsonArray pluginRelations) {
            int pluginRelationSize = pluginRelations.size();
            PluginRelation pluginRelation;
            int subjectArraySize = subjectArray.size();
            Subject subject;
            Phase tempPhase;

            for (int i = 0; i < pluginRelationSize; i++) {
                pluginRelation = new PluginRelation(pluginRelations.get(i).getAsJsonObject());
                tempPhase = cycle.getPhaseById(pluginRelation.getRelatedId());
                if (tempPhase != null && pluginRelation.getRelatedId() == tempPhase.getId() && pluginRelation.getRelatedType().equals("Phase")) {
                    tempPhase.setType(pluginRelations.get(i).getAsJsonObject().get("slug").getAsString());
                }
                for (int j = 0; j < subjectArraySize; j++) {
                    subject = new Subject(subjectArray.get(j).getAsJsonObject());
                    if (subject.getPluginRelationId() == pluginRelation.getId()) {
                        if (pluginRelation.getRelatedType().equals("Phase")) {
                            tempPhase.getSubjects().add(subject);
                        }
                        else {
                            cycle.getSubjects().add(subject);
                        }
                    }
                }
            }
        }
    }
    public static class CommentRequestWrapperAdapter implements
            JsonDeserializer<CommentRequestWrapper> {

        @Override
        public CommentRequestWrapper deserialize(JsonElement json, Type typeOfT,
                                         JsonDeserializationContext context)
                throws JsonParseException {
            CommentRequestWrapper wrapper = new CommentRequestWrapper();
            if (json.getAsJsonObject().has("comments")){
                List<Comment> comments;
                JsonArray commentsJsonArray = json.getAsJsonObject().get("comments")
                        .getAsJsonArray();
                int arraySize = commentsJsonArray.size();
                JsonObject jsonComment;
                Comment tempComment;
                comments = new ArrayList<>(arraySize);
                for (int i = 0; i < arraySize; i++) {
                    jsonComment = commentsJsonArray.get(i).getAsJsonObject();
                    tempComment = CustomGsonAdapter.gson.fromJson(jsonComment, Comment.class);
                    comments.add(tempComment);
                }
                wrapper.setComments(comments);

                if (json.getAsJsonObject().has("subject_user") &&
                        !json.getAsJsonObject().get("subject_user").isJsonNull()) {
                    wrapper.setHasCommentConfig(true);
                    JsonObject subjectUser = json.getAsJsonObject().get("subject_user")
                            .getAsJsonObject();
                    if (subjectUser == null) {
                        wrapper.setHasAnswered(false);
                    }
                    else {
                        wrapper.setHasAnswered(true);
                        if (subjectUser.has("is_anonymous") && !subjectUser.get("is_anonymous").isJsonNull()){
                            wrapper.setIsAnonymous(
                                    subjectUser.get("is_anonymous").getAsBoolean());
                        }
                        else wrapper.setIsAnonymous(false);
                    }
                }
                else {
                    wrapper.setHasCommentConfig(false);
                }
            }
            return wrapper;
        }
    }

    public static class CommentAdapter implements JsonSerializer<Comment>, JsonDeserializer<Comment> {

        @Override
        public JsonElement serialize(Comment comment, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject container = new JsonObject();
            JsonObject jsonComment = new JsonObject();
            jsonComment.addProperty("content", comment.getContent());
            jsonComment.addProperty("is_anonymous", comment.isAnonymous());
            container.add("comment", jsonComment);
            return container;
        }

        @Override
        public Comment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonComment = json.getAsJsonObject().get("comment").getAsJsonObject();

            return CustomGsonAdapter.gson.fromJson(jsonComment, Comment.class);
        }
    }

    public static class MainRequestAdapter implements JsonDeserializer<MainRequest> {

        @Override
        public MainRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            MainRequest mainRequest = new MainRequest();
            JsonObject jsonObject = json.getAsJsonObject();
            mainRequest.setSettings(parseAboutValues(jsonObject));
            mainRequest.setCycles(parseCycleList(jsonObject));
            return mainRequest;
        }
        private Settings parseAboutValues(JsonObject jsonObject) {
            Settings settings = new Settings();
            String key;
            String title = "title";
            String text = "text";
            String value;
            String content;
            for (Map.Entry<String,JsonElement> entry : jsonObject.entrySet()) {
                key = entry.getKey();

                if (key.contains(title) && ! key.equals("home_sub_title")) {
                    value = entry.getValue().getAsString();
                    content = jsonObject.get(key.replace(title, text)).getAsString();
                    content = fixLinks(content);
                    settings.getTitles().add(value);
                    settings.getContent().add(content);
                }
                else if (key.equals("home_header")) {
                    value = entry.getValue().getAsString();
                    settings.setHeader(value);
                }
                else if (key.equals("logo_png")) {
                    value = entry.getValue().getAsString();
                    settings.setLogo(value);
                }
                else if (key.equals("home_sub_title")) {
                    value = entry.getValue().getAsString();
                    settings.setSubtitle(value);
                }
            }

            return settings;

        }

        private String fixLinks(String text) {
            String fixedString;
            fixedString = text.replaceAll("href=\"/", "href=\""+ RestClient.HOME_URL);
            return fixedString;
        }

        private List<Cycle> parseCycleList(JsonElement element) {
            cycles = CustomGsonAdapter.gson.fromJson(element, cycleListType);
            return cycles;

        }
    }

    public static class LikeAdapter implements JsonDeserializer<Like>{
        @Override
        public Like deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonLikeRoot = json.getAsJsonObject();
            JsonObject jsonLike;
            Like like = new Like();
            if (jsonLikeRoot.has("like")) {
                like.setLike(true);
                jsonLike = jsonLikeRoot.getAsJsonObject("like");
            }
            else if (jsonLikeRoot.has("dislike")) {
                like.setLike(false);
                jsonLike = jsonLikeRoot.getAsJsonObject("dislike");
            }
            else {
                jsonLike = jsonLikeRoot;
            }

            like.setCommentId(jsonLike.get("comment_id").getAsInt());
            like.setUserId(jsonLike.get("user_id").getAsInt());
            like.setId(jsonLike.get("id").getAsInt());

            return like;
        }
    }

    public static class NotificationAdapter implements JsonDeserializer<Notifications> {

        @Override
        public Notifications deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonNotification = json.getAsJsonObject();
            Notifications notification = new Notifications();
            notification.setType(jsonNotification.get("target_object_type").getAsString());
            notification.setPictureUrl(jsonNotification.get("picture_url").getAsString());
            notification.setTitle(jsonNotification.get("title").getAsString());
            DateFormat df = new SimpleDateFormat(date_format);
            try {
                notification.setCreatedAt(
                        df.parse(jsonNotification.get("created_at").getAsString()));
            }
            catch (ParseException e) {
                e.printStackTrace();
            }

            JsonObject targetObject = jsonNotification.get("target_object").getAsJsonObject();
            if (notification.getType().equals("Comment")) {
                notification.setCommentId(targetObject.get("id").getAsInt());
                notification.setSubjectId(targetObject.get("subject_id").getAsInt());
                if (targetObject.has("parent_id") && ! targetObject.get("parent_id").isJsonNull()) {
                    notification.setParentId(targetObject.get("parent_id").getAsInt());
                }
            }
            else {
                // Like or dislike
                notification.setCommentId(targetObject.get("comment_id").getAsInt());
                notification.setSubjectId(targetObject.get("comment_subject_id").getAsInt());
                if (targetObject.has("comment_parent_id") && ! targetObject.get("comment_parent_id").isJsonNull()) {
                    notification.setParentId(targetObject.get("comment_parent_id").getAsInt());
                }
            }

            return notification;
        }
    }

    public static class NotificationListAdapter implements JsonDeserializer<List<Notifications>> {

        @Override
        public List<Notifications> deserialize(JsonElement json, Type typeOfT,
                                               JsonDeserializationContext context) throws JsonParseException {
            JsonArray jsonArray = json.getAsJsonObject()
                    .get("notifications").getAsJsonArray();
            List<Notifications> notifications = new ArrayList<>(jsonArray.size());
            Notifications notification;
            for (JsonElement jsonElement: jsonArray) {
                notification = gson.fromJson(jsonElement.getAsJsonObject(), Notifications.class);
                notifications.add(notification);
            }
            return notifications;
        }
    }
}
