package com.inventos.plataformab.data.repository.datasource.settings;

import com.inventos.plataformab.data.db.MudamosDb;

public class SettingsDataStoreFactory {
    public static SettingsDataStore createDbDataStore() {
        return new DbSettingsDataStore(MudamosDb.getDbInstance());
    }
}
