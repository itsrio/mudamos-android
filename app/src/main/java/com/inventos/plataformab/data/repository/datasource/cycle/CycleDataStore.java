package com.inventos.plataformab.data.repository.datasource.cycle;

import com.inventos.plataformab.presentation.model.Cycle;

import java.util.List;

import rx.Observable;

public interface CycleDataStore {
    Observable<List<Cycle>> cycles();

    Observable saveCycles(List<Cycle> cycles);
}
