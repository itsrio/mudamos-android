package com.inventos.plataformab.data.db.mapper;

import com.inventos.plataformab.data.db.entities.EntitySettings;
import com.inventos.plataformab.data.db.entities.RealmString;
import com.inventos.plataformab.presentation.model.Settings;

import io.realm.RealmList;

public class SettingsEntityMapper {

    public Settings getObject(EntitySettings settingsRealObject) {
        Settings settings = new Settings();
        if (settingsRealObject != null) {
            settings.setHeader(settingsRealObject.getHeader());
            settings.setLogo(settingsRealObject.getLogo());
            int titleSize = settingsRealObject.getTitles().size();
            String tempString;
            for (int i = 0; i < titleSize; i++) {
                tempString = settingsRealObject.getTitles().get(i).getString();
                settings.getTitles().add(tempString);
                tempString = settingsRealObject.getContent().get(i).getString();
                settings.getContent().add(tempString);
            }
        }
        return settings;
    }

    public EntitySettings getRealmObject(Settings settings) {
        EntitySettings entitySettings = new EntitySettings();
        if (settings != null) {
            entitySettings.setLogo(settings.getLogo());
            entitySettings.setHeader(settings.getHeader());
            int titleSize = settings.getTitles().size();

            entitySettings.setTitles(new RealmList<RealmString>());
            entitySettings.setContent(new RealmList<RealmString>());
            RealmString titleString;
            RealmString contentString;
            for (int i = 0; i < titleSize; i++) {
                titleString = new RealmString();
                titleString.setString(settings.getTitles().get(i));
                entitySettings.getTitles().add(titleString);
                 contentString = new RealmString();
                contentString.setString(settings.getContent().get(i));
                entitySettings.getContent().add(contentString);
            }
        }

        return entitySettings;
    }

}
