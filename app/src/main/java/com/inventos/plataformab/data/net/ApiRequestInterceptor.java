package com.inventos.plataformab.data.net;

import com.inventos.plataformab.MudamosApplication;

import retrofit.RequestInterceptor;

public class ApiRequestInterceptor implements RequestInterceptor {
    public static final String PLATFORM_HEADER = "X-PLATFORM" ;
    public static final String AUTH_TOKEN = "Authorization";

    @Override
    public void intercept(RequestFacade request) {
        request.addHeader(PLATFORM_HEADER, "android");
        request.addHeader(AUTH_TOKEN, "Token token=" + MudamosApplication.getAuthenticationToken());
        request.addHeader("Content-Type", "application/json");
    }
}
