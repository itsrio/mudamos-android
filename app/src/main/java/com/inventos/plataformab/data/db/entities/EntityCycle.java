package com.inventos.plataformab.data.db.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class EntityCycle extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String title;
    private String about;
    private String initialDate;
    private String finalDate;
    private String picture;
    private String color;
    private String compilationPdf;
    private String description;
    private RealmList<EntityPhase> entityPhases;

    public EntityCycle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(String initialDate) {
        this.initialDate = initialDate;
    }

    public String getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(String finalDate) {
        this.finalDate = finalDate;
    }

    public RealmList<EntityPhase> getEntityPhases() {
        return entityPhases;
    }

    public void setEntityPhases(RealmList<EntityPhase> entityPhases) {
        this.entityPhases = entityPhases;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompilationPdf() {
        return compilationPdf;
    }

    public void setCompilationPdf(String compilationPdf) {
        this.compilationPdf = compilationPdf;
    }
}
