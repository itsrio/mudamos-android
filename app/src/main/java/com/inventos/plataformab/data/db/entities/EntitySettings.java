package com.inventos.plataformab.data.db.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class EntitySettings  extends RealmObject {
    @PrimaryKey
    private int id;
    private String logo;
    private String header;
    private String subtitle;
    private RealmList<RealmString> titles;
    private RealmList<RealmString> content;

    public EntitySettings() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public RealmList<RealmString> getTitles() {
        return titles;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setTitles(RealmList<RealmString> titles) {
        this.titles = titles;
    }

    public RealmList<RealmString> getContent() {
        return content;
    }

    public void setContent(RealmList<RealmString> content) {
        this.content = content;
    }
}
