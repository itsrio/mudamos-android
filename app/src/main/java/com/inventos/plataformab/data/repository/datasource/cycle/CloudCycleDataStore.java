package com.inventos.plataformab.data.repository.datasource.cycle;

import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.Cycle;

import java.util.List;

import rx.Observable;

public class CloudCycleDataStore implements CycleDataStore {

    private final MudamosApi restApi;

    public CloudCycleDataStore(MudamosApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<List<Cycle>> cycles() {
        return this.restApi.getCycles();
    }

    @Override
    public Observable saveCycles(List<Cycle> cycles) {
        return null;
    }
}
