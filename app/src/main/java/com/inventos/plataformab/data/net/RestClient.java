package com.inventos.plataformab.data.net;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Like;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Notifications;
import com.inventos.plataformab.presentation.model.User;

import java.lang.reflect.Type;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class RestClient {

//    public static final String HOME_URL = "https://www.mudamos.org/";
    public static final String HOME_URL = "http://testes.mudamos.org/";
    private static final String API_URL = HOME_URL + "api/v1";

    static Type notificationsListType = new TypeToken<List<Notifications>>() {}.getType();

    private static MudamosApi apiInstance = null;

    public static MudamosApi getApiInstance() {
        if (apiInstance == null) {
            Gson gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .registerTypeAdapter(User.class, new CustomGsonAdapter.UserAdapter())
                    .registerTypeAdapter(CommentRequestWrapper.class, new CustomGsonAdapter.CommentRequestWrapperAdapter())
                    .registerTypeAdapter(Comment.class, new CustomGsonAdapter.CommentAdapter())
                    .registerTypeAdapter(notificationsListType, new CustomGsonAdapter.NotificationListAdapter())
                    .registerTypeAdapter(MainRequest.class, new CustomGsonAdapter.MainRequestAdapter())
                    .registerTypeAdapter(Like.class, new CustomGsonAdapter.LikeAdapter())
                    .create();

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(API_URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL) //To better analyze the req and res
                    .setRequestInterceptor(new ApiRequestInterceptor())
                    .setConverter(new GsonConverter(gson))
                    .build();
            apiInstance = restAdapter.create(MudamosApi.class);
        }

        return apiInstance;
    }

}
