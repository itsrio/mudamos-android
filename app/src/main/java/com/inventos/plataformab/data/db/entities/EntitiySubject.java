package com.inventos.plataformab.data.db.entities;

import io.realm.RealmObject;

public class EntitiySubject extends RealmObject{
    private int id;
    private int pluginRelationId;
    private String title;
    private String question;
    private String enunciation;

    public EntitiySubject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPluginRelationId() {
        return pluginRelationId;
    }

    public void setPluginRelationId(int pluginRelationId) {
        this.pluginRelationId = pluginRelationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getEnunciation() {
        return enunciation;
    }

    public void setEnunciation(String enunciation) {
        this.enunciation = enunciation;
    }
}
