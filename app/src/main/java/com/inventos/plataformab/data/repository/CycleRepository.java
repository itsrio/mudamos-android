package com.inventos.plataformab.data.repository;

import android.content.Context;

import com.inventos.plataformab.data.repository.datasource.cycle.CycleDataStore;
import com.inventos.plataformab.data.repository.datasource.cycle.CycleDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.cycle.DbCycleDataStore;
import com.inventos.plataformab.presentation.model.Cycle;

import java.util.List;

import rx.Observable;

public class CycleRepository extends BaseRepository implements CycleDataStore {

    private CycleDataStore cycleDataStore;
    private CycleDataStoreFactory cycleDataStoreFactory;

    public CycleRepository(CycleDataStoreFactory cycleDataStoreFactory, Context context) {
        this.context = context;
        this.cycleDataStoreFactory = cycleDataStoreFactory;
        if (isOnline()){
            cycleDataStore = cycleDataStoreFactory.createCloudDataStore();
        }
        else {
            cycleDataStore = cycleDataStoreFactory.createDbDataStore();
        }
    }

    @Override
    public Observable<List<Cycle>> cycles() {
        return cycleDataStore.cycles();
    }

    @Override
    public Observable saveCycles(List<Cycle> cycles) {
        if (! (cycleDataStore instanceof DbCycleDataStore)){
            cycleDataStore = cycleDataStoreFactory.createDbDataStore();
        }
        return cycleDataStore.saveCycles(cycles);
    }
}
