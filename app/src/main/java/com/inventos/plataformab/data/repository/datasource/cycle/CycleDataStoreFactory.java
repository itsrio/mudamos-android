package com.inventos.plataformab.data.repository.datasource.cycle;

import com.inventos.plataformab.data.db.DbHandlerImpl;
import com.inventos.plataformab.data.db.MudamosDb;
import com.inventos.plataformab.data.repository.BaseRepositoryFactory;

public class CycleDataStoreFactory extends BaseRepositoryFactory {
    public CycleDataStore createCloudDataStore() {
        return new CloudCycleDataStore(api);
    }
    public CycleDataStore createDbDataStore() {
        DbHandlerImpl database = MudamosDb.getDbInstance();
        return new DbCycleDataStore(database);
    }
}
