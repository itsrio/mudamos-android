package com.inventos.plataformab.data.repository.datasource.main;

import com.inventos.plataformab.data.db.DbHandler;
import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.MainRequest;

import rx.Observable;
import rx.functions.Action1;

public class MainRequestCloudDataStore implements MainRequestDataStore {

    MudamosApi api;
    DbHandler db;

    public MainRequestCloudDataStore(MudamosApi api, DbHandler db){
        this.api = api;
        this.db = db;
    }

    @Override
    public Observable<MainRequest> getMainRequest() {
        return this.api.getMainRequest().doOnNext(new Action1<MainRequest>() {
            @Override
            public void call(MainRequest mainRequest) {
                db.saveSettings(mainRequest.getSettings());
                db.saveCycles(mainRequest.getCycles());
            }
        });
    }
}
