package com.inventos.plataformab.data.repository.datasource.main;

import com.inventos.plataformab.data.db.DbHandler;
import com.inventos.plataformab.data.db.MudamosDb;
import com.inventos.plataformab.data.net.RestClient;

public class MainRequestDataStoreFactory {
    public MainRequestDataStore createCloudDataStore() {
        return new MainRequestCloudDataStore(RestClient.getApiInstance(), MudamosDb.getDbInstance());
    }

    public MainRequestDataStore createDbDataStore() {
        return new MainRequestDbDataStore(MudamosDb.getDbInstance());
    }
}
