package com.inventos.plataformab.data.repository.datasource.user;

import android.content.Context;

import com.inventos.plataformab.data.cache.PreferencesDataStore;
import com.inventos.plataformab.data.cache.PreferencesHandlerImpl;
import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.data.net.RestClient;

public class UserDataStoreFactory {

    public CloudUserDataStore createCloudDataStore() {
        MudamosApi api = RestClient.getApiInstance();
        return new CloudUserDataStore(api);
    }

    public PreferencesDataStore createCacheRepository(Context context) {
        PreferencesHandlerImpl preferencesHandler = new PreferencesHandlerImpl(context);
        return new PreferencesDataStore(preferencesHandler);
    }
}
