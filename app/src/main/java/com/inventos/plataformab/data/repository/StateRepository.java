package com.inventos.plataformab.data.repository;


import com.inventos.plataformab.data.repository.datasource.state.StateDataStore;
import com.inventos.plataformab.data.repository.datasource.state.StateDataStoreFactory;
import com.inventos.plataformab.presentation.model.State;

import java.util.List;

import rx.Observable;

public class StateRepository implements StateDataStore {

    StateDataStore stateDataStore;

    public StateRepository(StateDataStoreFactory stateDataStoreFactory) {
        stateDataStore = stateDataStoreFactory.createDbDataStore();
    }

    @Override
    public Observable<List<State>> getStates() {
        return this.stateDataStore.getStates();
    }
}
