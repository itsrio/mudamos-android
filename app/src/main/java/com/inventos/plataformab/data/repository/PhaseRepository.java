package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStore;
import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStoreFactory;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Phase;

import java.util.List;

import rx.Observable;

public class PhaseRepository implements PhaseDataStore {

    PhaseDataStore phaseDataStore;

    public PhaseRepository(PhaseDataStoreFactory phaseDataStoreFactory) {
        phaseDataStore = phaseDataStoreFactory.createDbDataStore();
    }

    @Override
    public Observable<List<Phase>> phasesByCycle(Cycle cycle) {
        return phaseDataStore.phasesByCycle(cycle);
    }

    @Override
    public Observable<Phase> phaseById(int phaseId) {
        return phaseDataStore.phaseById(phaseId);
    }
}
