package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.repository.datasource.notifications.NotificationDataStore;
import com.inventos.plataformab.data.repository.datasource.notifications.NotificationsDataStoreFactory;
import com.inventos.plataformab.presentation.model.Notifications;

import java.util.List;

import rx.Observable;

public class NotificationsRepository implements NotificationDataStore {

    private NotificationDataStore notificationDataStore;

    public NotificationsRepository(NotificationsDataStoreFactory factory) {
        this.notificationDataStore = factory.createCloudDataStore();
    }

    @Override
    public Observable<List<Notifications>> notifications() {
        return this.notificationDataStore.notifications();
    }
}
