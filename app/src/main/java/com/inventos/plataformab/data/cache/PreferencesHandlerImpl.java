package com.inventos.plataformab.data.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.inventos.plataformab.presentation.model.User;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.SessionManager;

import rx.Observable;
import rx.Subscriber;

public class PreferencesHandlerImpl implements PreferencesHandler {

    Context context;

    Gson gson;
    SharedPreferences preferences;

    public PreferencesHandlerImpl(Context context) {
        this.context = context;
        this.gson = new Gson();
        this.preferences = context.getSharedPreferences(APP_PREFERENCES, 0);
    }

    @Override
    public Observable<User> getUser() {
        return Observable.create(new Observable.OnSubscribe<User>() {
            @Override
            public void call(Subscriber<? super User> subscriber) {
                String userString = preferences.getString(USER, null);
                User user = null;
                if (userString != null) {
                    user = gson.fromJson(userString, User.class);
                }
                subscriber.onNext(user);
            }
        });
    }

    @Override
    public Observable saveUser(final User user) {
        return Observable.create(new Observable.OnSubscribe() {
            @Override
            public void call(Object o) {
                Subscriber subscriber = (Subscriber) o;
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString( USER, gson.toJson(user));
                editor.putString(AUTH_TOKEN, user.getAuthToken());
                editor.apply();
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Observable removeUser() {

        return Observable.create(new Observable.OnSubscribe() {
            @Override
            public void call(Object o) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.remove(PreferencesHandler.USER);
                editor.apply();

                logoutFacebook();
                logoutTwitter();
            }
        });
    }

    private void logoutTwitter() {
        CookieSyncManager.createInstance(context);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        SessionManager session = Twitter.getSessionManager();
        if (session != null) {
            session.clearActiveSession();
            Twitter.logOut();
        }
    }

    private void logoutFacebook() {
        //Facebook Logout
        LoginManager manager = LoginManager.getInstance();
        if (manager != null) manager.logOut();
    }
}
