package com.inventos.plataformab.data.repository.datasource.state;

import com.inventos.plataformab.presentation.model.State;

import java.util.List;

import rx.Observable;

public interface StateDataStore {
    Observable<List<State>> getStates();
}
