package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.repository.datasource.settings.SettingsDataStore;
import com.inventos.plataformab.data.repository.datasource.settings.SettingsDataStoreFactory;
import com.inventos.plataformab.presentation.model.Settings;

import rx.Observable;

public class SettingsRepository implements SettingsDataStore {

    private final SettingsDataStore settingsDataStore;

    public SettingsRepository(SettingsDataStoreFactory factory) {
        this.settingsDataStore = factory.createDbDataStore();

    }

    @Override
    public Observable save(Settings settings) {
        return this.settingsDataStore.save(settings);
    }

    @Override
    public Observable<Settings> settings() {
        return this.settingsDataStore.settings();
    }
}
