package com.inventos.plataformab.data.repository.datasource.notifications;

import com.inventos.plataformab.presentation.model.Notifications;

import java.util.List;

import rx.Observable;

public interface NotificationDataStore {
    Observable<List<Notifications>> notifications();
}
