package com.inventos.plataformab.data.repository.datasource.notifications;

import com.inventos.plataformab.data.net.RestClient;

public class NotificationsDataStoreFactory {
    public NotificationsCloudDataStore createCloudDataStore() {
        return new NotificationsCloudDataStore(RestClient.getApiInstance());
    }
}
