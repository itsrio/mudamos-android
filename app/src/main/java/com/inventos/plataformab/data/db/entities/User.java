package com.inventos.plataformab.data.db.entities;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private Profile profile;
    private String email;
    private String password;
    private String authToken;
    private String uid;
    @Ignore
    private String password_confirmation;


    public User() {}

    public User(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public User(String name, String password, String email, String passwordConfirmation) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.password_confirmation = passwordConfirmation;
    }

    public static User newUser(String authToken, String uId) {
        User newUser = new User();
        newUser.setAuthToken(authToken);
        newUser.setUid(uId);
        return newUser;
    }

    public static User newUserFromLogin(String email, String password) {
        User newUser = new User();
        newUser.setEmail(email);
        newUser.setPassword(password);
        return newUser;
    }

    public static User newUser(String authToken, long uId) {
        User newUser = new User();
        newUser.setAuthToken(authToken);
        newUser.setUid("" + uId);
        return newUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
