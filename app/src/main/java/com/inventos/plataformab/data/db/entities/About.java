package com.inventos.plataformab.data.db.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class About extends RealmObject {

    @PrimaryKey
    private int id;
    private String title;
    private String description;

    public About() {}

    public About(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
