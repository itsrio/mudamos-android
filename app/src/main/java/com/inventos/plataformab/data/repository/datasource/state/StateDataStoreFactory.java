package com.inventos.plataformab.data.repository.datasource.state;

import com.inventos.plataformab.data.db.MudamosDb;

public class StateDataStoreFactory {
    public StateDataStore createDbDataStore() {
        return new DbStateDataStore(MudamosDb.getDbInstance());
    }

}
