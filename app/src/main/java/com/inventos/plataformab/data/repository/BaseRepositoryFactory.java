package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.data.net.RestClient;

public class BaseRepositoryFactory {
    protected final MudamosApi api = RestClient.getApiInstance();
}
