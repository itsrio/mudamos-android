package com.inventos.plataformab.data.repository.datasource.cycle;

import com.inventos.plataformab.data.db.DbHandler;
import com.inventos.plataformab.presentation.model.Cycle;

import java.util.List;

import rx.Observable;
import rx.Subscriber;

public class DbCycleDataStore implements CycleDataStore {

    private final DbHandler db;

    public DbCycleDataStore(DbHandler db) {
        this.db = db;
    }

    @Override
    public Observable<List<Cycle>> cycles() {
        return Observable.create(new Observable.OnSubscribe<List<Cycle>>() {
            @Override
            public void call(Subscriber<? super List<Cycle>> subscriber) {
                List<Cycle> cycles = db.allCycles();
                subscriber.onNext(cycles);
                subscriber.onCompleted();
            }

        });
    }

    @Override
    public Observable saveCycles(final List<Cycle> cycles) {
        return Observable.create(new Observable.OnSubscribe() {
            @Override
            public void call(Object o) {
                db.saveCycles(cycles);
            }
        });
    }
}
