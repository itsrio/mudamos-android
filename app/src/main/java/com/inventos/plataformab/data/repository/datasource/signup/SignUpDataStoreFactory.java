package com.inventos.plataformab.data.repository.datasource.signup;

import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.data.net.RestClient;

public class SignUpDataStoreFactory {
    public SignUpDataStore getCloudDataStore() {
        MudamosApi api = RestClient.getApiInstance();
        return new CloudSignUpDataStore(api);
    }

}
