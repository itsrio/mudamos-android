package com.inventos.plataformab.data.db.migrations;

import com.inventos.plataformab.data.db.MudamosDb;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

public class ReportMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        if (oldVersion == 0) {
            schema.get("EntityCycle").addField("compilationPdf", String.class);
            schema.get("EntityPhase").addField("type", String.class);
            oldVersion++;
            MudamosDb.bumpSchema(oldVersion);
        }
    }
}
