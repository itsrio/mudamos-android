package com.inventos.plataformab.data.repository.datasource.user;

import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public interface UserDataStore {
    Observable<User> createUser(User user, String form);
    Observable<User> getUser();
    Observable<User> getLoggedUser();
    void logout();
    Observable removeUser();
    Observable saveUser(User user);
    Observable<User> authenticateUser(User user);
    Observable<User> authenticateUser(User user, String socialNetwork);
    Observable<User> editUser(User user);
    Observable<User> retrievePassword(User user);
}
