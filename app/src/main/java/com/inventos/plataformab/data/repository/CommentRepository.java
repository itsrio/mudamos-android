package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStore;
import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStoreFactory;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Like;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class CommentRepository implements CommentDataStore {

    CommentDataStore commentDataStore;

    public CommentRepository(CommentDataStoreFactory commentDataStoreFactory) {
        commentDataStore = commentDataStoreFactory.createCloudDataStore();
    }

    @Override
    public Observable<CommentRequestWrapper> getComments(int subjectId, int parentId, int pageNumber) {
        return commentDataStore.getComments(subjectId, parentId, pageNumber);
    }

    @Override
    public Observable<Comment> sendComment(int subjectId, int parentId, String comment, boolean isAnonymous) {
        return commentDataStore.sendComment(subjectId, parentId, comment, isAnonymous);
    }

    @Override
    public Observable<Like> sendLike(int subjectId, int commentId, boolean like) {
        return commentDataStore.sendLike(subjectId, commentId, like);
    }

    @Override
    public Observable removeLike(int subjectId, int commentId, boolean like) {
        return commentDataStore.removeLike(subjectId, commentId, like);
    }

}
