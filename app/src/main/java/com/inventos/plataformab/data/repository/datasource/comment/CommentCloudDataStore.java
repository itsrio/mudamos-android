package com.inventos.plataformab.data.repository.datasource.comment;

import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Like;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class CommentCloudDataStore implements CommentDataStore {

    private final MudamosApi apiInstance;

    public CommentCloudDataStore(MudamosApi apiInstance) {
        this.apiInstance = apiInstance;
    }

    @Override
    public Observable<CommentRequestWrapper> getComments(int subjectId, int parentId, int pageNumber) {
        if (parentId == 0)
            return this.apiInstance.getComments(subjectId, pageNumber);
        return this.apiInstance.getComments(subjectId, parentId, pageNumber);
    }

    @Override
    public Observable<Comment> sendComment(int subjectId, int parentId, String comment,
                                           boolean isAnonymous) {
        Comment tempComment = new Comment();
        tempComment.setContent(comment);
        tempComment.setIsAnonymous(isAnonymous);
        if (parentId == 0) return this.apiInstance.sendComment(subjectId, tempComment);
        else return this.apiInstance.sendComment(subjectId, parentId, tempComment);

    }

    @Override
    public Observable<Like> sendLike(int subjectId, int commentId, boolean like) {
        String action = (like) ? "likes" : "dislikes";
        return this.apiInstance.likeOrDislike(subjectId, commentId, action, "");
    }

    @Override
    public Observable removeLike(int subjectId, int commentId, boolean like) {
        String action = (like) ? "likes" : "dislikes";
        return this.apiInstance.removeLikeOrDislike(subjectId, commentId, action);
    }

}
