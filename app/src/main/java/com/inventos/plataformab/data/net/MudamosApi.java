package com.inventos.plataformab.data.net;

import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Like;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Notifications;
import com.inventos.plataformab.presentation.model.SignUpComponents;
import com.inventos.plataformab.presentation.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface MudamosApi {

    @POST("/users/sign_in")
    Observable<User> authenticateUser(@Body User user);

    @POST("/users/{provider}")
    Observable<User> createUser(@Path("provider") String provider, @Body User user);

    @POST("/users/{provider}")
    Observable<User> authenticateUser(@Path("provider") String provider, @Body User user);

    @GET("/user")
    Observable<User> getLoggerUser();

    @GET("/cycles")
    Observable<MainRequest> getMainRequest();

    @GET("/users/sign_up")
    Observable<SignUpComponents> getSignUpComponents();

    @PUT("/users")
    Observable<User> editUser(@Body User user);

    @POST("/passwords")
    Observable<User> recoverPassword(@Body User user);

    @GET("/cycles")
    Observable<List<Cycle>> getCycles();

    @GET("/users/notifications")
    Observable<List<Notifications>> getNotifications();

    @DELETE("/users/sign_out")
    void logoutUser(Callback<Object> callback);

    @GET("/subjects/{id}/comments?user_attributes")
    Observable<CommentRequestWrapper> getComments(@Path("id") int subjectId,
                                          @Query("page") int pageNumber);

    @GET("/subjects/{id}/comments/{parent}/comments?user_attributes")
    Observable<CommentRequestWrapper> getComments(@Path("id") int subjectId,
                                          @Path("parent") int parent,
                                          @Query("page") int pageNumber);

    @POST("/subjects/{id}/comments?user_attributes")
    Observable<Comment> sendComment(@Path("id") int subjectId, @Body Comment comment);

    @POST("/subjects/{id}/comments/{parent}/comments?user_attributes")
    Observable<Comment> sendComment(@Path("id") int subjectId, @Path("parent") int parent, @Body Comment comment);

    @POST("/subjects/{subject_id}/comments/{comment_id}/{action}")
    Observable<Like> likeOrDislike(@Path("subject_id") int subjectId, @Path("comment_id") int parent,
                                   @Path("action") String action, @Body String body);

    @DELETE("/subjects/{subject_id}/comments/{comment_id}/{action}")
    Observable<Like> removeLikeOrDislike(@Path("subject_id") int subjectId, @Path("comment_id") int parent,
                                   @Path("action") String action);
}
