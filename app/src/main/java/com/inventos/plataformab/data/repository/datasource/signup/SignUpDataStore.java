package com.inventos.plataformab.data.repository.datasource.signup;

import com.inventos.plataformab.presentation.model.SignUpComponents;

import rx.Observable;

public interface SignUpDataStore {
    Observable<SignUpComponents> getComponents();
}
