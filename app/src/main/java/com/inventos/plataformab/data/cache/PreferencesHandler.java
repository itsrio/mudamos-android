package com.inventos.plataformab.data.cache;

import com.inventos.plataformab.presentation.model.User;

import rx.Observable;

public interface PreferencesHandler {
    String APP_PREFERENCES = "preferences.mudamos";
    String USER = "user";
    String AUTH_TOKEN = "user.token";
    String FIRST_TIME = "first.time"; // First time app is opened

    Observable<User> getUser();
    Observable saveUser(User user);
    Observable removeUser();
}
