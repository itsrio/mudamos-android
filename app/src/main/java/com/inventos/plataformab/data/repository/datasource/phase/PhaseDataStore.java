package com.inventos.plataformab.data.repository.datasource.phase;

import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Phase;

import java.util.List;

import rx.Observable;

public interface PhaseDataStore {
    Observable<List<Phase>> phasesByCycle(Cycle cycle);
    Observable<Phase> phaseById(int phaseId);
}
