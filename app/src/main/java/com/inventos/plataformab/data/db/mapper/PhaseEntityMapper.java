package com.inventos.plataformab.data.db.mapper;

import com.inventos.plataformab.data.db.entities.EntitiySubject;
import com.inventos.plataformab.data.db.entities.EntityPhase;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.Subject;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class PhaseEntityMapper {

    private SubjectEntityMapper mSubjectMapper;

    public PhaseEntityMapper(SubjectEntityMapper subjectMapper) {
        this.mSubjectMapper = subjectMapper;
    }

    public Phase getObject(EntityPhase phaseRealmObject) {
        Phase phase = new Phase();
        if (phaseRealmObject != null) {
            phase.setName(phaseRealmObject.getName());
            phase.setDescription(phaseRealmObject.getDescription());
            phase.setId(phaseRealmObject.getId());
            phase.setCycleId(phase.getCycleId());
            phase.setFinalDate(phaseRealmObject.getFinalDate());
            phase.setInitialDate(phaseRealmObject.getInitialDate());
            phase.setCurrent(phaseRealmObject.isCurrent());
            phase.setPictureFileName(phaseRealmObject.getPictureFileName());
            phase.setType(phaseRealmObject.getType());

            phase.setSubjects(new ArrayList<Subject>());
            int subjectSize = phaseRealmObject.getSubjects().size();
            List<EntitiySubject> realmSubjects = phaseRealmObject.getSubjects();
            for (int i = 0; i < subjectSize; i++) {
                phase.getSubjects().add(mSubjectMapper.getObject(realmSubjects.get(i)));
            }
        }
        return phase;
    }

    public EntityPhase getRealmObject(Phase phase) {
        EntityPhase phaseRealmObject = new EntityPhase();
        if (phase != null) {
            phaseRealmObject.setName(phase.getName());
            phaseRealmObject.setDescription(phase.getDescription());
            phaseRealmObject.setId(phase.getId());
            phaseRealmObject.setCycleId(phase.getCycleId());
            phaseRealmObject.setFinalDate(phase.getFinalDate());
            phaseRealmObject.setInitialDate(phase.getInitialDate());
            phaseRealmObject.setCurrent(phase.isCurrent());
            phaseRealmObject.setPictureFileName(phase.getPictureFileName());
            phaseRealmObject.setType(phase.getType());

            int subjectSize = phase.getSubjects().size();
            phaseRealmObject.setSubjects(new RealmList<EntitiySubject>());
            List<Subject> subjects = phase.getSubjects();
            for (int i = 0; i < subjectSize; i++) {
                phaseRealmObject.getSubjects().add(mSubjectMapper.getRealmObject(subjects.get(i)));
            }
        }

        return phaseRealmObject;
    }

}
