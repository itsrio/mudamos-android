package com.inventos.plataformab.data.repository.datasource.signup;

import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.presentation.model.SignUpComponents;

import rx.Observable;

public class CloudSignUpDataStore implements SignUpDataStore {

    MudamosApi api;

    public CloudSignUpDataStore(MudamosApi api) {
        this.api = api;
    }


    @Override
    public Observable<SignUpComponents> getComponents() {
        return this.api.getSignUpComponents();
    }
}
