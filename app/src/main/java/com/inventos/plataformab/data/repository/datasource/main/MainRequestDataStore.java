package com.inventos.plataformab.data.repository.datasource.main;

import com.inventos.plataformab.presentation.model.MainRequest;

import rx.Observable;

public interface MainRequestDataStore {
    Observable<MainRequest> getMainRequest();
}
