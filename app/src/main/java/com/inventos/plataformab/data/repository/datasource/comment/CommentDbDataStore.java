package com.inventos.plataformab.data.repository.datasource.comment;

import com.inventos.plataformab.data.db.DbHandlerImpl;
import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.CommentRequestWrapper;
import com.inventos.plataformab.presentation.model.Like;

import rx.Observable;

public class CommentDbDataStore implements CommentDataStore {

    final DbHandlerImpl db;

    public CommentDbDataStore(DbHandlerImpl db) {
        this.db = db;
    }

    @Override
    public Observable<CommentRequestWrapper> getComments(int subjectId, int parentId, int pageNumber) {
        return null;
    }

    @Override
    public Observable<Comment> sendComment(int subjectId, int parentId, String comment, boolean isAnonymous) {
        return null;
    }

    @Override
    public Observable<Like> sendLike(int subjectId, int commentId, boolean like) {
        return null;
    }

    @Override
    public Observable removeLike(int subjectId, int commentId, boolean like) {
        return null;
    }
}
