package com.inventos.plataformab.data.repository.datasource.settings;

import com.inventos.plataformab.presentation.model.Settings;

import rx.Observable;

public interface SettingsDataStore {
    Observable save(Settings settings);
    Observable<Settings> settings();
}
