package com.inventos.plataformab.data.repository;

import com.inventos.plataformab.data.repository.datasource.main.MainRequestDataStore;
import com.inventos.plataformab.data.repository.datasource.main.MainRequestDataStoreFactory;
import com.inventos.plataformab.presentation.model.MainRequest;

import rx.Observable;

public class MainRequestRepository implements MainRequestDataStore {

    MainRequestDataStore mainRequestDataStore;

    public MainRequestRepository(MainRequestDataStoreFactory factory, boolean useCloud) {
        if (useCloud) {
            this.mainRequestDataStore = factory.createCloudDataStore();
        }
        else {
            this.mainRequestDataStore = factory.createDbDataStore();
        }

    }

    @Override
    public Observable<MainRequest> getMainRequest() {
        return this.mainRequestDataStore.getMainRequest();
    }
}
