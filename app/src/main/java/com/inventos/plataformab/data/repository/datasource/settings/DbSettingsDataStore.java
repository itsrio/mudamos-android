package com.inventos.plataformab.data.repository.datasource.settings;

import com.inventos.plataformab.data.db.DbHandlerImpl;
import com.inventos.plataformab.presentation.model.Settings;

import rx.Observable;
import rx.Subscriber;

public class DbSettingsDataStore implements SettingsDataStore {

    DbHandlerImpl db;

    public DbSettingsDataStore(DbHandlerImpl dbInstance) {
        this.db = dbInstance;
    }

    @Override
    public Observable save(final Settings settings) {
        return Observable.create(new Observable.OnSubscribe() {
            @Override
            public void call(Object o) {
                db.saveSettings(settings);
            }
        });
    }

    @Override
    public Observable<Settings> settings() {
        return Observable.create(new Observable.OnSubscribe<Settings>() {
            @Override
            public void call(Subscriber<? super Settings> subscriber) {
                Settings settings = db.getSettings();
                subscriber.onNext(settings);
                subscriber.onCompleted();
            }

        });
    }
}
