package com.inventos.plataformab.data.db.entities;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Comment extends RealmObject {

    private int id;
    private User user;
    private EntitiySubject entitiySubject;
    private String name;
    private String commentDate;
    private int likes;
    private int dislikes;
    private RealmList<Comment> answers;

    public Comment() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EntitiySubject getEntitiySubject() {
        return entitiySubject;
    }

    public void setEntitiySubject(EntitiySubject entitiySubject) {
        this.entitiySubject = entitiySubject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public RealmList<Comment> getAnswers() {
        return answers;
    }

    public void setAnswers(RealmList<Comment> answers) {
        this.answers = answers;
    }
}
