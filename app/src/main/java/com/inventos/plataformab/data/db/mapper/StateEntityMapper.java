package com.inventos.plataformab.data.db.mapper;

import com.inventos.plataformab.presentation.model.City;
import com.inventos.plataformab.presentation.model.State;

import java.util.ArrayList;

public class StateEntityMapper {
    public State getObject(com.inventos.plataformab.data.db.entities.State entityState) {
        State state = new State();
        state.setCities(new ArrayList<City>());
        state.setUf(entityState.getUf());
        state.setName(entityState.getName());
        int size = entityState.getCities().size();
        for (int i = 0; i < size; i++) {
            City cityObject = getCityObject(entityState.getCities().get(i));
            state.getCities().add(cityObject);
        }
        return state;
    }

    private City getCityObject(com.inventos.plataformab.data.db.entities.City entityCity) {
        City city = new City();
        city.setName(entityCity.getName());
        city.setId( entityCity.getId());
        return city;
    }
}
