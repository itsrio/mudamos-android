package com.inventos.plataformab.data.db.mapper;

import com.inventos.plataformab.data.db.entities.EntityCycle;
import com.inventos.plataformab.data.db.entities.EntityPhase;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Phase;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class CycleEntityMapper {

    private final PhaseEntityMapper phaseEntityMapper;

    public CycleEntityMapper(PhaseEntityMapper phaseEntityMapper) {
            this.phaseEntityMapper = phaseEntityMapper;
    }

    public Cycle getObject(EntityCycle entityCycle) {
        Cycle cycleObj = new Cycle();
        cycleObj.setPhases(new ArrayList<Phase>());
        if (entityCycle != null){
            cycleObj.setAbout(entityCycle.getAbout());
            cycleObj.setFinalDate(entityCycle.getFinalDate());
            cycleObj.setId(entityCycle.getId());
            cycleObj.setName(entityCycle.getName());
            cycleObj.setInitialDate(entityCycle.getInitialDate());
            cycleObj.setTitle(entityCycle.getTitle());
            cycleObj.setColor(entityCycle.getColor());
            cycleObj.setPicture(entityCycle.getPicture());
            cycleObj.setDescription(entityCycle.getDescription());
            cycleObj.setCompilationPdf(entityCycle.getCompilationPdf());

            cycleObj.setPhases(new ArrayList<Phase>());
            int phasesSize = entityCycle.getEntityPhases().size();
            List<EntityPhase> realmPhases = entityCycle.getEntityPhases();
            for (int i = 0; i < phasesSize; i++) {
                cycleObj.getPhases().add(phaseEntityMapper.getObject(realmPhases.get(i)));
            }

        }
        return cycleObj;
    }

    public EntityCycle getRealmObject(Cycle cycleObj) {
        EntityCycle cycleRealmObj = new EntityCycle();
        cycleRealmObj.setId(cycleObj.getId());
        cycleRealmObj.setTitle(cycleObj.getTitle());
        cycleRealmObj.setFinalDate(cycleObj.getFinalDate());
        cycleRealmObj.setInitialDate(cycleObj.getInitialDate());
        cycleRealmObj.setName(cycleObj.getName());
        cycleRealmObj.setAbout(cycleObj.getAbout());
        cycleRealmObj.setColor(cycleObj.getColor());
        cycleRealmObj.setPicture(cycleObj.getPicture());
        cycleRealmObj.setCompilationPdf(cycleObj.getCompilationPdf());
        cycleRealmObj.setDescription(cycleObj.getDescription());

        cycleRealmObj.setEntityPhases(new RealmList<EntityPhase>());
        int phasesSize = cycleObj.getPhases().size();
        List<Phase> objectPhases = cycleObj.getPhases();
        for (int i = 0; i < phasesSize; i++) {
            cycleRealmObj.getEntityPhases().add(
                    phaseEntityMapper.getRealmObject(objectPhases.get(i)));
        }

        return cycleRealmObj;
    }

}
