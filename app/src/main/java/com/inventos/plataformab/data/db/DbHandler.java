package com.inventos.plataformab.data.db;

import com.inventos.plataformab.presentation.model.Comment;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.MainRequest;
import com.inventos.plataformab.presentation.model.Phase;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.model.State;

import java.util.List;

public interface DbHandler {

    void saveCycle(Cycle cycle);
    void saveCycles(List<Cycle> cycles);
    Cycle getCycle(int id);
    List<Cycle> allCycles();
    List<Phase> phasesByCycle(int cycleId);

    void savePhase(Phase phase);
    Phase getPhase(int phaseId);
    List<Comment> getComments(int subjectId);

    void saveSettings(Settings settings);
    Settings getSettings();

    MainRequest getMainRequest();

    List<State> allStates();

    void bumpSchema(long version);
}