package com.inventos.plataformab.data.db.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DiscussionPlugin extends RealmObject {
    @PrimaryKey private int id;
    private RealmList<EntitiySubject> entitiySubjects;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<EntitiySubject> getEntitiySubjects() {
        return entitiySubjects;
    }

    public void setEntitiySubjects(RealmList<EntitiySubject> entitiySubjects) {
        this.entitiySubjects = entitiySubjects;
    }
}
