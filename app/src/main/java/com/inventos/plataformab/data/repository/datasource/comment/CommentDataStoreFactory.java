package com.inventos.plataformab.data.repository.datasource.comment;

import com.inventos.plataformab.data.db.DbHandlerImpl;
import com.inventos.plataformab.data.db.MudamosDb;
import com.inventos.plataformab.data.net.MudamosApi;
import com.inventos.plataformab.data.net.RestClient;

public class CommentDataStoreFactory {

    public CommentDataStore createDbDataStore() {
        DbHandlerImpl database = MudamosDb.getDbInstance();
        return new CommentDbDataStore(database);
    }
    public CommentDataStore createCloudDataStore() {
        MudamosApi api = RestClient.getApiInstance();
        return new CommentCloudDataStore(api);
    }


}
