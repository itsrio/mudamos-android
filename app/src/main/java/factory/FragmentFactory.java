package factory;

import android.content.Context;

import com.google.gson.Gson;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.model.Settings;
import com.inventos.plataformab.presentation.view.fragment.AboutFragment;
import com.inventos.plataformab.presentation.view.fragment.CommentFragment;
import com.inventos.plataformab.presentation.view.fragment.ConfigurationFragment;
import com.inventos.plataformab.presentation.view.fragment.CycleFragment;
import com.inventos.plataformab.presentation.view.fragment.EditUserFragment;
import com.inventos.plataformab.presentation.view.fragment.NotificationsFragment;
import com.inventos.plataformab.presentation.view.fragment.PhaseFragment;
import com.inventos.plataformab.presentation.view.fragment.SignUpFragment;
import com.inventos.plataformab.presentation.view.fragment.SplashFragment;

import java.util.ArrayList;

public class FragmentFactory {

    private static final Gson gson = new Gson();

    public static SignUpFragment newSignUpFragment(Context context, String userJson) {
        SignUpFragment signUpFragment = SignUpFragment.newInstance(userJson);
        signUpFragment.setPresenter(PresenterFactory.newSignUpPresenter(context),
                PresenterFactory.newLoginPresenter(context));
        return signUpFragment;
    }

    public static SplashFragment newSplashFragment(Context context) {
        SplashFragment splashFragment = SplashFragment.newInstance();
        splashFragment.setPresenter(PresenterFactory.newSplashPresenter(context));
        return splashFragment;
    }

    public static AboutFragment newAboutFragment(Settings settings) {
        return AboutFragment.newInstance(settings);
    }

    public static CycleFragment newCycleFragment(Cycle cycle) {
        String jsonCycle = gson.toJson(cycle);
        CycleFragment cycleFragment = CycleFragment.newInstance(jsonCycle);
        cycleFragment.setPresenter(PresenterFactory.newPhaseListPresenter(cycle));
        return cycleFragment;
    }

    public static PhaseFragment newPhaseFragment(int phaseId, int cycleColor) {
        PhaseFragment phaseFragment = PhaseFragment.newInstance(phaseId, cycleColor);
        phaseFragment.setPresenter(PresenterFactory.newPhasePresenter(phaseId));
        return phaseFragment;
    }

    public static CommentFragment newCommentFragment(int subjectId,
                                                     ArrayList<Integer> commentParent, int color, boolean hasAnswered, boolean isAnonymous, boolean phaseHasEnded) {
        int parentId = commentParent.size() == 0 ? 0 : commentParent.get(commentParent.size() - 1);
        CommentFragment fragment = CommentFragment.newInstance(subjectId, color, commentParent,
                hasAnswered, isAnonymous, phaseHasEnded);
        fragment.setPresenter(PresenterFactory.newCommentPresenter(subjectId, parentId));
        return fragment;
    }

    public static ConfigurationFragment newConfigurationFragment() {
        ConfigurationFragment fragment = ConfigurationFragment.newInstance();
        return fragment;
    }

    public static EditUserFragment newEditUserFragment(Context context) {
        EditUserFragment fragment = EditUserFragment.newInstance();
        fragment.setPresenter(PresenterFactory.newEditUserPresenter(context));
        return fragment;
    }

    public static NotificationsFragment newNotificationsFragment() {
        NotificationsFragment fragment = NotificationsFragment.newInstance();
        fragment.setPresenter(PresenterFactory.newNotificationsPresenter());
        return fragment;
    }
}
