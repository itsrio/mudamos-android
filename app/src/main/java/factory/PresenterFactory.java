package factory;

import android.content.Context;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.inventos.plataformab.data.executor.JobExecutor;
import com.inventos.plataformab.data.repository.CommentRepository;
import com.inventos.plataformab.data.repository.CycleRepository;
import com.inventos.plataformab.data.repository.MainRequestRepository;
import com.inventos.plataformab.data.repository.NotificationsRepository;
import com.inventos.plataformab.data.repository.PhaseRepository;
import com.inventos.plataformab.data.repository.SettingsRepository;
import com.inventos.plataformab.data.repository.SignUpRepository;
import com.inventos.plataformab.data.repository.StateRepository;
import com.inventos.plataformab.data.repository.UserDataRepository;
import com.inventos.plataformab.data.repository.datasource.comment.CommentDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.cycle.CycleDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.main.MainRequestDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.notifications.NotificationsDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.phase.PhaseDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.settings.SettingsDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.signup.SignUpDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.state.StateDataStoreFactory;
import com.inventos.plataformab.data.repository.datasource.user.UserDataStore;
import com.inventos.plataformab.data.repository.datasource.user.UserDataStoreFactory;
import com.inventos.plataformab.domain.interactor.ChangePasswordUseCase;
import com.inventos.plataformab.domain.interactor.CreateUserUseCase;
import com.inventos.plataformab.domain.interactor.EditUserUseCase;
import com.inventos.plataformab.domain.interactor.GetCommentsUseCase;
import com.inventos.plataformab.domain.interactor.GetLoggedUserUseCase;
import com.inventos.plataformab.domain.interactor.GetNotificationsUseCase;
import com.inventos.plataformab.domain.interactor.GetPhasePluginsUseCase;
import com.inventos.plataformab.domain.interactor.GetPhasesUseCase;
import com.inventos.plataformab.domain.interactor.GetSignUpComponentsUseCase;
import com.inventos.plataformab.domain.interactor.GetStatesUseCase;
import com.inventos.plataformab.domain.interactor.GetUserUseCase;
import com.inventos.plataformab.domain.interactor.LikeDislikeUseCase;
import com.inventos.plataformab.domain.interactor.LoginUseCase;
import com.inventos.plataformab.domain.interactor.LogoutUserUseCase;
import com.inventos.plataformab.domain.interactor.MainRequestUseCase;
import com.inventos.plataformab.domain.interactor.RemoveLikeDislikeUseCase;
import com.inventos.plataformab.domain.interactor.RetrievePasswordUseCase;
import com.inventos.plataformab.domain.interactor.SaveCyclesUseCase;
import com.inventos.plataformab.domain.interactor.SaveSettingsUseCase;
import com.inventos.plataformab.domain.interactor.SaveUserUseCase;
import com.inventos.plataformab.domain.interactor.SendCommentUseCase;
import com.inventos.plataformab.presentation.model.Cycle;
import com.inventos.plataformab.presentation.presenter.UIThread;
import com.inventos.plataformab.presentation.presenter.cycle.PhaseListPresenter;
import com.inventos.plataformab.presentation.presenter.drawer.DrawerPresenter;
import com.inventos.plataformab.presentation.presenter.login.ChangePasswordPresenter;
import com.inventos.plataformab.presentation.presenter.login.LoginPresenter;
import com.inventos.plataformab.presentation.presenter.main.MainPresenter;
import com.inventos.plataformab.presentation.presenter.main.TabPresenter;
import com.inventos.plataformab.presentation.presenter.notification.NotificationsPresenter;
import com.inventos.plataformab.presentation.presenter.phase.PhasePresenter;
import com.inventos.plataformab.presentation.presenter.signup.SignUpPresenter;
import com.inventos.plataformab.presentation.presenter.splash.SplashPresenter;
import com.inventos.plataformab.presentation.presenter.subject.CommentPresenter;
import com.inventos.plataformab.presentation.presenter.user.EditUserPresenter;
import com.inventos.plataformab.presentation.util.FacebookHelper;
import com.inventos.plataformab.presentation.view.activity.BaseActivity;

public class PresenterFactory {

    private static final JobExecutor jobExecutor = new JobExecutor();
    private static final UIThread uiThread = new UIThread();

    public static SignUpPresenter newSignUpPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataStore userRepository = new UserDataRepository(factory, context);
        StateDataStoreFactory stateFactory = new StateDataStoreFactory();
        StateRepository stateRepository = new StateRepository(stateFactory);
        SignUpDataStoreFactory signUpFactory = new SignUpDataStoreFactory();
        SignUpRepository signUpRepository = new SignUpRepository(signUpFactory);

        return new SignUpPresenter(new CreateUserUseCase(userRepository, jobExecutor,uiThread),
                new SaveUserUseCase(userRepository, jobExecutor, uiThread),
                new GetStatesUseCase(stateRepository, jobExecutor, uiThread),
                new GetSignUpComponentsUseCase(signUpRepository, jobExecutor, uiThread));
    }

    public static SplashPresenter newSplashPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataStore userRepository = new UserDataRepository(factory, context);
        MainRequestDataStoreFactory mainFactory = new MainRequestDataStoreFactory();
        MainRequestRepository mainRepository = new MainRequestRepository(mainFactory, true);
        return new SplashPresenter(
                new GetUserUseCase(userRepository, jobExecutor,uiThread),
                new MainRequestUseCase(mainRepository, jobExecutor, uiThread));
    }

    public static MainPresenter newMainPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataStore userRepository = new UserDataRepository(factory, context);
        CycleDataStoreFactory cycleFactory = new CycleDataStoreFactory();
        CycleRepository cycleRepository = new CycleRepository(cycleFactory, context);
        MainRequestDataStoreFactory mainFactory = new MainRequestDataStoreFactory();
        MainRequestRepository mainRepository = new MainRequestRepository(mainFactory, true);
        SettingsDataStoreFactory settingsFactory = new SettingsDataStoreFactory();
        SettingsRepository settingsRepository = new SettingsRepository(settingsFactory);
        return new MainPresenter(
                new GetUserUseCase(userRepository, jobExecutor,uiThread),
                new MainRequestUseCase(mainRepository, jobExecutor, uiThread),
                new SaveCyclesUseCase(cycleRepository, jobExecutor, uiThread),
                new SaveSettingsUseCase(settingsRepository, jobExecutor, uiThread));
    }

    public static DrawerPresenter newDrawerPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataStore userRepository = new UserDataRepository(factory, context);
        return new DrawerPresenter(new LogoutUserUseCase(userRepository, jobExecutor,uiThread),
                new GetLoggedUserUseCase(userRepository, jobExecutor, uiThread),
                new SaveUserUseCase(userRepository, jobExecutor, uiThread));
    }

    public static LoginPresenter newLoginPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataStore userRepository = new UserDataRepository(factory, context);

        LoginManager loginManager = LoginManager.getInstance();

        LoginPresenter presenter =  new LoginPresenter(new LoginUseCase(userRepository, jobExecutor,uiThread),
                new SaveUserUseCase(userRepository, jobExecutor, uiThread),
                new RetrievePasswordUseCase(userRepository, jobExecutor, uiThread),
                new FacebookHelper(CallbackManager.Factory.create(), loginManager.getInstance(),
                        (BaseActivity) context));
        return presenter;
    }

    public static TabPresenter newTabPresenter(Context context) {
        MainRequestDataStoreFactory factory = new MainRequestDataStoreFactory();
        MainRequestRepository repository = new MainRequestRepository(factory, false);
        return new TabPresenter(new MainRequestUseCase(repository, jobExecutor, uiThread));
    }

    public static PhaseListPresenter newPhaseListPresenter(Cycle cycle) {
        PhaseDataStoreFactory phaseFactory = new PhaseDataStoreFactory();
        PhaseRepository repository = new PhaseRepository(phaseFactory);
        return new PhaseListPresenter(new GetPhasesUseCase(repository, cycle,
                jobExecutor, uiThread));
    }

    public static PhasePresenter newPhasePresenter(int phaseId) {
        PhaseDataStoreFactory phaseFactory = new PhaseDataStoreFactory();
        PhaseRepository repository = new PhaseRepository(phaseFactory);
        return new PhasePresenter(new GetPhasePluginsUseCase(
                repository, phaseId, jobExecutor, uiThread));
    }

    public static CommentPresenter newCommentPresenter(int subjectId, int parentId) {
        CommentDataStoreFactory commentFactory = new CommentDataStoreFactory();
        CommentRepository repository = new CommentRepository(commentFactory);
        GetCommentsUseCase getCommentsUseCase =
                new GetCommentsUseCase(repository, subjectId, parentId, jobExecutor, uiThread);
        SendCommentUseCase sendCommentUseCase =
                new SendCommentUseCase(repository, subjectId, parentId, jobExecutor, uiThread);
        LikeDislikeUseCase likeDislikeUseCase =
                new LikeDislikeUseCase(repository, jobExecutor, uiThread);
        RemoveLikeDislikeUseCase removeLikeDislikeUseCase=
                new RemoveLikeDislikeUseCase(repository, jobExecutor, uiThread);
        return new CommentPresenter(getCommentsUseCase, likeDislikeUseCase, removeLikeDislikeUseCase,sendCommentUseCase);
    }

    public static ChangePasswordPresenter newChangePasswordPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataRepository repository = new UserDataRepository(factory, context);
        ChangePasswordUseCase changePasswordUseCase = new ChangePasswordUseCase(repository,
                jobExecutor, uiThread);
        SaveUserUseCase saveUserUseCase = new SaveUserUseCase(repository, jobExecutor, uiThread);
        return new ChangePasswordPresenter(changePasswordUseCase, saveUserUseCase);
    }

    public static EditUserPresenter newEditUserPresenter(Context context) {
        UserDataStoreFactory factory = new UserDataStoreFactory();
        UserDataStore userRepository = new UserDataRepository(factory, context);
        StateDataStoreFactory stateFactory = new StateDataStoreFactory();
        StateRepository stateRepository = new StateRepository(stateFactory);
        SignUpDataStoreFactory signUpFactory = new SignUpDataStoreFactory();
        SignUpRepository signUpRepository = new SignUpRepository(signUpFactory);

        return new EditUserPresenter(new EditUserUseCase(userRepository, jobExecutor,uiThread),
                new SaveUserUseCase(userRepository, jobExecutor, uiThread),
                new GetStatesUseCase(stateRepository, jobExecutor, uiThread),
                new GetSignUpComponentsUseCase(signUpRepository, jobExecutor, uiThread));
    }

    public static NotificationsPresenter newNotificationsPresenter() {
        NotificationsDataStoreFactory factory = new NotificationsDataStoreFactory();
        NotificationsRepository repository = new NotificationsRepository(factory);
        return new NotificationsPresenter(new GetNotificationsUseCase(
                repository, jobExecutor, uiThread));
//        UserDataStore userRepository = new UserDataRepository(factory, context);
//        StateDataStoreFactory stateFactory = new StateDataStoreFactory();
//        StateRepository stateRepository = new StateRepository(stateFactory);
//        SignUpDataStoreFactory signUpFactory = new SignUpDataStoreFactory();
//        SignUpRepository signUpRepository = new SignUpRepository(signUpFactory);

//        return new EditUserPresenter(new EditUserUseCase(userRepository, jobExecutor,uiThread),
//                new SaveUserUseCase(userRepository, jobExecutor, uiThread),
//                new GetStatesUseCase(stateRepository, jobExecutor, uiThread),
//                new GetSignUpComponentsUseCase(signUpRepository, jobExecutor, uiThread));
    }
}

